﻿window.$official_slides = $("#official_slides");
window.$official_win = $("#offcial_win");
window.current_official_slide = 1;
window.userObj = [];


$(	function() {
	GetUser() ;				//後台銜接後執行此行
})


/* ============================================================================= */
/* ========================= official slide ==================================== */
/* ============================================================================= */

function GetUser() {
	$.ajax( {
		type: 'GET',
		url: "GetUser/",
		dataType: 'json',
		success: function(Jdata) {
			window.userObj = Jdata;
			InitialOfficialPage();
		},
		error: function(Jdata) {
			window.userObj = Jdata;
			InitialOfficialPage();
		}
	})
}

function InitialOfficialPage() {
	// console.log( window.userObj );
	if ( window.userObj == null ) {
		$("#login_button").text("Login");
		$("#register_button").text('Register');
		SetUnloggedIconClick();
	}
	else {
		
		$("#login_button").text(window.userObj.name);
		$("#login_button").unbind("click");
		$("#register_button").text('Logout');
		$("#register_button").unbind("click");
		$("#register_button").attr("href","Logout/");
		
	}
	NewsInfomationAppend();
	
	SetButtonClick();
}

function SetButtonClick() {
	$("#try_button").click( function(){
		window.open('ProductSetting/CloudSliders');
	})
}



function NewsInfomationAppend() {

	for ( var i = 0; i < window.news.length; i++ ) {
		$('<div class="media"></div>').append( $('<a href="#" class="pull-left"> <img id="news_image'+ i +'"src="' + window.news[i].pic + '" alt="" class="media-object news_image" /> </a>') )
									  .append( $('<div class="media-body"> <h4 class="media-heading">'+window.news[i].title+'</h4> <p>'+window.news[i].content+'</p> </div>') )
									  .appendTo( $("#news_container") );

		
	}

}


function SetUnloggedIconClick() {
	
	$("#cata_cliders").click( function(e) {
		e.preventDefault();
	})
	
	$("#cata_home").click( function(e) {
		e.preventDefault();
		TweenMax.set($("#register_panel"), { "height": 0} );
		TweenMax.set( $("#login_panel"), { "height":0} );
		$($('li'), $("#login_register_nav")).removeClass('active');
		$(this).parent().addClass('active');
	})
	
	$("#cata_service").click( function(e) {
		e.preventDefault();
	})
	
	$("#cata_news").click( function(e) {
		e.preventDefault();
	})
	
	$("#cata_contact").click( function(e) {
		e.preventDefault();
	})
	
	$("#login_button").click( function(e) {
		e.preventDefault();
		if ( $("#login_panel").height() == 0 ) {
			TweenMax.set($("#register_panel"), { "height": 0} );
			TweenMax.to( $("#login_panel"), 1, { "height":150} );
			$('#login_account').focus();
			$($('li'), $("#login_register_nav")).removeClass('active');
			$(this).parent().addClass('active');
		}
		else {
			$($('li'), $("#login_register_nav")).removeClass('active');
			$("#cata_home").parent().addClass('active');
			TweenMax.to( $("#login_panel"), 1, { "height":0} );
		}
	})

	$("#register_button").click( function(e) {
		e.preventDefault();
		if ( $("#register_panel").height() == 0 ) {
			TweenMax.set( $("#login_panel"), { "height":0} );
			TweenMax.to($("#register_panel"), 1, { "height": 265} );
			$('#register_account').focus();
			$($('li'), $("#login_register_nav")).removeClass('active');
			$(this).parent().addClass('active');
		}
		else {
			$($('li'), $("#login_register_nav")).removeClass('active');
			$("#cata_home").parent().addClass('active');
			TweenMax.to($("#register_panel"), 1, { "height": 0} );
		}
	})
	
	$("#login_submit").click( function(e) {
		e.preventDefault();
		var account=$('#login_account').val();
		var password=$('#login_password').val();
		// console.log(account + "   " + password);
		var data = { "account" : account, "password" : password } ;
		$.ajax({
			type: 'POST',
			url: "Login/",
			data: data,
			dataType: 'json',
			success: function(JData) {
				if ( JData.error ) {
					alert(JData.error);
				}
				else
					window.location.reload();
			},
			error: function() {
				alert("ERROR!!!");
			}
		});
	})

	$("#register_submit").click( function(e) {
		e.preventDefault();
		var account=$(register_account).val();
		var password=$(register_password).val();
		var repeat=$(register_repeat).val();
		var name=$(register_name).val();
		var sex=$('input[name=register_sex]:checked').val();
		
		var data = { "account" : account, "password" : password, "repeat" : repeat, "name" : name, "sex" : sex };
		console.log( data );
		$.ajax({
			type: 'POST',
			url: "Regist/",
			data: data,
			dataType: 'json',
			success: function(JData) {
				if ( JData.error ) {
					alert(JData.error);
				}
				else
					window.location.reload();
			},
			error: function() {
				alert("REGISTER ERROR!!!");
			}
		});
	})
}


//------------------------------------------------------------------ Add_By_Cycho
$( document ).ready(function() {

	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				// Does this cookie string begin with the name we want?
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	function csrfSafeMethod(method) {
		// these HTTP methods do not require CSRF protection
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
  
	function sameOrigin(url) {
		// test that a given url is a same-origin URL
		// url could be relative or scheme relative or absolute
		var host = document.location.host; // host + port
		var protocol = document.location.protocol;
		var sr_origin = '//' + host;
		var origin = protocol + sr_origin;
		// Allow absolute or scheme relative URLs to same origin
		return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
			(url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
			// or any other URL that isn't scheme relative or absolute i.e relative.
			!(/^(\/\/|http:|https:).*/.test(url));
	}
	  
	$.ajaxSetup({
		crossDomain: false, // obviates need for sameOrigin test
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type)) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		}
	});
});
//------------------------------------------------------------------ Add_By_Cycho

