﻿//============================================== 2013/11/04 updated by Paul Ku 
$(window).load( function (){
	$(document).on('touchmove',function(e) {  //禁止拖拉畫面
		e.preventDefault();
	});

	/*====================================================*/
	
	AnimationAppend();
	ClickIconFunction();
	/*====================================================*/

	fixLayout();

	if ( /Android/i.test(navigator.userAgent) ) {
		setTimeout( "fixLayout()" ,200 );

		$( window ).on( "orientationchange", function( e ) {
			e.preventDefault();
			setTimeout( "fixLayout()" ,200 );
		});
	}
	else if ( /webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$( window ).on( "orientationchange", function( e ) {
			e.preventDefault();
			fixLayout();
		});
	}
	else {
		$(window).resize( function(e) {
			e.preventDefault();
			fixLayout();
		});
	}
	
});

function fixLayout() {
	
	var win_width = $(window).width();
	var win_height = $(window).height();
	var half_win_width = win_width/2;
	var half_win_height = win_height/2;
	var top_height = 0;
	var foot_height = 0;
	var display_width = win_width;
	var display_height = win_height;

	if ( win_width/212*317 + 150 <= win_height) {
		// 寬滿版配置
		console.log("width_max_1");

		display_width = win_width;
		display_height=win_width/212*317;

		$("#display_area").width(display_width);
		$("#display_area").height(display_height);
	}
	else if ( win_height*0.75/317*212 <= win_width ) {
		console.log("height_max");
		console.log(win_width + ",  " + win_height);

		display_width = win_height*0.75/317*212;
		display_height = win_height*0.75;

		$("#display_area").height(display_height);
		$("#display_area").width(display_width);
	}
	else {
		console.log("width_max_2");
		console.log(win_width + ",  " + win_height);

		display_width = win_width;
		display_height=win_width/212*317;

		$("#display_area").width(display_width);
		$("#display_area").height(display_height);
	}

	$("#display_area").css("margin-top", (win_height - display_height)*0.3 + "px" );
	$("#display_area").css("margin-left", half_win_width - display_width/2 + "px" );

	$("#emp_img").width(display_width);
	$("#emp_img").height(display_height);

	top_height = (win_height - display_height)*0.3;
	foot_height = (win_height - display_height)*0.7;


	
	// ==================================== top component setting ===========================================
	$("#top").width(display_width).height(top_height).css("margin-left", half_win_width - display_width/2 + "px" );
	$("#page_info").width(display_width).height(top_height).css("line-height", top_height+ 10 + "px");
	
	$("#next_step_button").height(top_height*0.8).width(display_width*0.15)
						  .css("margin-top",top_height*0.1).css("margin-left",display_width*0.8 + "px");



	// ==================================== foot component setting ===========================================
	$("#foot").width(display_width);
	$("#foot").height(foot_height);
	$("#foot").css("margin-top",top_height + display_height + "px");
	$("#foot").css("margin-left", half_win_width - display_width/2 + "px");

	var animation_width = display_width*0.9;
	var animation_height = foot_height*0.4;

	
	$("#animation_line_div").width(animation_width).height(animation_height);
	$("#animation_line_div").css("margin-left",display_width*0.05 + "px");

	$('#effect').height(animation_height*0.5)
				.css("line-height", animation_height*0.7+ "px")
				.css("font-size", "14px")
				.css("margin-left", display_width*0.03 + "px")
				.css("margin-right", display_width*0.05 + "px");


	$('.animation_name', $("#animation_line_div") ).height(animation_height*0.5)
												   .css("line-height", animation_height*0.7+ "px")
												   .css("margin-left", display_width*0.03 + "px")
												   .css("font-size", "12px");

	$("#animation_line").width(animation_width).height(animation_height*0.5);

	var button_amount = 5 ;
	var button_size = foot_height*0.5;
	var button_margin = button_size*0.25;

	$("#foot_buttons_div").height(button_size).width(display_width*0.9).css("margin-left",display_width*0.05);
	$("#foot_button_container").width((button_size+button_margin)*button_amount).height(button_size);
	$( ".foot_buttons", $("#foot_button_container") ).width(button_size).height(button_size).css("margin-right", button_margin + "px" );
	console.log('================================================');
	console.log('button_size: '+button_size);
	$("#photo_upload_area").width(button_size).height(button_size).css("margin-right", button_margin + "px" );
	$("#upload_drop_area").width(button_size).height(button_size).css({"margin-right": button_margin + "px" ,"padding":0});

	$('#foot_button_container').mCustomScrollbar({
		mouseWheel:true,
		contentTouchScroll: true,
        scrollButtons:{
          enable:true,
		  scrollType:"continuous",
		  scrollSpeed: "auto",
		  scrollAmount: 15,
        }
	});
	
	window.display_rate = display_width/212;

	
	console.log('win_width:'+win_width+', win_height:'+win_height);
	console.log('display_width:'+display_width+', display_height:'+display_height);
	console.log('foot_height: '+foot_height);
	console.log('================================================');
}
/*
function FunctionButtonSetting() {
	$('#edit_button').cli
	
}
*/

function AnimationAppend(){
	for(var i=0; i<window.in_animation_name.length; i++){
		$('<div class="animation_name">'+ window.in_animation_name[i].chineseName +'</div>')
			.attr('id',window.in_animation_name[i].name)
			.appendTo($('#animation_line_div'));
	}
	
	$('<img src="/static/images/animation_line.png" id="animation_line"/>').appendTo($("#animation_line_div"));	

}



function GetUploadPhotos() {
	return $.ajax({
		type: 'POST',
		url: '/GetUploadPhotos/',
		dataType: 'json',
		data: { "pageId": window.pageId},
		success: function(JData) {
			if ( JData.error ) {
				alert(JData.error);
				return false;
			}
			else {
				window.photos = JData.photos;
				return true;
			}
		},
		error: function() {
			alert("ERROR!!");
			return false;
		}
	})
}

// ============================== zoom gesture @mobile ============================  updated by Paul Ku 2013/11/05
function ZoomGestureMobile (){
	// console.log('1. ZoomGestureMobile works.');
	$$('#photo_container_'+window.photo_index).pinchingOut(function(){
		if ( window.photos[window.photo_index].zoom <1.5 ){
			var pointer_left = parseInt( $('#zoom_line_pointer').css('left'), 10 )  + 10 ;
			$('#zoom_line_pointer').css( 'left' , pointer_left ); 
			PhotoZoomRatioPresent();
			var zoom = window.photos[ window.photo_index ].zoom;
			var left = ReadPhotoPositionLeft();
			var top = ReadPhotoPositionTop();
			PhotoContainerAdjust( window.photo_index );
			PhotoPositionAdjust( window.photo_index, left, top );
			SavePhotoPosition();
		}
	});

	$$('#photo_container_'+window.photo_index).pinchingIn(function(){
		if ( window.photos[window.photo_index].zoom >0.5 ){
			var pointer_left = parseInt( $('#zoom_line_pointer').css('left'), 10 )  - 10 ;
			$('#zoom_line_pointer').css( 'left' , pointer_left ); 
			PhotoZoomRatioPresent();
			var zoom = window.photos[ window.photo_index ].zoom;
			var left = ReadPhotoPositionLeft();
			var top = ReadPhotoPositionTop();
			PhotoContainerAdjust( window.photo_index );
			PhotoPositionAdjust( window.photo_index, left, top );
			SavePhotoPosition();
		}
	});
		
}


function UploadPhotoSetting(callback) {
	
		$.ajax({
		type: "POST",
		url: "/UploadPhotoSetting/",
		dataType: 'json',
		data: { "pageId" : window.pageId, "photos" : window.photos},
		success: function(JData) {
			if (JData.error) {
				alert(JData.error);
			}
			else {
				callback();
			}
		},
		error: function() {
			alert("ERROR!!");

		}
		
	})
	
}

function GetTextContent() {
	return $.ajax({
		type:'POST',
		dataType:'json',
		url:'/GetTextContent/',
		data:{"pageId": window.pageId},
		success: function(JData) {
			if ( JData.error ) {
				alert(JData.error);
				return false;
			}
			else {
				window.text=JData.text;
				return true;
			}
		},
		error: function() {
			alert("ERROR!!");
			return false;
		}
		
	})
}

function UploadText(callback) {
	window.text = $("#text_edit_div").val().replace(/\n/g,"<br>").replace(/ /g,"&nbsp").replace(/"/g,'&#34;').replace(/'/g,"&#39;");
	console.log("pageId: " + window.pageId + ",  Upload: " + '"' + window.text + '"');
	$.ajax({
		type: "POST",
		url: "/UploadText/",
		dataType: 'json',
		data: { "pageId" : window.pageId, "text" : window.text},
		success: function(JData) {
			if (JData.error) {
				alert(JData.error);
			}
			else {
				callback();
			}
		},
		error: function() {
			alert("ERROR!!");
		}
	})
}

// =================================================================================
// ========================= click icon functions ================================
function ClickIconFunction(){
	$('#edit_button').click(function(){
		console.log('edit_button works.  <-----------------');
		ZoomGestureMobile();
		PhotoDraggable();
	});
}




// =================================================================================
// =========================== photo append functions ==============================
function PhotoContainerAppend( index ) {

	var photo_container = $('<div id="photo_container_' + index + '" data-bind="css:{ noneDisplay: currentSettingPhoto() != ' + index + '}"></div>');
	$('#page_info').html( ( window.photo_index + 1 ).toString() + ' / ' + window.photos.length.toString() );
	
	var photo_obj = new Image();
	photo_obj.src = window.photos[index].url;
	$( photo_obj ).attr('id',index).css({'left':'0px','top':'0px','position':'relative'});
	photo_container.append(photo_obj).appendTo($('#display_area'));
	
	$(photo_obj).load( function() {
		var photo_id = $(this).attr('id');
		$(this).css("height",$("#display_area").height()*window.photos[parseInt(photo_id)].zoom);
		$(this).css("width","auto");
		PhotoContainerAdjust( photo_id );
		var ratio = window.photos[ parseInt( photo_id ) ].zoom;
		var left = window.photos[ parseInt( photo_id ) ].positionLeft;
		var top = window.photos[ parseInt( photo_id ) ].positionTop;
		PhotoPositionAdjust( parseInt( photo_id ), left, top );
		ko.applyBindings(viewModel);
	})
	
}

function PhotoContainerAdjust( index ){
	var zoom = window.photos[ index ].zoom ;
	var $container = $('#photo_container_'+index); 
	var $img = $('#'+index, $container);
	var container_width = $img.width()*2-$("#display_area").width();
	var container_height = $img.height()*2-$("#display_area").height();
	
	var target_width = $("#display_area").width();
	var target_height = $("#display_area").height();
	
	if ( container_width <= target_width ) {
		$container.css( "width", target_width );
		$container.css('margin-left', 0 );
	}
	else {
		$container.css( "width", container_width );
		$container.css('margin-left', -( container_width - target_width ) / 2 );
	}	
	if ( container_height <= target_height ) {
		$container.css( "height", target_height );
		$container.css('margin-top', 0 );
	}
	else {
		$container.css( "height", container_height );
		$container.css('margin-top', -( container_height - target_height ) / 2 );
	}
}

function PhotoPositionAdjust( index, left, top ){
	var ratio = window.photos[ index ].zoom;
	var img_width = $('#'+index,'#photo_container_'+index).width();
	var img_height = $('#'+index,'#photo_container_'+index).height();
	var div_width = $('#photo_container_'+index).width();
	var div_height = $('#photo_container_'+index).height();
	var photo_left = ( div_width - img_width )/2 + left*ratio ;
	var photo_top = ( div_height - img_height)/2 + top*ratio ;
	
	if ( img_width <= $("#display_area").width() ) {
		if ( photo_left < 0)
			photo_left = 0;
		else if ( photo_left > $("#display_area").width()-img_width )
			photo_left = $("#display_area").width()-img_width;
	}

	if ( img_height <= $("#display_area").height() ) {
		if ( photo_top < 0 )
			photo_top = 0;
		else if ( photo_top > $("#display_area").height()-img_height )
			photo_top = $("#display_area").height()-img_height;
	}
		
	$('#'+index,'#photo_container_'+index).css({'left':photo_left,'top':photo_top});
	
}


// =================================================================================
// ========================= Zoom setting functions ================================
function ReadPhotoZoomRatio(){
	return window.photos[window.photo_index].zoom;
}
function ZoomRatioSetting(){
	var zoom = ReadPhotoZoomRatio();
	$('#zoom_line_pointer').css('left', (( zoom - 1)*100 +55) );  
	$('#zoom_ratio_display').html( parseInt(zoom * 100) + '%' );  
}
function SavePhotoZoomRatio(){
	var left = parseInt(  $('#zoom_line_pointer').css('left') , 10 );
	window.photos[ window.photo_index ].zoom = (left - 55) / 100 + 1 ;  
	return left;
}
function PhotoZoomRatioPresent() {
	var left = SavePhotoZoomRatio();
	var $img = $('#'+window.photo_index,'#photo_container_'+window.photo_index);
	$('#zoom_ratio_display').html( parseInt(100 + ( left - 55 )) + '%' );
	
	
	$img.height( $("#center_panel").height()* window.photos[window.photo_index].zoom );
	$img.css( "width" , "auto");
}
function ZoomRatioReset() {
	SavePhotoZoomRatio();
	$('#zoom_ratio_display').html( '100%' );
	$('#zoom_line_pointer').css('left', 55);
}


// =================================================================================
// =========================== position setting functions ==========================
function PhotoMoveVertical(top) {
	
	var photo_top =  parseFloat( $("#" + window.photo_index).css("top") ) + top;
	var div_height = $("#photo_container_" + window.photo_index).height();
	var img_height = $("#"+window.photo_index, $("#photo_container_" + window.photo_index)).height();
	
	if ( img_height <= $("#center_panel").height() ) {
		if ( photo_top < 0 )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",0) ;
		else if ( photo_top > $("#center_panel").height()-img_height )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",$("#center_panel").height()-img_height) ;
		else
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",photo_top) ;
	}
	else {
		if ( photo_top < 0 )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",0) ;
		else if ( photo_top > div_height - img_height )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",div_height - img_height) ;
		else
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",photo_top) ;
	}
}
function PhotoMoveHorizontal(left) {
	
	var photo_left =  parseFloat( $("#" + window.photo_index).css("left") ) + left;
	var div_width = $("#photo_container_" + window.photo_index).width();
	var img_width = $("#"+window.photo_index, $("#photo_container_" + window.photo_index)).width();
	
	if ( img_width <= $("#center_panel").width() ) {
		if ( photo_left < 0 )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left", 0) ;
		else if ( photo_left > $("#center_panel").width()-img_width )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left", $("#center_panel").width()-img_width) ;
		else
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left",photo_left) ;
	}
	else {
		if ( photo_left < 0 )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left",0) ;
		else if ( photo_left > div_width - img_width )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left",div_width - img_width) ;
		else
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left",photo_left) ;
	}
}
function ReadPhotoPositionLeft() {
	return window.photos[window.photo_index].positionLeft;
}
function ReadPhotoPositionTop() {
	return window.photos[window.photo_index].positionTop;
}
function SavePhotoPosition()  {
	var index = window.photo_index;
	var ratio = window.photos[ index ].zoom;
	var left = parseFloat( $('#'+index, $('#photo_container_' + index)).css('left'));
	var top = parseFloat( $('#'+index, $('#photo_container_' + index)).css('top'));
	var img_width = $('#'+index,'#photo_container_'+index).width();
	var img_height = $('#'+index,'#photo_container_'+index).height();
	var div_width = $('#photo_container_'+index).width();
	var div_height = $('#photo_container_'+index).height();
	window.photos[index].positionLeft = ( left - (div_width - img_width)/2 ) / ratio ; 
	window.photos[index].positionTop = ( top - (div_height - img_height)/2 ) / ratio;
	console.log('==============='+index+'=========================');
	console.log("ratio: " + ratio + ", img_left: " + left + ", img_width: " + img_width + ", container_width: " + div_width + ", positionLeft: " + window.photos[index].positionLeft);
}
function PhotoDraggable (){
	for (i=0;i<window.photos.length;i++){
		$('#'+i,'#photo_container_'+i ).draggable({
			containment: '#photo_container_'+i,
			stop: function() {
				SavePhotoPosition();
			}
		});
	}
	PhotoEnableDraggable();
}
function PhotoEnableDraggable(){
	for (i = 0;i<window.photos.length;i++){
		$('#'+ i,'#photo_container_'+i ).draggable( 'enable' );
	}
}
function PhotoDisableDraggable(){
	for (i = 0;i<window.photos.length;i++){
		$('#'+ i,'#photo_container_'+i ).draggable( 'disable' ).css('opacity',1);
	}
}


// ======================================================================================
// ============================== in-animation setting functions ========================
function ReadStartAnimateSecond( index ) {
	var second = parseFloat( window.photos[ index ].startAnimateSeconds );
	console.log('@read: seconds='+second);
	return second;
}
function ReadStartAnimateName( index ) {
	var name = window.photos[index].startAnimate;
	return name;
}
function SaveStartAnimateSecond( second ) {
	window.photos[window.photo_index].startAnimateSeconds = second ;
}
function SaveStartAnimateName( name ) { 
	window.photos[window.photo_index].startAnimate = name ;
}
	/*======================================================= update by Paul Ku 2013/10/26  */  
function InCancelRestAll( obj ){
	for (var i = 0;i<window.in_animation_name.length;i++){
		$('input[name ='+window.in_animation_name[ i ]+']').prop('checked',false);
	}
	$('input[name=Random_in]').prop('checked',false);
	obj.prop('checked',true);
	return obj.prop('name');
}
function InAnimationReset( index ){
	var second = ReadStartAnimateSecond( index );
	var name = ReadStartAnimateName( index );
	InCancelRestAll( $('input[name='+name+']') );
	$('#in-animation_seconds').html(second);
	console.log('@in animation works. name: '+name);
}
	/*======================================================= update by Paul Ku 2013/10/26  */  


// ==========================================================================================
// ============================== display time setting function =============================
function ReadDisplaySeconds( index ){
	var display_sec = window.photos[ index ].displaySeconds;
	return display_sec;
}
function SaveDisplaySeconds( sec ){
	window.photos[ window.photo_index ].displaySeconds = sec;
}
/*======================================================= update by Paul Ku 2013/10/26  */ 
function DisplaySecondsReset ( index ){
	var display_sec =  ReadDisplaySeconds( index );
	$('#display-time_seconds').html(display_sec);
}
/*======================================================= update by Paul Ku 2013/10/26  */ 

// ==========================================================================================
// ============================== out-animation setting function ============================
function ReadEndAnimateSecond( index ) {
	var second = parseFloat( window.photos[ index ].endAnimateSeconds );
	console.log('@read: seconds='+second);
	return second;
}
function ReadEndAnimateName( index ) {
	var name = window.photos[index].endAnimate;
	return name;
}
function SaveEndAnimateSecond( second ) {
	window.photos[window.photo_index].endAnimateSeconds = second ;
}
function SaveEndAnimateName( name ) { 
	window.photos[window.photo_index].endAnimate = name ;
}
function OutCancelRestAll( obj ){
	for (var i = 0;i<window.out_animation_name.length;i++){
		$('input[name ='+window.out_animation_name[ i ]+']').prop('checked',false);
	}
	$('input[name=Random_out]').prop('checked',false);
	obj.prop('checked',true);
	return obj.prop('name');
}
function OutAnimationReset( index ){
	var second = ReadEndAnimateSecond( index );
	var name = ReadEndAnimateName( index );
	OutCancelRestAll( $('input[name='+name+']') );
	$('#out-animation_seconds').html(second);
	console.log('@out animation works. name: '+name);
}

// ========================================================================================
// =================================== CSRF Setting =======================================
// ========================================================================================

$( document ).ready(function() {

	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				// Does this cookie string begin with the name we want?
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	function csrfSafeMethod(method) {
		// these HTTP methods do not require CSRF protection
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
  
	function sameOrigin(url) {
		// test that a given url is a same-origin URL
		// url could be relative or scheme relative or absolute
		var host = document.location.host; // host + port
		var protocol = document.location.protocol;
		var sr_origin = '//' + host;
		var origin = protocol + sr_origin;
		// Allow absolute or scheme relative URLs to same origin
		return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
			(url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
			// or any other URL that isn't scheme relative or absolute i.e relative.
			!(/^(\/\/|http:|https:).*/.test(url));
	}
	  
	$.ajaxSetup({
		crossDomain: false, // obviates need for sameOrigin test
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type)) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		}
	});
});