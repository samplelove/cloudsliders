﻿window.para_obj = new Array();
window.para_obj = [ 
	{height:320,id:'#zoom-and-position',title:'Zoom/Position'},{height:320,id:'#in-animation',title:'In-animation'},
	{height:320,id:'#display-time',title:'Display-time'},{height:320,id:'#out-animation',title:'Out-animation'}
	];

//============================================== 2013/11/04 updated by Paul Ku 
$(window).load( function (){
	$(document).on('touchmove',function(e) {  //禁止拖拉畫面
		e.preventDefault();
	});

	/*====================================================*/


	/*====================================================*/
	window.currentState = "";
	ThemeSelectPanelDisplaySetting( window.themes );
	window.theme_selected_index = 0;
	
	if ($(window).width() >= 666 )
		window.mobile_layout = true;
	else
		window.mobile_layout = false;

	fixLayout();

	if ( /Android/i.test(navigator.userAgent) ) {
		setTimeout( "fixLayout()" ,200 );

		$( window ).on( "orientationchange", function( e ) {
			e.preventDefault();
			setTimeout( "fixLayout()" ,200 );
		});
	}
	else if ( /webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$( window ).on( "orientationchange", function( e ) {
			e.preventDefault();
			fixLayout();
		});
	}
	else {
		$(window).resize( function() {
			fixLayout();
		});
	}
	
});

function fixLayout() {
	
	window.temptimer++;

	var window_width = $(window).width();
	var window_height = $(window).height();

	$('body').width(window_width);
	$('body').height(window_height);
	$('body').css('margin',0);
	$('body').css('left',0).css('right',0).css('top',0).css('bottom',0);
	
	
	console.log( "=========" + temptimer + "=========" )
	console.log(window_width);
	console.log(window.mobile_layout);
	
	if( window_width < 666 ) {
	
		// mobile layout reset
	
		var half_win_width = window_width/2;
		var half_win_height = window_height/2;
		
		$('body').css("overflow","hidden");
		$('body').css("margin",0);
		

		// alert("[body] " + window.temptimer + ":  " + $('body').css('margin-left') + ",  " + $('body').css('left'));
		
		$("#panel").width(window_width);
		$("#panel").height(window_height);
		$("#panel").css("margin-top", 0);
		
		$("#outside").css("display","none");
		
		// ============================== center ====================================
		$("#center_panel").css("outline","solid red 1px");
		$("#center_panel").css("margin-left", half_win_width-106);
		$("#center_panel").css("margin-top", half_win_height-158);
		$("#center_panel").css("z-index", 999);		

		$('#previous_photo_mobile').css('opacity',0);
		$('#next_photo_mobile').css('opacity',0);
		// // ============================== top ====================================
		$("#top_panel").css("width",window_width);
		$("#top_panel").css("height", (window_height-317)/2);
		$("#top_panel").css("line-height", (window_height-317)/2 + "px");
		$("#top_panel").css("margin-left",0);
		$("#top_panel").css("margin-top", 0);	
		$("#top_panel").css("background", "#272627");
		$("#top_panel").css("opacity", 0.8);
		$("#text_preview").css("margin-top",(window_height-317)/2-35);
		$("#text_preview").css("margin-left",(window_width-212)/2);
		$("#text_edit").css("margin-top",(window_height-317)/2-35);
		
		
		// ============================== left ====================================
		
		$('#left_panel').height(317).width((window_width-212)/2);
		$('#left_panel').css({
			'background-color':'#272627',
			'opacity':0.8,
			'margin-left':0,
			'margin-top':(window_height-317)/2,
			'background-image':'none'
		});
		

		
		
		if ( window.photos.length == 0 ) {
			$("#photo_upload_area").width(window_width);
			$("#photo_upload_area").height(window_height);
			$("#photo_upload_area").css("margin-top",0);
			$("#photo_upload_area").css("margin-left",0);
			$("#photo_upload_area").css("background-image", "none");
			$("#photo_upload_area").css("background-color", "#272627");
			$("#photo_upload_area").css("opacity", "0.8");

			$("#upload_area_descript").css("display","block");
			if ( $("#upload_area_descript").length ) {
				$("#upload_drop_area").css("min-height",window_height-100)
									  .css("margin-top",50);
				$("#upload_area_descript").width($("#photo_upload_area").width()-46)
										  .css("margin-top",((window_height-100)/2-$("#upload_area_descript").height()/2))
										  .appendTo($("#upload_drop_area"));
			}
			else {
				$("#upload_drop_area").css("min-height",window_height-100)
									  .css("margin-top",50);
				$('<div id="upload_area_descript">click to upload photo</div>').width(window_width-46)
															.css("margin-top",(window_height-100)/2-$("#upload_area_descript").height()/2)
															.appendTo($("#upload_drop_area"));
			}
		}
		else {

			$('#photo_upload_area').css('background-color','')
                             	   .css('background-image','none')
                                   .css('opacity','')
                                   .css('margin-top',(window_height-317)/2)
                                   .css("margin-left", 0 );
			
			$('#photo_upload_area').css('width',$(window).width()/2-106);
			$("#photo_upload_area").height(317);

			$("#upload_area_descript").css("display","block");
			$("#upload_area_descript").html("upload").css('width', "")
									  .css("margin-top",((317-$("#upload_area_descript").width())/2)-50)
                    				  .css("margin-left", ( ($(window).width()-212)/2 - 84 )/2 -  $("#upload_area_descript").height()/2 );
    	}
		
		
		
		// ============================== right ====================================
		
			
		$('#right_panel').height(317).width((window_width-212)/2);
		$('#right_panel').css('margin-left',half_win_width+106);
		$('#right_panel').css({
			'background-color':'#272627',
			'opacity':0.8,
			'margin-top':(window_height-$('#left_panel').height())/2,
			'background-image':'none'
		});

		
		$('#out_para_panel_mobile').css({'width':window_width/2-106,'margin-left':window_width/2+106});
		$("#out_para_panel_mobile").css("height",317)
								   .css("margin-top",(window_height-317)/2)
								   .css('display','block');
		$('#para_panel_mobile').css('margin-left',$('#para_panel_mobile').width()/2 );
			
		
		
		for (var i=1;i>window.para_obj.length;i++){
			if ( window_width > 350 )
				$(para_obj[i].id+'_mobile').css({'margin-top':10,'margin-left':$('#para_panel_mobile').width()/2-30});
			else
				$(para_obj[i].id+'_mobile').css({'margin-top':10,'margin-left':0});
		}
		$('#para_panel_mobile').css('margin-top',-$('#para_panel_mobile').height()/2);
		$('#para_panel_mobile').css('margin-left',106);
		window.para_mobile_boolean = false;


		$('#para_setting_mobile').css('height',(window_height-317)/2);
		$('#para_setting_mobile').css('margin-top',-(window_height-317)/2);

		$('.para_mobile_window').css('margin-top',(window_height-317)/2);

		// ============================== bottom ====================================
		$("#bottom_panel").css("width",window_width);
		$("#bottom_panel").css("height", (window_height-317)/2);
		$("#bottom_panel").css("line-height", (window_height-317)/2 + "px");
		$("#bottom_panel").css("margin-left",0);
		$("#bottom_panel").css("margin-top", half_win_height+159);
		
		$("#bottom_panel").css("background", "#272627");
		$("#bottom_panel").css("opacity", 0.8);
		
			
			
		$("#confirm").css("width", window_width);
		$("#confirm").css("height", (window_height-317)/2);
		$("#confirm").css("line-height", (window_height-317)/2 + "px");
		$("#confirm").css("margin-left", 0);
		$("#confirm").css("margin-top", half_win_height+159);
		$("#confirm").css("background", "none");
		
		$('#previous_photo_button').css('margin-left', (window_width-$('#center_panel').width())/2-10);
		$('#previous_photo_button').css('margin-top',(window_height-$('#center_panel').height())/2+$('#center_panel').height());
		
		$('#next_photo_button').css('margin-left', (window_width-212)/2+103);
		$('#next_photo_button').css('margin-top',(window_height-317)/2+317 - 1.5);
		/*======= text_edit_button ======*/
		$('#text_confirm').css('background-image','none');
		$('#text_confirm').css('background-color','#272627');
		$('#text_confirm').css('opacity',0.8);
		$('#text_confirm').css('margin-left',(window_width-329)/2);
		/*======= text_edit_button ======*/

		
		// ============================ function button ============================
		
		$("#photo_upload_and_setting_button").width(60);
		$("#photo_upload_and_setting_button").css("margin-left", 0 );
		$("#photo_upload_and_setting_button").css("margin-top", 0 );
		$("#photo_upload_and_setting_button").css("background-image","url('/static/images/settingPages/mobileIcon/photo_function_mobile_button.png')");
		$("#photo_upload_and_setting_button").html("");
		
		$("#text_edited_button").width(60);
		$("#text_edited_button").css("margin-left", window_width - 60 );
		$("#text_edited_button").css("margin-top", 0 );
		$("#text_edited_button").css("background-image","url('/static/images/settingPages/mobileIcon/text_function_mobile_button.png')");
		$("#text_edited_button").html("");
		
		$("#vedio_upload_button").width(60);
		$("#vedio_upload_button").css("margin-left", 0 );
		$("#vedio_upload_button").css("margin-top", window_height - 60 );
		$("#vedio_upload_button").css("background-image","url('/static/images/settingPages/mobileIcon/video_block_mobile_button.png')");	
		$("#vedio_upload_button").html("");
		$("#vedio_button_block").css("display","none");
		
		$("#finish_button").width(60);
		$("#finish_button").css("margin-left", window_width - 60 );
		$("#finish_button").css("margin-top", window_height - 60 );
		$("#finish_button").css("background-image","url('/static/images/settingPages/mobileIcon/finish_function_mobile_button.png')");
		$("#finish_button").html("");
		
		$('#para_panel').css('display','none');
		
		$('.para_mobile_window')
			.css({'width':window_width,'height':(window_height-$('#center_panel').height())/2,'left':'50%','margin-left':-window_width/2});
		TweenMax.set($('.para_mobile_window'),{y:0});

		$('#para_setting_mobile').css('left','50%');
		$('#para_setting_mobile').css('margin-left',-$('#para_setting_mobile').width()/2);

		$('#in_mobile_time').width( 80+$('#in_minus_mobile').width()+$('#in_seconds_mobile').width()+$('#in_plus_mobile').width() ); 
		$('#in_mobile_time').css('margin-left',(window_width-$('#in_mobile_time').width())/2);
		$('#mobile_in_animation').css('margin-left',(window_width-$('#mobile_in_animation').width())/2);

		$('#out_mobile_time').width( 98+$('#out_minus_mobile').width()+$('#out_seconds_mobile').width()+$('#out_plus_mobile').width() ); 
		$('#out_mobile_time').css('margin-left',(window_width-$('#out_mobile_time').width())/2);
		$('#mobile_out_animation').css('margin-left',(window_width-$('#mobile_out_animation').width())/2);

		$('#previous_photo_mobile').css('opacity',0);
		$('#next_photo_mobile').css('opacity',0);
		
		if ( window.photos.length )
			InitialMobileAnimation();

		$('#display_mobile_time').width( 131+$('#display_minus_mobile').width()+$('#display_seconds_mobile').width()+$('#display_plus_mobile').width() ); 
		$('#display_mobile_time').css('margin-left',(window_width-$('#display_mobile_time').width())/2);
		$('#display_mobile_time').css('margin-top',($('.para_mobile_window').height()-$('#display_mobile_time').height())/2);
		//=========================
				
		window.mobile_layout = true;
		console.log("mobile layout reset");
	}
	else if ( window.mobile_layout ) {
	
		// pc layout reset
	
		if ( window_height > 533 )
			$("#panel").css("margin-top", window_height/2-267);
			
		$('body').css("overflow","");
		$('body').css("margin",0);
		
		$("#panel").width(666);
		$("#panel").height(533);
		if ( window_height > 533 )
			$("#panel").css("margin-top", window_height/2-267);
			
		$("#outside").css("display","block");
		
		// ============================== center ====================================
		$("#center_panel").css("outline","");
		$("#center_panel").css("margin-left", 227);
		$("#center_panel").css("margin-top", 108);
		$("#center_panel").css("z-index", "");

		$('#previous_photo_mobile').css('opacity',0);
		$('#next_photo_mobile').css('opacity',0);
		// ============================== top ====================================
		$("#top_panel").css("width",329);
		$("#top_panel").css("height", 84);
		$("#top_panel").css("line-height", "104px");
		$("#top_panel").css("margin-left", 169);
		$("#top_panel").css("margin-top", 14);
		$("#top_panel").css("background", "url('/static/images/top.png')");
		$("#top_panel").css("background-color","");
		$("#top_panel").css("opacity","");

		$("#text_preview").css("margin-top","");
		$("#text_preview").css("margin-left","");
		$("#text_edit").css("margin-top","");
		$("#text_edit").css("margin-left","");
		// ============================== left ====================================
		$("#left_panel").css("height","428px");
		$("#left_panel").css("width","");
		$("#left_panel").css("margin-left", 18);
		$("#left_panel").css("margin-top", 52);
		$("#left_panel").css("background", "url('/static/images/left_no_arrow.png')");
		$("#left_panel").css("background-color","");
		$("#left_panel").css("opacity","");

		$("#upload_area_descript").css("display","none");
		if( window.photos.length == 0 ) {
			$("#photo_upload_area").width(630);
			$("#upload_drop_area").css("min-height",420)
								  .css("margin-top","");
		}
		else
			$('#photo_upload_area').width(198);

		$("#photo_upload_area").height(395);
		$("#photo_upload_area").css("margin-top",70);
		$("#photo_upload_area").css("margin-left",20);
		$("#photo_upload_area").css("background", "");
		$("#photo_upload_area").css("background-image", "url('/static/images/photo_upload_area.png')");
		$("#photo_upload_area").css("opacity", "");
		
		
		
		// ============================== right ====================================
		$("#right_panel").css("height","428px");
		$("#right_panel").css("width","");
		$("#right_panel").css("margin-left", 449);
		$("#right_panel").css("margin-top", 52);
		$("#right_panel").css("background", "url('/static/images/right_no_arrow.png')");
		$("#right_panel").css("background-color","");
		$("#right_panel").css("opacity","");

		$('#para_panel').css('display','block');
		
		$('#out_para_panel_mobile').css('display','none');
		
		// ============================== bottom ====================================
		$("#bottom_panel").css("width",329);
		$("#bottom_panel").css("height", 82);
		$("#bottom_panel").css("line-height", "82px");
		$("#bottom_panel").css("margin-left", 175);
		$("#bottom_panel").css("margin-top", 433);
 		$("#bottom_panel").css("background", "url('/static/images/bottom.png')");
		$("#bottom_panel").css("background-color","");
		$("#bottom_panel").css("opacity","");

		$("#confirm").css("width",329);
		$("#confirm").css("height", 82);
		$("#confirm").css("line-height", "82px");
		$("#confirm").css("margin-left", 175);
		$("#confirm").css("margin-top", 433);
	 	$("#confirm").css("background", "url('/static/images/bottom.png')");
		
		$('#previous_photo_button').css('margin-left', '');
		$('#previous_photo_button').css('margin-top', '');
		$('#previous_photo_button').css('opacity', '');
		$('#previous_photo_button').css('visibility', '');
		
		$('#next_photo_button').css('margin-left', '');
		$('#next_photo_button').css('margin-top', '');
		$('#next_photo_button').css('opacity', '');
		$('#next_photo_button').css('visibility', '');
		/*======  text_exit_button ===============*/
		$('#text_confirm').css('background-image','');
		$('#text_confirm').css('background-color','');
		$('#text_confirm').css('opacity','');
		$('#text_confirm').css('margin-left','');
		/*======  text_exit_button ===============*/
		// ============================ function button ============================
		$("#photo_upload_and_setting_button").width(110).height(60);
		$("#photo_upload_and_setting_button").css("margin-left", 10 );
		$("#photo_upload_and_setting_button").css("margin-top", 10 );
		$("#photo_upload_and_setting_button").css("background-image","url('/static/images/function_button.png')");
		$("#photo_upload_and_setting_button").html("photo");
		
		$("#text_edited_button").width(110).height(60);
		$("#text_edited_button").css("margin-left", 546 );
		$("#text_edited_button").css("margin-top", 10 );
		$("#text_edited_button").css("background-image","url('/static/images/function_button.png')");
		$("#text_edited_button").html("text");
		
		$("#vedio_upload_button").width(110).height(60);
		$("#vedio_upload_button").css("margin-left", 15 );
		$("#vedio_upload_button").css("margin-top", 463 );
		$("#vedio_upload_button").css("background-image","url('/static/images/function_button.png')");
		$("#vedio_upload_button").html("video");
		$("#vedio_button_block").css("display", "block" );
		
		$("#finish_button").width(110).height(60);
		$("#finish_button").css("margin-left", 546 );
		$("#finish_button").css("margin-top", 463 );
		$("#finish_button").css("background-image","url('/static/images/function_button.png')");
		$("#finish_button").html("finish");
		
		window.mobile_layout = false;
		console.log("pc layout reset");
	}
	
	console.log( "======" + temptimer + " end ======\n\n\n" );
	
}

function FunctionButtonSetting() {
	$("#photo_upload_and_setting_button").css("display", "block");
	$("#text_edited_button").css("display", "block");
	$("#vedio_upload_button").css("display", "block");
	if ( window.mobile_layout )
		;
	else 
		$("#vedio_button_block").css("display", "block");
	$("#finish_button").css("display", "block");
	
	TweenMax.to( $("#photo_upload_and_setting_button"), 2, {opacity: 1} );
	TweenMax.to( $("#text_edited_button"), 2, {opacity: 1} );
	TweenMax.to( $("#vedio_upload_button"), 2, {opacity: 1} );
	TweenMax.to( $("#vedio_button_block"), 2, {opacity: 1} );
	TweenMax.to( $("#finish_button"), 2, {opacity: 1} );
	
	$("#photo_upload_and_setting_button").click( function() {
		if ( window.currentState != "photo_setting" ) {
			UploadText( function() {
				TextPanelClear();
				if ( GetUploadPhotos() ) {
					PhotoPanelDisplaySetting();
					window.currentState = "photo_setting";
					// fixLayout();
				} 
				else ;
			});
		}
		else ;
	})
	
	$("#text_edited_button").click( function(){
		if ( window.currentState != "text_setting" ) {
			UploadPhotoSetting( function() {
				PhotoPanelClear();
				if ( GetTextContent() ) {
					TextPanelDisplaySetting();
					window.currentState = "text_setting";
					// fixLayout();
				}
				else;
			});
		}
		else 
			;
	})
	
	$("#vedio_upload_button").click( function() {

	})
	
	$("#finish_button").click( function() {
		
		if ( window.currentState == "photo_setting" ) {
			UploadPhotoSetting(function() {
				PhotoPanelClear();
				FinishPanelDisplay();
				FunctionButtonClear();
			});
		}
		else if ( window.currentState == "text_setting" ) {
			UploadText( function() {
				TextPanelClear();
				FinishPanelDisplay();
				FunctionButtonClear();
			});
		}
		
	})
}  

function FunctionButtonClear() {

	$("#photo_upload_and_setting_button").unbind("click");
	$("#text_edited_button").unbind("click");
	$("#vedio_upload_button").unbind("click");
	$("#finish_button").unbind("click");
	
	TweenMax.to( $("#photo_upload_and_setting_button"), 2, {opacity: 0} );
	TweenMax.to( $("#text_edited_button"), 2, {opacity: 0} );
	TweenMax.to( $("#vedio_upload_button"), 2, {opacity: 0} );
	TweenMax.to( $("#vedio_button_block"), 2, {opacity: 0} );
	TweenMax.to( $("#finish_button"), 2, {opacity: 0} );
	
}

function ThemeSelectPanelDisplaySetting( item ) {
	var $container = $('<div id="img_container"></div>');
	var img_load = 0;
	
	for (var i = 0 ; i < item.length ; i ++ ){
		var imageObj = new Image();
		imageObj.src = item[i].url;
		$(imageObj).attr('id','image_'+i).attr("class","theme_img").appendTo($container);
		$container.appendTo('#center_panel');
		$(imageObj).load(function (){
			img_load ++;
			CheckImageLoadComplete();
		});
	}
	
	function CheckImageLoadComplete() {
		if ( img_load == item.length ){
			$container.css({ 'width': ( 320 + 20 ) * img_load });
			if (item.length > 1)
				$("#next_theme_button").css('display','block');
			else
				$("#next_theme_button").css('display','none');
			$('#top_panel').html( item[ window.theme_selected_index ].theme_name );
			ThemeSelectPanelActionSetting( $container );
			
		}
	}
}

function ThemeSelectPanelActionSetting ( item ) {
	var item_x_value = 0;
	$('#next_theme_button').click( function () {
		window.theme_selected_index++;
		if (item_x_value > - 232 * ( window.themes.length - 1 ) ){
			item_x_value = item_x_value - 232 ;
			TweenMax.to( item , 1 , {x: item_x_value,ease:'Power2.easeNone' } );
			$('#top_panel').html( window.themes[ window.theme_selected_index ].theme_name );
			if (item_x_value == - 232 * ( window.themes.length - 1 ) )
				$("#next_theme_button").css('display','none');
			else 
				$("#previous_theme_button").css('display','block');
		}
	});
	$('#previous_theme_button').click( function () {
		window.theme_selected_index--;
		if (item_x_value !== 0){
			item_x_value = item_x_value + 232 ;
			TweenMax.to( item , 1 , {x: item_x_value,ease:'Power2.easeNone' } );
			$('#top_panel').html( window.themes[ window.theme_selected_index ].theme_name );
			if (item_x_value == 0)
				$("#previous_theme_button").css('display','none');
			else 
				$("#next_theme_button").css('display','block');
		}
	});
	
	$('#confirm').click( function() {
		$.ajax({
			type: 'POST',
			url: '/SaveThemeSelected/',
			dataType: 'json',
			data: { "themeName" : window.themes[window.theme_selected_index].theme_name },
			success: function(JData) {
				if ( JData.error ) {
					alert(JData.error);
				}
				else {
					window.pageId = JData.pageId;
					ThemeSelectPanelClear();
					PhotoPanelDisplaySetting();
					FunctionButtonSetting();
					window.currentState = "photo_setting";
				}
			},
			error: function() {
				alert('ERROR!!');
			}
			
		})
	})
}

function ThemeSelectPanelClear() {
	// clear click action
	$("#previous_theme_button").unbind("click");
	$("#next_theme_button").unbind("click");
	$("#confirm").unbind("click");

	// clear display
	$("#top_panel").html("");
	$("#center_panel").html("");
	$("#previous_theme_button").css('display','none');
	$("#next_theme_button").css('display','none');
	$("#confirm").css('display','none');

}

function GetUploadPhotos() {
	return $.ajax({
		type: 'POST',
		url: '/GetUploadPhotos/',
		dataType: 'json',
		data: { "pageId": window.pageId},
		success: function(JData) {
			if ( JData.error ) {
				alert(JData.error);
				return false;
			}
			else {
				window.photos = JData.photos;
				return true;
			}
		},
		error: function() {
			alert("ERROR!!");
			return false;
		}
	})
}

function PhotoPanelDisplaySetting() {
	$("#photo_upload_area").css("display", "block");
	$("#previous_photo_button").css("display", "block");
	$("#next_photo_button").css("display", "block");

	
	//==================== mobile setting =========================Paul Ku Update 2013/11/04
	if ( $(window).width()<666 ){
		$('#out_para_panel_mobile').css('display','block');
	}
	else{ 
		$("#para_panel").css("display","block");
	}
	$('#next_photo_mobile').css('display','block');
	$('#previous_photo_mobile').css('display','block');

	TweenMax.set($('#previous_photo_button'),{autoAlpha:1});
	TweenMax.set($('#next_photo_button'),{autoAlpha:1});

	$('<div id="previous_photo_mobile"></div>').css({'margin-top':0,'margin-left':0}).appendTo('#center_panel');
	$('<div id="next_photo_mobile"></div>').css({'margin-top':0,'margin-left':$('#center_panel').width()-30}).appendTo('#center_panel');
	//====================  mobile setting  =========================Paul Ku Update 2013/11/04
	if ( window.photos.length == 0 )
		$('#top_panel').html( "0/0" );
	else {
		$('#top_panel').html( (window.photo_index+1) +"/"+ window.photos.length );
		for ( var i = 0; i < window.photos.length; i++ ) {
			PhotoContainerAppend(i);
		}
		viewModel.currentSettingPhoto(0);
	}
	
	
	// ===================== para_panel title setting ==================
	
	var para_obj = window.para_obj;
	for( var i = 0;i < para_obj.length; i++ ){
		var para_id = para_obj[i].id.slice( 1 , para_obj[i].length );
		
		$('<div id="'+para_id+'" class="para_window"></div>')
			.appendTo('#para_panel')
			.append($('<div id="'+para_id+'_panel" class="para_list_panel">'+para_obj[i].title+'</div>'));
			
		$('<span class="ui-icon ui-icon-circle-plus"></span>').css({'margin-top':-28,'margin-left':-20}).appendTo(para_obj[i].id+'_panel');
		$('<div class="mask" id="open_'+para_id+'"></div>').appendTo(para_obj[i].id+'_panel').css({'width':198,'height':40,'margin-top':-28,'margin-left':-16,'z-index':30,'position':'absolute','display':'block'});
		$('<div class="mask" id="close_'+para_id+'"></div>').appendTo(para_obj[i].id+'_panel').css({'width':198,'height':40,'margin-top':-28,'margin-left':-16,'z-index':30,'position':'absolute','display':'none'});
	}	
	
	$.each( para_obj, function( index ){
		var para_id = para_obj[index].id.slice( 1 , para_obj[index].length );
		
		$('#close_'+para_id).click( function(  ){
			$(para_obj[index].id).css('z-index',0);
			TweenMax.to( '.para_list_panel', 1,{autoAlpha:1,ease:'Linear.easeOut'} );
			TweenMax.to( para_obj[index].id, 1,{ height:64,marginTop: 0 ,ease:'Power2.easeOut' } );
			$(this).css('display','none');
			$('#open_'+para_id).css('display','block');
		});
		$('#open_'+para_id).click( function(){
			$(para_obj[index].id+'_panel').css('z-index',20);
			TweenMax.to( '.para_list_panel', 1,{autoAlpha:0,ease:'Linear.easeOut'} );
			TweenMax.to( para_obj[index].id+'_panel'  , 1,{ autoAlpha:1 ,ease:'Power2.easeOut'} );
			TweenMax.to( para_obj[index].id, 1,{ marginTop: -64 * index , autoAlpha:1,height:para_obj[index].height,ease:'Power2.easeOut' } );
			$(this).css('display','none');
			$('#close_'+para_id).css('display','block');
		});

	});
	//==========================================================================
	// ===================== para_panel@mobile  title setting ==================  //update by Paul Ku  2013/11/04 <------------
	//==========================================================================
	var window_width = $(window).width();
	var window_height = $(window).height(); 
	
	$('#para_setting_mobile')
		.width(window_width)
		.css({'left':'50%','margin-left':-window_width/2});
	
	for( var i = 1;i < para_obj.length; i++ ){
		console.log( 'para_mobile_append!!' );
		var para_id = para_obj[i].id.slice( 1 , para_obj[i].length );
		$('<div id="'+para_id+'_mobile" class="para_mobile_icon"></div>')
			.appendTo('#para_panel_mobile');
		if ( window_width > 350 )
			$(para_obj[i].id+'_mobile').css({'margin-top':10,'margin-left':$('#para_panel_mobile').width()/2-25});
		else
			$(para_obj[i].id+'_mobile').css({'margin-top':10,'margin-left':0});

		$('<div id="'+para_obj[i].title+'_mobile" class="para_mobile_window" ></div>')
			.css({'width':$('#panel').width(),'height':($('#panel').height()-$('#center_panel').height())/2,'margin-left':0,'margin-top':($('#panel').height()-$('#center_panel').height())/2 })
			.appendTo('#para_setting_mobile');
	}
	$('#para_panel_mobile').css('margin-top',-$('#para_panel_mobile').height()/2);
	
	//================ in-animation @mobile ====================================
	$('<div id="in_mobile_time"></div>')
		.height(30).appendTo('#In-animation_mobile');
	$('<label for="in-animation_mobile">In secs :</label>').appendTo('#in_mobile_time')
		.css({'color':'white','position':'relative','float':'left','font-size':20,'margin-top':5});
	$('<span id="in_minus_mobile" class="ui-icon ui-icon-minusthick"></span>').appendTo('#in_mobile_time')
		.css({'position':'relative','float':'left','margin-left':0,'margin-top':7});
	$('<div id="in_seconds_mobile"></div>').appendTo('#in_mobile_time');
	$('<span id="in_plus_mobile" class="ui-icon ui-icon-plusthick"></span>').appendTo('#in_mobile_time')
		.css({'position':'relative','float':'left','margin-left':0,'margin-top':7});
	$('<div id="mobile_in_animation"></div>').appendTo('#In-animation_mobile');

	$('<div id="mobile_in_animation_container"></div>').appendTo('#mobile_in_animation');
	$('<div class="mobile_animation_div">Random_in</div>').appendTo('#mobile_in_animation_container');
	for ( var i=0;i<window.in_animation_name.length;i++ ){
		$('<div class="mobile_animation_div">'+window.in_animation_name[i]+'</div>').appendTo('#mobile_in_animation_container');
	}
	$('#mobile_in_animation_container').width(9999).height(50);
	$('<div id="in_previous_animation_mobile"></div>').appendTo('#mobile_in_animation');
	$('<div id="in_next_animation_mobile"></div>').appendTo('#mobile_in_animation');

	$('#in_mobile_time').width( 80+$('#in_minus_mobile').width()+$('#in_seconds_mobile').width()+$('#in_plus_mobile').width() ); 
	$('#in_mobile_time').css('margin-left',(window_width-$('#in_mobile_time').width())/2);
	$('#mobile_in_animation').css('margin-left',(window_width-$('#mobile_in_animation').width())/2);
	//================ in-animation @mobile ====================================
	
	//================ display-time @mobile ====================================
	$('<div id="display_mobile_time"></div>')
		.height(30).appendTo('#Display-time_mobile');
	$('#display_mobile_time').css('margin-left',($('#panel').width()-$('#display_mobile_time').width())/2);
	$('<label for="display_time_mobile">Display secs :</label>').appendTo('#display_mobile_time')
		.css({'color':'white','position':'relative','float':'left','font-size':20,'margin-top':5});
	$('<span id="display_minus_mobile" class="ui-icon ui-icon-minusthick"></span>').appendTo('#display_mobile_time')
		.css({'position':'relative','float':'left','margin-left':0,'margin-top':9});
	$('<div id="display_seconds_mobile"></div>').appendTo('#display_mobile_time');
	$('<span id="display_plus_mobile" class="ui-icon ui-icon-plusthick"></span>').appendTo('#display_mobile_time')
		.css({'position':'relative','float':'left','margin-left':0,'margin-top':9});

	$('#display_mobile_time').width( 131+$('#display_minus_mobile').width()+$('#display_seconds_mobile').width()+$('#display_plus_mobile').width() ); 
	$('#display_mobile_time').css('margin-left',(window_width-$('#display_mobile_time').width())/2);
	$('#display_mobile_time').css('margin-top',($('.para_mobile_window').height()-$('#display_mobile_time').height())/2);
	//================ display-time @mobile ====================================
	
	//================ out-animation @mobile ====================================
	$('<div id="out_mobile_time"></div>')
		.css({'width':212,'height':30}).appendTo('#Out-animation_mobile');
	// $('#out_mobile_time').css('margin-left',($('#panel').width()-$('#out_mobile_time').width())/2);
	$('<label for="out-animation_mobile">Out secs :</label>').appendTo('#out_mobile_time')
		.css({'color':'white','position':'relative','float':'left','font-size':20,'margin-top':5});
	$('<span id="out_minus_mobile" class="ui-icon ui-icon-minusthick"></span>').appendTo('#out_mobile_time')
		.css({'position':'relative','float':'left','margin-left':0,'margin-top':7});
	$('<div id="out_seconds_mobile"></div>').appendTo('#out_mobile_time');
	$('<span id="out_plus_mobile" class="ui-icon ui-icon-plusthick"></span>').appendTo('#out_mobile_time')
		.css({'position':'relative','float':'left','margin-left':0,'margin-top':7});
	$('<div id="mobile_out_animation"></div>').appendTo('#Out-animation_mobile');
	// $('#mobile_out_animation').css('margin-left',($('#panel').width()-$('#mobile_out_animation').width())/2);
	$('<div id="mobile_out_animation_container"></div>').appendTo('#mobile_out_animation');
	$('<div class="mobile_animation_div">Random_out</div>').appendTo('#mobile_out_animation_container');
	for ( var i=0;i<window.in_animation_name.length;i++ ){
		$('<div class="mobile_animation_div">'+window.out_animation_name[i]+'</div>').appendTo('#mobile_out_animation_container');
	}
	$('#mobile_out_animation_container').width(9999).height(50);
	$('<div id="out_previous_animation_mobile"></div>').appendTo('#mobile_out_animation');
	$('<div id="out_next_animation_mobile"></div>').appendTo('#mobile_out_animation');

	$('#out_mobile_time').width( 98+$('#out_minus_mobile').width()+$('#out_seconds_mobile').width()+$('#out_plus_mobile').width() ); 
	$('#out_mobile_time').css('margin-left',(window_width-$('#out_mobile_time').width())/2);
	$('#mobile_out_animation').css('margin-left',(window_width-$('#mobile_out_animation').width())/2);
	
	//================ out-animation @mobile ====================================
	
	//==========================================================================
	// ===================== para_panel_mobile  title setting ==================  //update by Paul Ku  2013/11/04  <------------
	//==========================================================================
	
	// ==================== zoom panel setting ============================
	$('<div id="zoom_container"></div>').appendTo('#zoom-and-position_panel');
	$('<span id="zoom_minus" class="ui-icon ui-icon-minusthick"></span>').appendTo('#zoom_container')
		.css({'position':'absolute','margin-top':27,'margin-left':3,'float':'left'});
	$('<span id="zoom_plus" class="ui-icon ui-icon-plusthick"></span>').appendTo('#zoom_container')
		.css({'position':'absolute','margin-top':26,'margin-left':126,'float':'left'});
	$('<div id="zoom_line"></div>')
		.append('<img src="/static/images/zoom_line.png">')
		.appendTo('#zoom_container');
	$('<div id="zoom_line_pointer"></div>')
		.append('<img src="/static/images/zoom_line_pointer.png">').appendTo('#zoom_line')
		.draggable({ axis:'x',
					 grid:[10],
					containment: "#zoom_line",
					drag: function () { 
					PhotoZoomRatioPresent();
					var zoom = window.photos[ window.photo_index ].zoom;
					var left = ReadPhotoPositionLeft();
					var top = ReadPhotoPositionTop();
					PhotoContainerAdjust( window.photo_index );
					PhotoPositionAdjust( window.photo_index, left, top );
					SavePhotoPosition();
		}});
	if( $.browser.mozilla )
		$('<label for="zoom_ratio:">Zoom Ratio</label>').appendTo('#zoom_container').css({'font-size':16,'float':'left','margin-left':-141,'margin-top':43,'color':'white','position':'absolute'});
	else
		$('<label for="zoom_ratio:">Zoom Ratio</label>').appendTo('#zoom_container').css({'font-size':16,'float':'left','margin-left':0,'margin-top':43,'color':'white','position':'absolute'});
	$('<div id="zoom_ratio_display"></div>').appendTo('#zoom_container').html('100%');
	
	

	// =================== position panel setting ===============================
	$('<div id="position_div"></div>').appendTo('#zoom-and-position');
	$('<div id="position_left"></div>').appendTo('#position_div');
	$('<div id="position_top"></div>').appendTo('#position_div');
	$('<div id="position_right"></div>').appendTo('#position_div');
	$('<div id="position_bottom"></div>').appendTo('#position_div');
	$('<div id="position_center"></div>').appendTo('#position_div');
	
	
	// =====================in_animation panel setting =============================
	$('<div id="in-animation_div"></div>').appendTo('#in-animation_panel');
	$('<div id="in-animation_time"></div>').css('z-index',200).appendTo('#in-animation_div');
	$('<label for="in-animation_minus">In secs :</label>').appendTo('#in-animation_time')
		.css({'color':'white','position':'absolute','font-size':20});
	$('<span id="in-animation_minus" class="ui-icon ui-icon-minusthick"></span>').appendTo('#in-animation_time')
		.css({'position':'absolute','margin-left':70,'margin-top':13});
	$('<div id="in-animation_seconds"></div>').appendTo('#in-animation_time');
	$('<span id="in-animation_plus" class="ui-icon ui-icon-plusthick"></span>').appendTo('#in-animation_time')
		.css({'position':'absolute','margin-left':112,'margin-top':13});
	$('<div id="Random_in_div" class="in-animation_checkbox"></div>').appendTo('#in-animation_div')
		.css({'margin-top':5});
	$('<input type="checkbox" name="Random_in" checked="checked"></input><label for="Random_in" >Random</label>').appendTo('#Random_in_div')
		.css({'color':'white'});
	$('<div id="in-animation_line" ></div>').appendTo('#in-animation_div')
		.css({'background-color':'white','height':1,'width':120,'opacity':0.6,'border':'outset 1px'});
	$('<div id="in-animation_list"></div>').appendTo('#in-animation_div').css({'width':120,'height':140});
	

	/*================= malihu custom scrollbar =================================*/
	$('#in-animation_list').mCustomScrollbar({
		mouseWheel:true,
		contentTouchScroll: true,
        scrollButtons:{
          enable:false,
		  scrollType:"continuous",
		  scrollSpeed: "auto",
		  scrollAmount: 15,
        }
	});
	for (var i=0; i < window.in_animation_name.length; i++){
		var in_animation_name = window.in_animation_name[i];
		$('<div id="'+in_animation_name+'_in_div" class="in-animation_checkbox"></div>').appendTo($('.mCSB_container','#in-animation_list'));
		$('<input type="checkbox" name="'+in_animation_name+'" ></input><label for="'+in_animation_name+'" >'+in_animation_name+'</label>').appendTo('#'+in_animation_name+'_in_div')
			.css({'color':'white'});
	}
	$('#in-animation_list').mCustomScrollbar("update");
	
	
	//====================== display time panel setting =============================
	$('<div id="display-time_div"></div>').appendTo('#display-time_panel');
	$('<label for="display-time_minus">Sec:</label>').appendTo('#display-time_div')
		.css({'color':'white','position':'absolute','font-size':20});
	$('<span id="display-time_minus" class="ui-icon ui-icon-minusthick"></span>').appendTo('#display-time_div')
		.css({'position':'absolute','margin-left':45,'margin-top':13});
	$('<div id="display-time_seconds">3</div>').appendTo('#display-time_div');
	$('<span id="display-time_plus" class="ui-icon ui-icon-plusthick"></span>').appendTo('#display-time_div')
		.css({'position':'absolute','margin-left':89,'margin-top':13});

	
	// ===================== out_animation panel setting =============================
	$('<div id="out-animation_div"></div>').appendTo('#out-animation_panel');
	$('<div id="out-animation_time"></div>').appendTo('#out-animation_div');
	$('<label for="out-animation_minus">Out secs :</label>').appendTo('#out-animation_time')
		.css({'color':'white','position':'absolute'});
	$('<span id="out-animation_minus" class="ui-icon ui-icon-minusthick"></span>').appendTo('#out-animation_time')
		.css({'position':'absolute','margin-left':70,'margin-top':13});
	$('<div id="out-animation_seconds">2</div>').appendTo('#out-animation_time');
	$('<span id="out-animation_plus" class="ui-icon ui-icon-plusthick"></span>').appendTo('#out-animation_time')
		.css({'position':'absolute','margin-left':112,'margin-top':13});
	$('<div id="Random_out_div" class="out-animation_checkbox"></div>').appendTo('#out-animation_div')
		.css({'margin-top':5});
	$('<input type="checkbox" name="Random_out" checked="checked"></input><label for="Random_out" >Random</label>').appendTo('#Random_out_div')
		.css({'color':'white'});
	$('<div id="out-animation_line" ></div>').appendTo('#out-animation_div')
		.css({'background-color':'white','height':1,'width':120,'opacity':0.6,'border':'outset 1px'});
	$('<div id="out-animation_list"></div>').appendTo('#out-animation_div').css({'width':120,'height':140});
	
	
	/*================= malihu custom scrollbar =================================*/
	$('#out-animation_list').mCustomScrollbar({
		mouseWheel:true,
		contentTouchScroll: true,
        scrollButtons:{
          enable:false,
		  scrollType:"continuous",
		  scrollSpeed: "auto",
		  scrollAmount: 15,
        }
	});
	for (var i=0; i < window.out_animation_name.length; i++){
		var out_animation_name = window.out_animation_name[i];
		$('<div id="'+out_animation_name+'_out_div" class="out-animation_checkbox"></div>').appendTo($('.mCSB_container','#out-animation_list'));
		$('<input type="checkbox" name="'+out_animation_name+'" ></input><label for="'+out_animation_name+'" >'+out_animation_name+'</label>').appendTo('#'+out_animation_name+'_out_div')
			.css({'color':'white'});
	}
	$('#out-animation_list').mCustomScrollbar("update");
	
	// fixLayout
	PhotoPanelActionSetting();
}

function PhotoPanelActionSetting() {
	PhotoDraggable();
	// ============================== para control button click =============================
	$('#open_zoom-and-position').click(function(){});
	$('#close_zoom-and-position').click(function(){});
	$('#open_in-animation').click(function(){
		InAnimationReset( window.photo_index );
	});
	$('#close_in-animation').click(function(){});
	$('#open_display-time').click(function(){
		DisplaySecondsReset ( window.photo_index );
	});
	$('#close_display-time').click(function(){});
	$('#open_out-animation').click(function(){
		OutAnimationReset( window.photo_index );
	});
	$('#close_out-animation').click(function(){});
	
	$.each(window.para_obj, function( index,item ){
		var mask_name = (item.id).slice(1,(item.id).length);
		$('#open_'+mask_name,$( item.id+'_panel')).click(function(){
			$('.ui-icon-circle-plus', $( item.id+'_panel' ) ).toggleClass('ui-icon-circle-plus',false).toggleClass('ui-icon-circle-minus',true);
		});
		$('#close_'+mask_name,$( item.id+'_panel')).click(function(){
			$('.ui-icon-circle-minus',$( item.id+'_panel' ) ).toggleClass('ui-icon-circle-plus',true).toggleClass('ui-icon-circle-minus',false);
		});
	});
	
	
	// ========================== Turn photo button click ===========================
	var photos_x_value = 0;
	$("#previous_photo_button,#previous_photo_mobile").click(function() {
		if ( window.photo_index > 0 ){
			ZoomRatioReset();  //   Zoon Function
			
			//=========================turn page
			window.photo_index -- ;
			var index = window.photo_index;
			var zoom = window.photos[ index ].zoom;
			var left= window.photos[ index ].positionLeft;
			var top = window.photos[ index ].positionTop;
			
			viewModel.currentSettingPhoto(window.photo_index);
			PhotoPositionAdjust( index, left, top );
			ZoomRatioSetting();
			/*================================================================================================= update by Paul Ku 2013/10/26  */  
			InAnimationReset( window.photo_index );
			DisplaySecondsReset ( window.photo_index );
			OutAnimationReset( window.photo_index );
			InitialMobileAnimation();
			ZoomGestureMobile();
			/*================================================================================================= update by Paul Ku 2013/10/26  */  
			$('#top_panel').html( (window.photo_index + 1).toString() + ' / ' + window.photos.length.toString());
		}
	});
	$("#next_photo_button,#next_photo_mobile").click(function() {		
		if (window.photo_index < window.photos.length - 1 ){
			ZoomRatioReset();	// Zoon Function
			
			//=========================turn page
			window.photo_index ++ ;
			
			var index = window.photo_index;
			var zoom = window.photos[ index ].zoom;
			var left= window.photos[ index ].positionLeft;
			var top = window.photos[ index ].positionTop;
			
			viewModel.currentSettingPhoto(window.photo_index);
			PhotoPositionAdjust( index, left, top );
			ZoomRatioSetting();
			/*================================================================================================= update by Paul Ku 2013/10/26  */  
			InAnimationReset( window.photo_index );
			DisplaySecondsReset ( window.photo_index );
			OutAnimationReset( window.photo_index );
			InitialMobileAnimation();
			ZoomGestureMobile();
			/*================================================================================================= update by Paul Ku 2013/10/26  */  
			$('#top_panel').html( (window.photo_index + 1).toString() + ' / ' + window.photos.length.toString());
		}
	});
	
	
	// ========================= zoom panel button click ============================ 
	$('#zoom_minus').on('click',function(){
		if ( window.photos[window.photo_index].zoom > 0.5 ){
			var pointer_left = parseInt( $('#zoom_line_pointer').css('left'), 10 )  - 10 ;
			$('#zoom_line_pointer').css( 'left' , pointer_left ); 
			PhotoZoomRatioPresent();
			var zoom = window.photos[ window.photo_index ].zoom;
			var left = ReadPhotoPositionLeft();
			var top = ReadPhotoPositionTop();
			PhotoContainerAdjust( window.photo_index );
			PhotoPositionAdjust( window.photo_index, left, top );
			SavePhotoPosition();
			//console.log('click.');
		}
	});
	$('#zoom_plus').on('click',function(){
		if ( window.photos[window.photo_index].zoom < 1.5 ){
			var pointer_left = parseInt( $('#zoom_line_pointer').css('left'), 10 )  + 10 ;
			$('#zoom_line_pointer').css( 'left' , pointer_left ); 
			var zoom = window.photos[ window.photo_index ].zoom;
			var left = ReadPhotoPositionLeft();
			var top = ReadPhotoPositionTop();
			PhotoZoomRatioPresent();
			PhotoContainerAdjust( window.photo_index );
			PhotoPositionAdjust( window.photo_index, left, top );
			SavePhotoPosition();
			//console.log('click.');
		}
	});

	
	// ======================== position panel button click =========================
	$('#position_top').mousedown( function() {
		PhotoMoveVertical(-5);
		clearInterval(window.intevalId);
		window.intevalId = setInterval( function() { PhotoMoveVertical(-5); } , 100);
	}).bind('mouseup', function() {
		clearInterval(window.intevalId);
		SavePhotoPosition();
	});
	
	$('#position_bottom').mousedown( function() {
		PhotoMoveVertical(5);
		clearInterval(window.intevalId);
		window.intevalId = setInterval( function() { PhotoMoveVertical(5); } , 100);
	}).bind('mouseup', function() {
		clearInterval(window.intevalId);
		SavePhotoPosition();
	});
	
	$('#position_right').mousedown( function() {
		PhotoMoveHorizontal(5);
		clearInterval(window.intevalId);
		window.intevalId = setInterval( function() { PhotoMoveHorizontal(5); } , 100);
	}).bind('mouseup', function() {
		clearInterval(window.intevalId);
		SavePhotoPosition();
	});
	
	$('#position_left').mousedown( function() {
		PhotoMoveHorizontal(-5);
		clearInterval(window.intevalId);
		window.intevalId = setInterval( function() { PhotoMoveHorizontal(-5); } , 100);
	}).bind('mouseup', function() {
		clearInterval(window.intevalId);
		SavePhotoPosition();
	});
	
	$('#position_center').on('click',function(){
		clearInterval(window.intevalId);
		PhotoPositionAdjust( window.photo_index, 0, 0 );
		SavePhotoPosition();
		console.log('center click.Left:'+ parseInt( $('#'+window.photo_index).css('left'),10));
	});
	

	// ============================== in-animation button click ============================= 
	$('#in-animation_minus,#in_minus_mobile').click(function(){
		var sec = parseFloat( ReadStartAnimateSecond( window.photo_index ) );
		if ( sec <= 5 && sec > 0 ){
			sec = sec -0.5;
			SaveStartAnimateSecond( sec );
			$('#in-animation_seconds').html(sec);
			$('#in_seconds_mobile').html( sec );
		}
	});
	$('#in-animation_plus,#in_plus_mobile').click(function(){
		var sec = parseFloat( ReadStartAnimateSecond( window.photo_index ) );
		if ( sec < 5 && sec >= 0 ){
			sec = sec +0.5;
			SaveStartAnimateSecond( sec );
			$('#in-animation_seconds').html(sec);
			$('#in_seconds_mobile').html( sec );
		}
		
	});
	$('input[name = Random_in]').click(function(){
			var name = InCancelRestAll( $(this) );
			SaveStartAnimateName( name );
	});
	
	for (var i = 0 ; i<window.in_animation_name.length;i++) {
		$('input[name='+window.in_animation_name[i]+']').click(function(){
				var name = InCancelRestAll( $(this) );
				SaveStartAnimateName( name );
				var index = window.photo_index;
				var width = $('#center_panel').width();
				var height = $('#center_panel').height();
				ClidersAnimate( $('#center_panel'),$('#photo_container_'+index) , 'null' , name, ReadStartAnimateSecond(index), -width, 0, -height, 0, 1, 1 );
		});
	}
	

	// ============================== display second button click ============================= 
	$('#display-time_minus,#display_minus_mobile').click(function(){
		var sec = parseFloat( ReadDisplaySeconds( window.photo_index ) );
		if ( sec <= 7 && sec > 0 ){
			sec = sec -0.5;
			SaveDisplaySeconds( sec );
			$('#display-time_seconds').html(sec);
			$('#display_seconds_mobile').html(sec);
		}
		
	});
	$('#display-time_plus,#display_plus_mobile').click(function(){
		var sec = parseFloat( ReadDisplaySeconds( window.photo_index ) );
		if ( sec < 7 && sec >= 0 ){
			sec = sec +0.5;
			SaveDisplaySeconds( sec );
			$('#display-time_seconds').html(sec);
			$('#display_seconds_mobile').html(sec);
		}
		
	});
	
	
	// ============================== out-animation button click ============================
	var current_select_animation = 'Random';
	
	$('#out-animation_minus,#out_minus_mobile').click(function(){
		var sec = parseFloat( ReadEndAnimateSecond( window.photo_index ) );
		if ( sec <= 5 && sec > 0 ){
			sec = sec -0.5;
			SaveEndAnimateSecond( sec );
			$('#out-animation_seconds').html(sec);
			$('#out_seconds_mobile').html( sec );
		}
		
	});
	$('#out-animation_plus,#out_plus_mobile').click(function(){
		var sec = parseFloat( ReadEndAnimateSecond( window.photo_index ) );
		if ( sec < 5 && sec >= 0 ){
			sec = sec +0.5;
			SaveEndAnimateSecond( sec );
			$('#out-animation_seconds').html(sec);
			$('#out_seconds_mobile').html( sec );
		}
		
	});
	$('input[name = Random_out]').click(function(){
			var name = OutCancelRestAll( $(this) );
			SaveEndAnimateName( name );
	});
	
	for (var i = 0 ; i<window.out_animation_name.length;i++) {
		$('input[name='+window.out_animation_name[i]+']').click(function(){
			var name = OutCancelRestAll( $(this) );
			SaveEndAnimateName( name );
			var index = window.photo_index;
			var width = $('#center_panel').width();
			var height = $('#center_panel').height();
			ClidersAnimate( $('#center_panel'),$('#photo_container_'+index) , 'null' , name, ReadEndAnimateSecond(index), 0, width, 0, height, 1, 1 );
		});
	}
	// ============================== para_panel button @mobile =====================================  updated by Paul Ku 2013/11/05
	var target_id = '';
	var para_mobile_boolean = window.para_mobile_boolean;
	$('#in-animation_mobile','#para_panel_mobile').click(function(){
		$('#in_seconds_mobile').html( ReadStartAnimateSecond( window.photo_index ) );
		if ( target_id !== $(this).attr('id') ){
			target_id = $(this).attr('id');
			$('#In-animation_mobile','#para_setting_mobile').css('display','block');
			ParaPanelMobile( $(this) );
		}
		else {
			if ( para_mobile_boolean ){
				ParaPanelMobile( $(this) );
				para_mobile_boolean = false;
				window.paramobile_boolean = para_mobile_boolean;
			}
			else{
				TweenMax.to($('#In-animation_mobile','#para_setting_mobile'),2,{y:0,ease:'Power2.easeNone',onStart:NonDisplayRearrangeButtons});
				para_mobile_boolean = true;
				window.paramobile_boolean = para_mobile_boolean;
				target_id = '';
				console.log('target_id: '+target_id);
			}
		}
		console.log('para_mobile_boolean: '+para_mobile_boolean);
			
	});
	$('#display-time_mobile','#para_panel_mobile').click(function(){
		$('#display_seconds_mobile').html( ReadDisplaySeconds( window.photo_index ) );
		if ( target_id !== $(this).attr('id') ){
			target_id = $(this).attr('id');
			$('#Display-time_mobile','#para_setting_mobile').css('display','block');
			ParaPanelMobile( $(this) );
		}
		else {
			if ( para_mobile_boolean ){
				ParaPanelMobile( $(this) );
				para_mobile_boolean = false;
				window.paramobile_boolean = para_mobile_boolean;
			}
			else{
				TweenMax.to($('#Display-time_mobile','#para_setting_mobile'),2,{y:0,ease:'Power2.easeNone',onStart:NonDisplayRearrangeButtons});
				para_mobile_boolean = true;
				window.paramobile_boolean = para_mobile_boolean;
				console.log('target_id: '+target_id);
			}
		}
		console.log('para_mobile_boolean: '+para_mobile_boolean);
	});
	$('#out-animation_mobile','#para_panel_mobile').click(function(){
		$('#out_seconds_mobile').html( ReadEndAnimateSecond( window.photo_index ) );
		if ( target_id !== $(this).attr('id') ){
			target_id = $(this).attr('id');
			$('#Out-animation_mobile','#para_setting_mobile').css('display','block');
			ParaPanelMobile( $(this) );
		}
		else{
			if ( para_mobile_boolean ){
				ParaPanelMobile( $(this) );
				para_mobile_boolean = false;
				window.paramobile_boolean = para_mobile_boolean;
				DisplayRearrangeButtons();
			}
			else{
				TweenMax.to($('#Out-animation_mobile','#para_setting_mobile'),2,{y:0,ease:'Power2.easeNone',onStart:NonDisplayRearrangeButtons});
				para_mobile_boolean = true;
				window.paramobile_boolean = para_mobile_boolean;
				SaveEndAnimateName( out_animate_text ); 
				console.log('target_id: '+target_id);
			}
		}
		console.log('para_mobile_boolean: '+para_mobile_boolean);
	});
	function ParaPanelMobile( target ){
		var y_up = $('.para_mobile_window').height();
		TweenMax.to( $('.para_mobile_window' ), 1,{y:0,ease:'Power2.easeNone'});
		if ( $(target).attr('id') == 'in-animation_mobile' )
			TweenMax.to($('#In-animation_mobile','#para_setting_mobile'),2,{y:-y_up,ease:'Power2.easeNone'});
		else if ( $(target).attr('id') == 'display-time_mobile' )
			TweenMax.to($('#Display-time_mobile','#para_setting_mobile'),2,{y:-y_up,ease:'Power2.easeNone'});
		else if ( $(target).attr('id') == 'out-animation_mobile' )
			TweenMax.to($('#Out-animation_mobile','#para_setting_mobile'),2,{y:-y_up,ease:'Power2.easeNone'});
		DisplayRearrangeButtons();
		para_mobile_boolean = false;
		window.paramobile_boolean = para_mobile_boolean;
	}
	function DisplayRearrangeButtons(){
		TweenMax.to( [$('#previous_photo_button'),$('#next_photo_button')],1,{autoAlpha:0,ease:'Power2.easeNone'} );
		TweenMax.to( [$('#previous_photo_mobile'),$('#next_photo_mobile')],1,{display:'block',autoAlpha:1,ease:'Power2.easeNone'} );
	}
	function NonDisplayRearrangeButtons(){
		TweenMax.to( [$('#previous_photo_button'),$('#next_photo_button')],1,{autoAlpha:1,ease:'Power2.easeNone'} );
		TweenMax.to( [$('#previous_photo_mobile'),$('#next_photo_mobile')],1,{display:'none',autoAlpha:0,ease:'Power2.easeNone'} );
	}
	// ============================== zoom gesture @mobile ============================  updated by Paul Ku 2013/11/05
	ZoomGestureMobile();
	// ============================== in-animation button click  @mobile ============================  updated by Paul Ku 2013/11/05

	var in_animate_text = 'Random_in';
	window.in_animate_index = -1;
	window.in_mobile_x = 0
	$('#in_previous_animation_mobile').click(function(){
		console.log('window.in_mobile_x: '+window.in_mobile_x);
		window.in_mobile_x = previous_in_animation_mobile ( window.in_mobile_x );
	});
	$('#mobile_in_animation').on('swipeRight',function(){
		window.in_mobile_x = previous_in_animation_mobile ( window.in_mobile_x );
	});
	function previous_in_animation_mobile ( in_mobile_x ){
		var container =  $('#mobile_in_animation_container');
		if ( in_mobile_x < 0 ){
			in_mobile_x = in_mobile_x + 212;
			console.log('window.in_mobile_x: '+window.in_mobile_x);
			window.in_animate_index = window.in_animate_index -1;
			TweenMax.to( container,1,{x: in_mobile_x,onComplete:CurrentInAnimation,onCompleteParams:[window.in_animate_index],ease:'Power2.easeOut'} );	
			InAnimationButtonSetting ( in_mobile_x );
		}
		else
			;
		return in_mobile_x;
	}
	$('#in_next_animation_mobile').click(function(){
		console.log('window.in_mobile_x@click: '+window.in_mobile_x);
		window.in_mobile_x = next_in_animation_mobile ( window.in_mobile_x );
	});
	$('#mobile_in_animation').on('swipeLeft',function(){
		window.in_mobile_x = next_in_animation_mobile ( window.in_mobile_x );
	});
	function next_in_animation_mobile ( in_mobile_x ){
		var container =  $('#mobile_in_animation_container');
		if ( in_mobile_x > -212*( in_animation_name.length ) ){
			in_mobile_x = in_mobile_x - 212;
			console.log('window.in_mobile_x@function : '+window.in_mobile_x);
			in_animate_index = in_animate_index +1;
			TweenMax.to( container,1,{x:in_mobile_x,onComplete:CurrentInAnimation,onCompleteParams:[window.in_animate_index],ease:'Power2.easeOut'} );
			InAnimationButtonSetting ( in_mobile_x );
		}
		else
			;
		return in_mobile_x;		
	}

	function CurrentInAnimation( index ){
		var width = $('#center_panel').width();
		var height = $('#center_panel').height();
		if ( index == -1 )
			in_animate_text = 'Random_in';
		else{
			in_animate_text = window.in_animation_name[ index ];
			ClidersAnimate( $('#center_panel'),$('#photo_container_'+window.photo_index) , 'null' , in_animate_text, ReadStartAnimateSecond(window.photo_index), -width, 0, -height, 0, 1, 1 );
		}
		console.log('on complete. animation is '+in_animate_text );		
		SaveStartAnimateName( in_animate_text ); 
	}
	// ============================== out-animation button click  @mobile ============================  updated by Paul Ku 2013/11/05
	
	var out_animate_text = 'Random_out';
	window.out_animate_index = -1;
	window.out_mobile_x = 0;
	$('#mobile_out_animation').on('swipeRight',function(){
		window.out_mobile_x = previous_out_animation_mobile( window.out_mobile_x );
	});
	$('#out_previous_animation_mobile').click(function(){
		window.out_mobile_x = previous_out_animation_mobile( window.out_mobile_x );
	});
	function previous_out_animation_mobile ( out_mobile_x ){
		var container =  $('#mobile_out_animation_container');
		if ( out_mobile_x < 0 ){
			out_mobile_x = out_mobile_x + 212;
			// console.log('window.in_mobile_x: '+window.in_mobile_x);
			window.out_animate_index = window.out_animate_index -1;
			TweenMax.to( container,1,{x:out_mobile_x,onComplete:CurrentOutAnimation,onCompleteParams:[window.out_animate_index],ease:'Power2.easeOut'} );	
			OutAnimationButtonSetting ( out_mobile_x );
		}
		else
			;
		return out_mobile_x;
	}
	$('#mobile_out_animation').on('swipeLeft',function(){
		window.out_mobile_x  = next_out_animation_mobile( window.out_mobile_x );
	});
	$('#out_next_animation_mobile').click(function(){
		window.out_mobile_x  = next_out_animation_mobile( window.out_mobile_x );
	});
	function next_out_animation_mobile ( out_mobile_x ){
		var container =  $('#mobile_out_animation_container');
		if ( out_mobile_x > -212*( window.out_animation_name.length ) ){
			out_mobile_x = out_mobile_x - 212;
			// console.log('window.in_mobile_x@function : '+window.in_mobile_x);
			out_animate_index = out_animate_index +1;
			TweenMax.to( container,1,{x:out_mobile_x,onComplete:CurrentOutAnimation,onCompleteParams:[window.out_animate_index],ease:'Power2.easeOut'} );
			OutAnimationButtonSetting ( out_mobile_x );
		}
		else
			;
		return out_mobile_x;		
	}
	function CurrentOutAnimation( index ){
		var width = $('#center_panel').width();
		var height = $('#center_panel').height();
		if ( index == -1 )
			out_animate_text = 'Random_out';
		else{
			out_animate_text = window.out_animation_name[ index ];
			ClidersAnimate( $('#center_panel'),$('#photo_container_'+window.photo_index) , 'null' , out_animate_text, ReadEndAnimateSecond(window.photo_index), 0, width, 0, height, 1, 1 );
		}
		console.log('on complete. animation is '+index+out_animate_text );
		SaveEndAnimateName( out_animate_text ); 
	}
}
// ============================== zoom gesture @mobile ============================  updated by Paul Ku 2013/11/05
function ZoomGestureMobile (){
	// console.log('1. ZoomGestureMobile works.');
	$$('#photo_container_'+window.photo_index).pinchingOut(function(){
		if ( window.photos[window.photo_index].zoom <1.5 ){
			var pointer_left = parseInt( $('#zoom_line_pointer').css('left'), 10 )  + 10 ;
			$('#zoom_line_pointer').css( 'left' , pointer_left ); 
			PhotoZoomRatioPresent();
			var zoom = window.photos[ window.photo_index ].zoom;
			var left = ReadPhotoPositionLeft();
			var top = ReadPhotoPositionTop();
			PhotoContainerAdjust( window.photo_index );
			PhotoPositionAdjust( window.photo_index, left, top );
			SavePhotoPosition();
		}
	});

	$$('#photo_container_'+window.photo_index).pinchingIn(function(){
		if ( window.photos[window.photo_index].zoom >0.5 ){
			var pointer_left = parseInt( $('#zoom_line_pointer').css('left'), 10 )  - 10 ;
			$('#zoom_line_pointer').css( 'left' , pointer_left ); 
			PhotoZoomRatioPresent();
			var zoom = window.photos[ window.photo_index ].zoom;
			var left = ReadPhotoPositionLeft();
			var top = ReadPhotoPositionTop();
			PhotoContainerAdjust( window.photo_index );
			PhotoPositionAdjust( window.photo_index, left, top );
			SavePhotoPosition();
		}
	});
		
}

function InitialMobileAnimation(){
	var start_name = ReadStartAnimateName( window.photo_index );
	var end_name = ReadEndAnimateName( window.photo_index );
	var start_sec = ReadStartAnimateSecond( window.photo_index );
	var end_sec = ReadEndAnimateSecond( window.photo_index );
	var display_sec = ReadDisplaySeconds ( window.photo_index );
	$('#in_seconds_mobile').html( start_sec );
	$('#display_seconds_mobile').html( display_sec );
	$('#out_seconds_mobile').html( end_sec );
	if ( start_name == 'Random_in' ){
		TweenMax.set($('#mobile_in_animation_container'),{x:0});
		InAnimationButtonSetting ( 0 );
		window.in_mobile_x = 0;
		window.in_animate_index = -1;
	}	
	else{
		for ( var i=0;i<window.in_animation_name.length;i++ ){
			if ( start_name == window.in_animation_name[ i ] ){
				window.in_mobile_x = -212*(i+1);
				window.in_animate_index = i;
				TweenMax.set($('#mobile_in_animation_container'),{x: -212*(i+1) });
				InAnimationButtonSetting ( -212*(i+1) );
			}
		}
	}

	if ( end_name == 'Random_out' ){
		TweenMax.set($('#mobile_out_animation_container'),{x:0});
		OutAnimationButtonSetting ( 0 );
		window.out_mobile_x = 0;
		window.out_animate_index = -1;
	}
	else{
		for ( var i=0;i<window.out_animation_name.length;i++ ){
			if ( end_name == window.out_animation_name[ i ] ){
				window.out_mobile_x = -212*(i+1);
				window.out_animate_index = i;
				TweenMax.set($('#mobile_out_animation_container'),{x: -212*(i+1) });
				OutAnimationButtonSetting ( -212*(i+1) );
			}
		}
	}
}
function InAnimationButtonSetting ( x ){
	if( x >= 0 ){
		$('#in_previous_animation_mobile').css('display','none');
		$('#in_next_animation_mobile').css('display','block');	
	}
	else if ( x<= -212*window.in_animation_name.length ){
		$('#in_next_animation_mobile').css('display','none');
		$('#in_previous_animation_mobile').css('display','block');
	}
	else{
		$('#in_previous_animation_mobile').css('display','block');	
		$('#in_next_animation_mobile').css('display','block');
	}
}
function OutAnimationButtonSetting ( x ){
	if( x >= 0 ){
		$('#out_previous_animation_mobile').css('display','none');
		$('#out_next_animation_mobile').css('display','block');
	}
	else if ( x<= -212*window.out_animation_name.length ){
		$('#out_next_animation_mobile').css('display','none');
		$('#out_previous_animation_mobile').css('display','block');
	}
	else{
		$('#out_previous_animation_mobile').css('display','block');	
		$('#out_next_animation_mobile').css('display','block');
	}
}

function UploadPhotoSetting(callback) {
	
		$.ajax({
		type: "POST",
		url: "/UploadPhotoSetting/",
		dataType: 'json',
		data: { "pageId" : window.pageId, "photos" : window.photos},
		success: function(JData) {
			if (JData.error) {
				alert(JData.error);
			}
			else {
				callback();
			}
		},
		error: function() {
			alert("ERROR!!");

		}
		
	})
	
}

function PhotoPanelClear() {

	// clear click action
	$('#open_zoom-and-position').unbind("click");
	$('#close_zoom-and-position').unbind("click");
	$('#open_in-animation').unbind("click");
	$('#close_in-animation').unbind("click");
	$('#open_display-time').unbind("click");
	$('#close_display-time').unbind("click");
	$('#open_out-animation').unbind("click");
	$('#close_out-animation').unbind("click");
	//mobile clear click function 
	$('#in-animation_mobile').unbind('click'); 
	$('#display-time_mobile').unbind('click'); 
	$('#out-animation_mobile').unbind('click'); 
	
	$.each(window.para_obj, function( index,item ){
		var mask_name = (item.id).slice(1,(item.id).length);
		$('#open_'+mask_name,$( item.id+'_panel')).unbind("click");
		$('#close_'+mask_name,$( item.id+'_panel')).unbind("click");
	});
	
	
	// ========================== Turn photo button click ===========================
	$("#previous_photo_button").unbind("click");
	$("#next_photo_button").unbind("click");
	
	// ========================= zoom panel button click ============================ 
	$('#zoom_minus').unbind("click");
	$('#zoom_plus').unbind("click");

	
	// ======================== position panel button click =========================
	$('#position_top').unbind("click");
	$('#position_bottom').unbind("click");
	$('#position_right').unbind("click");
	$('#position_left').unbind("click");
	$('#position_center').unbind("click");

	// ============================== in-animation button click ============================= 
	$('#in-animation_minus').unbind("click");
	$('#in-animation_plus').unbind("click");
	$('input[name = Random_in]').unbind("click");
	for (var i = 0 ; i<window.in_animation_name.length;i++) {
		$('input[name='+window.in_animation_name[i]+']').unbind("click");
	}
	
	// ============================== display second button click ============================= 
	$('#display-time_minus').unbind("click");
	$('#display-time_plus').unbind("click");
	
	// ============================== out-animation button click ============================
	var current_select_animation = 'Random';
	
	$('#out-animation_minus').unbind("click");
	$('#out-animation_plus').unbind("click");
	$('input[name = Random_out]').unbind("click");
	
	for (var i = 0 ; i<window.out_animation_name.length;i++) {
		$('input[name='+window.out_animation_name[i]+']').unbind("click");
	}
	
	
	// clear display
	$("#center_panel").html("");
	$("#top_panel").html("");
	$("#left_panel").html("");
	$("#right_panel").html("");
	$("bottom_panel").html("");
	$("#para_panel").html("");
	
	$("#photo_upload_area").css("display", "none");
	$("#previous_photo_button").css("display", "none");
	$("#next_photo_button").css("display", "none");
	$("#para_panel").css("display","none");
	//mobile empty display
	$('#para_setting_mobile').html('');
	$('#para_panel_mobile').html('');
	//mobile clear display 
	$('#out_para_panel_mobile').css('display','none');
	$('#next_photo_mobile').css('display','none');
	$('#previous_photo_mobile').css('display','none');	
	TweenMax.set(['#next_photo_mobile','#previous_photo_mobile'],{autoAlpha:0});

	window.photo_index = 0;
}

function GetTextContent() {
	return $.ajax({
		type:'POST',
		dataType:'json',
		url:'/GetTextContent/',
		data:{"pageId": window.pageId},
		success: function(JData) {
			if ( JData.error ) {
				alert(JData.error);
				return false;
			}
			else {
				window.text=JData.text;
				return true;
			}
		},
		error: function() {
			alert("ERROR!!");
			return false;
		}
		
	})
}

function TextPanelDisplaySetting() {
	var window_width = $(window).width();
	var window_height = $(window).height();
	var text = window.text.replace(/<br>/g,"\n").replace(/&nbsp/g," ");
	$('<div id="text_preview">預覽</div>').appendTo($("#top_panel"));
	$('<div id="text_edit">編輯</div>').appendTo($("#top_panel"));
	$('<div id="text_confirm">確定</div>').appendTo($("#bottom_panel"));
	$('{% block content %}<textarea type="text" id="text_edit_div"></textarea>{% endblock %}').val(text).appendTo($("#center_panel"));
	$('<div id="text_preview_div"></div>').html(window.text).appendTo($("#center_panel"));
	if ( $(window).width()<666 ){
		$("#text_preview").css("margin-top",(window_height-317)/2-35);
		$("#text_preview").css("margin-left",(window_width-212)/2);
		$("#text_edit").css("margin-top",(window_height-317)/2-35);

		$('#text_confirm').css('background-image','none');
		$('#text_confirm').css('background-color','#272627');
		$('#text_confirm').css('opacity',0.8);
		$('#text_confirm').css('margin-left',(window_width-329)/2);

		$('#left_panel').width((window_width-212)/2).height(317);
		$('#left_panel').css('background-color','#272627');
		$('#left_panel').css('backgorund-image','none');
		$('#left_panel').css('opacity',0.8);
		$('#left_panel').css('margin-left',0);
		$('#left_panel').css('margin-top',(window_height-317)/2);


		$('#right_panel').width((window_width-212)/2).height(317);
		$('#right_panel').css('background-color','#272627');
		$('#right_panel').css('backgorund-image','none');
		$('#right_panel').css('opacity',0.8);
		$('#right_panel').css('margin-left',window_width/2+106);
		$('#right_panel').css('margin-top',(window_height-317)/2);

		$('#bottom_panel').css('background-color','#272627');
		$('#bottom_panel').css('opacity',0.8);
		
		// $('#top_panel').width(212);
	}
	TextPanelActionSetting();
}

function TextPanelActionSetting() {
	$("#text_preview").click( function() {
		TweenMax.to( $("#text_edit_div"), 0.5, {opacity:0, onComplete: textPreview });
	})
	$("#text_edit").click( function(){
		TweenMax.to( $("#text_preview_div"), 0.5, {opacity:0, onComplete: textEdit} );
	})
	$("#text_confirm").click( function(){
		window.text = $("#text_edit_div").val().replace(/\n/g,"<br>").replace(/ /g,"&nbsp").replace(/"/g,'&#34;').replace(/'/g,"&#39;");
		UploadText( function(){
			$("#text_edit_div").css("display","none").css("opacity",0);
			textPreview();
		})
	})
}

function UploadText(callback) {
	window.text = $("#text_edit_div").val().replace(/\n/g,"<br>").replace(/ /g,"&nbsp").replace(/"/g,'&#34;').replace(/'/g,"&#39;");
	console.log("pageId: " + window.pageId + ",  Upload: " + '"' + window.text + '"');
	$.ajax({
		type: "POST",
		url: "/UploadText/",
		dataType: 'json',
		data: { "pageId" : window.pageId, "text" : window.text},
		success: function(JData) {
			if (JData.error) {
				alert(JData.error);
			}
			else {
				callback();
			}
		},
		error: function() {
			alert("ERROR!!");
		}
	})
}

function TextPanelClear() {
	// clear click actions
	$("#text_preview").unbind("click");
	$("#text_edit").unbind("click");
	$("#text_confirm").unbind("click");
	
	// clear display
	$("#top_panel").html("");
	$("#bottom_panel").html("");
	$("#center_panel").html("");
}

function FinishPanelDisplay() {
	
	$.ajax({
		type: 'POST',
		url: '/GenerateQR/',
		dataType: 'json',
		data: {"pageId": window.pageId},
		success: function(JData) {
			if ( JData.error ) {
				alert("ERROR!!");
			}
			else {
				$('<div id="qrDescribe"  style="opacity:0; width:329px; height:50px; margin-top:34px; color:#ffffbd; text-align:center; line-height:50px;">QRCode for your page:</div>').appendTo($("#top_panel"));
				TweenMax.to($("#qrDescribe"), 1, {autoAlpha:1});
			
				var qrImg = new Image();
				qrImg.src = JData.qrImgPath;
				$(qrImg).attr('id','qrImg').attr('onclick','window.open("' + JData.path + '", "_blank");')
					    .css("opacity",0).css("cursor","pointer").appendTo($("#center_panel"));
				$(qrImg).load(function(){
					$(this).width(150).height(150)
						   .css("margin-top",$("#center_panel").height()/2-75)
						   .css("margin-left",$("#center_panel").width()/2-75);
					TweenMax.to( $(this), 1, {autoAlpha: 1});
					
				})
			}
		},
		error: function() {
			alert("ERROR!!");
		}
	})
	
}

// =================================================================================
// ======================= text edit and preview functions =========================
function textEdit() {
	$("#text_preview_div").css("display","none");
	$("#text_edit_div").css("display", "block");
	TweenMax.to( $("#text_edit_div"), 0.5, {autoAlpha:1} );
}
function textPreview() {
	var text = $("#text_edit_div").val().replace(/\n/g,"<br>").replace(/ /g,"&nbsp");
	$("#text_preview_div").html(text);
	$("#text_edit_div").css("display","none");
	$("#text_preview_div").css("display", "block");
	TweenMax.to( $("#text_preview_div"), 1, {autoAlpha:0.6});
	
	var second = $("#text_preview_div").height()/25 + 1;
	var start_position = $("#center_panel").height() ;
	var end_position = $("#text_preview_div").height() * -1 -30 ;
	
	TweenMax.fromTo($("#text_preview_div"), second, {y: start_position }, {y: end_position, ease: 'Linear.easeNone', onComplete:textEdit} );
}


// =================================================================================
// =========================== photo append functions ==============================
function PhotoContainerAppend( index ) {

	var photo_container = $('<div id="photo_container_' + index + '" data-bind="css:{ noneDisplay: currentSettingPhoto() != ' + index + '}"></div>');
	$('#top_panel').html( ( window.photo_index + 1 ).toString() + ' / ' + window.photos.length.toString() );
	
	var photo_obj = new Image();
	photo_obj.src = window.photos[index].url;
	$( photo_obj ).attr('id',index).css({'left':'0px','top':'0px','position':'relative'});
	photo_container.append(photo_obj).appendTo('#center_panel');
	
	$(photo_obj).load( function() {
		var photo_id = $(this).attr('id');
		$(this).css("height",$("#center_panel").height()*window.photos[parseInt(photo_id)].zoom);
		$(this).css("width","auto");
		//$( "#photo_container_"+photo_id ).css( "width", $(this).width()*2-$("#center_panel").width()).css("height", $(this).height()*2-$("#center_panel").height());
		PhotoContainerAdjust( photo_id );
		var ratio = window.photos[ parseInt( photo_id ) ].zoom;
		var left = window.photos[ parseInt( photo_id ) ].positionLeft;
		var top = window.photos[ parseInt( photo_id ) ].positionTop;
		PhotoPositionAdjust( parseInt( photo_id ), left, top );
		ko.applyBindings(viewModel);
	})
	
}


// =================================================================================
// ========================= Zoom setting functions ================================
function ReadPhotoZoomRatio(){
	return window.photos[window.photo_index].zoom;
}
function ZoomRatioSetting(){
	var zoom = ReadPhotoZoomRatio();
	$('#zoom_line_pointer').css('left', (( zoom - 1)*100 +55) );  
	$('#zoom_ratio_display').html( parseInt(zoom * 100) + '%' );  
}
function SavePhotoZoomRatio(){
	var left = parseInt(  $('#zoom_line_pointer').css('left') , 10 );
	window.photos[ window.photo_index ].zoom = (left - 55) / 100 + 1 ;  
	return left;
}
function PhotoZoomRatioPresent() {
	var left = SavePhotoZoomRatio();
	var $img = $('#'+window.photo_index,'#photo_container_'+window.photo_index);
	$('#zoom_ratio_display').html( parseInt(100 + ( left - 55 )) + '%' );
	
	
	$img.height( $("#center_panel").height()* window.photos[window.photo_index].zoom );
	$img.css( "width" , "auto");
}
function ZoomRatioReset() {
	SavePhotoZoomRatio();
	$('#zoom_ratio_display').html( '100%' );
	$('#zoom_line_pointer').css('left', 55);
}


// =================================================================================
// =========================== position setting functions ==========================
function PhotoMoveVertical(top) {
	
	var photo_top =  parseFloat( $("#" + window.photo_index).css("top") ) + top;
	var div_height = $("#photo_container_" + window.photo_index).height();
	var img_height = $("#"+window.photo_index, $("#photo_container_" + window.photo_index)).height();
	
	if ( img_height <= $("#center_panel").height() ) {
		if ( photo_top < 0 )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",0) ;
		else if ( photo_top > $("#center_panel").height()-img_height )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",$("#center_panel").height()-img_height) ;
		else
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",photo_top) ;
	}
	else {
		if ( photo_top < 0 )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",0) ;
		else if ( photo_top > div_height - img_height )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",div_height - img_height) ;
		else
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",photo_top) ;
	}
}
function PhotoMoveHorizontal(left) {
	
	var photo_left =  parseFloat( $("#" + window.photo_index).css("left") ) + left;
	var div_width = $("#photo_container_" + window.photo_index).width();
	var img_width = $("#"+window.photo_index, $("#photo_container_" + window.photo_index)).width();
	
	if ( img_width <= $("#center_panel").width() ) {
		if ( photo_left < 0 )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left", 0) ;
		else if ( photo_left > $("#center_panel").width()-img_width )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left", $("#center_panel").width()-img_width) ;
		else
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left",photo_left) ;
	}
	else {
		if ( photo_left < 0 )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left",0) ;
		else if ( photo_left > div_width - img_width )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left",div_width - img_width) ;
		else
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left",photo_left) ;
	}
}
function ReadPhotoPositionLeft() {
	return window.photos[window.photo_index].positionLeft;
}
function ReadPhotoPositionTop() {
	return window.photos[window.photo_index].positionTop;
}
function SavePhotoPosition()  {
	var index = window.photo_index;
	var ratio = window.photos[ index ].zoom;
	var left = parseFloat( $('#'+index, $('#photo_container_' + index)).css('left'));
	var top = parseFloat( $('#'+index, $('#photo_container_' + index)).css('top'));
	var img_width = $('#'+index,'#photo_container_'+index).width();
	var img_height = $('#'+index,'#photo_container_'+index).height();
	var div_width = $('#photo_container_'+index).width();
	var div_height = $('#photo_container_'+index).height();
	window.photos[index].positionLeft = ( left - (div_width - img_width)/2 ) / ratio ; 
	window.photos[index].positionTop = ( top - (div_height - img_height)/2 ) / ratio;
	console.log('==============='+index+'=========================');
	console.log("ratio: " + ratio + ", img_left: " + left + ", img_width: " + img_width + ", container_width: " + div_width + ", positionLeft: " + window.photos[index].positionLeft);
}
function PhotoContainerAdjust( index ){  
	var zoom = window.photos[ index ].zoom ;
	var $container = $('#photo_container_'+index); 
	var $img = $('#'+index, $container);
	var container_width = $img.width()*2-$("#center_panel").width();
	var container_height = $img.height()*2-$("#center_panel").height();
	
	var target_width = $("#center_panel").width();
	var target_height = $("#center_panel").height();
	
	if ( container_width <= target_width ) {
		$container.css( "width", target_width );
		$container.css('margin-left', 0 );
	}
	else {
		$container.css( "width", container_width );
		$container.css('margin-left', -( container_width - target_width ) / 2 );
	}	
	if ( container_height <= target_height ) {
		$container.css( "height", target_height );
		$container.css('margin-top', 0 );
	}
	else {
		$container.css( "height", container_height );
		$container.css('margin-top', -( container_height - target_height ) / 2 );
	}
}
function PhotoPositionAdjust( index, left, top ){
	var ratio = window.photos[ index ].zoom;
	var img_width = $('#'+index,'#photo_container_'+index).width();
	var img_height = $('#'+index,'#photo_container_'+index).height();
	var div_width = $('#photo_container_'+index).width();
	var div_height = $('#photo_container_'+index).height();
	var photo_left = ( div_width - img_width )/2 + left*ratio ;
	var photo_top = ( div_height - img_height)/2 + top*ratio ;
	
	if ( img_width <= $("#center_panel").width() ) {
		if ( photo_left < 0)
			photo_left = 0;
		else if ( photo_left > $("#center_panel").width()-img_width )
			photo_left = $("#center_panel").width()-img_width;
	}

	if ( img_height <= $("#center_panel").height() ) {
		if ( photo_top < 0 )
			photo_top = 0;
		else if ( photo_top > $("#center_panel").height()-img_height )
			photo_top = $("#center_panel").height()-img_height;
	}
		
	$('#'+index,'#photo_container_'+index).css({'left':photo_left,'top':photo_top});
	
}
function PhotoDraggable (){
	for (i=0;i<window.photos.length;i++){
		$('#'+i,'#photo_container_'+i ).draggable({
			containment: '#photo_container_'+i,
			stop: function() {
				SavePhotoPosition();
			}
		});
	}
	PhotoEnableDraggable();
}
function PhotoEnableDraggable(){
	for (i = 0;i<window.photos.length;i++){
		$('#'+ i,'#photo_container_'+i ).draggable( 'enable' );
	}
}
function PhotoDisableDraggable(){
	for (i = 0;i<window.photos.length;i++){
		$('#'+ i,'#photo_container_'+i ).draggable( 'disable' ).css('opacity',1);
	}
}


// ======================================================================================
// ============================== in-animation setting functions ========================
function ReadStartAnimateSecond( index ) {
	var second = parseFloat( window.photos[ index ].startAnimateSeconds );
	console.log('@read: seconds='+second);
	return second;
}
function ReadStartAnimateName( index ) {
	var name = window.photos[index].startAnimate;
	return name;
}
function SaveStartAnimateSecond( second ) {
	window.photos[window.photo_index].startAnimateSeconds = second ;
}
function SaveStartAnimateName( name ) { 
	window.photos[window.photo_index].startAnimate = name ;
}
	/*======================================================= update by Paul Ku 2013/10/26  */  
function InCancelRestAll( obj ){
	for (var i = 0;i<window.in_animation_name.length;i++){
		$('input[name ='+window.in_animation_name[ i ]+']').prop('checked',false);
	}
	$('input[name=Random_in]').prop('checked',false);
	obj.prop('checked',true);
	return obj.prop('name');
}
function InAnimationReset( index ){
	var second = ReadStartAnimateSecond( index );
	var name = ReadStartAnimateName( index );
	InCancelRestAll( $('input[name='+name+']') );
	$('#in-animation_seconds').html(second);
	console.log('@in animation works. name: '+name);
}
	/*======================================================= update by Paul Ku 2013/10/26  */  


// ==========================================================================================
// ============================== display time setting function =============================
function ReadDisplaySeconds( index ){
	var display_sec = window.photos[ index ].displaySeconds;
	return display_sec;
}
function SaveDisplaySeconds( sec ){
	window.photos[ window.photo_index ].displaySeconds = sec;
}
/*======================================================= update by Paul Ku 2013/10/26  */ 
function DisplaySecondsReset ( index ){
	var display_sec =  ReadDisplaySeconds( index );
	$('#display-time_seconds').html(display_sec);
}
/*======================================================= update by Paul Ku 2013/10/26  */ 

// ==========================================================================================
// ============================== out-animation setting function ============================
function ReadEndAnimateSecond( index ) {
	var second = parseFloat( window.photos[ index ].endAnimateSeconds );
	console.log('@read: seconds='+second);
	return second;
}
function ReadEndAnimateName( index ) {
	var name = window.photos[index].endAnimate;
	return name;
}
function SaveEndAnimateSecond( second ) {
	window.photos[window.photo_index].endAnimateSeconds = second ;
}
function SaveEndAnimateName( name ) { 
	window.photos[window.photo_index].endAnimate = name ;
}
function OutCancelRestAll( obj ){
	for (var i = 0;i<window.out_animation_name.length;i++){
		$('input[name ='+window.out_animation_name[ i ]+']').prop('checked',false);
	}
	$('input[name=Random_out]').prop('checked',false);
	obj.prop('checked',true);
	return obj.prop('name');
}
function OutAnimationReset( index ){
	var second = ReadEndAnimateSecond( index );
	var name = ReadEndAnimateName( index );
	OutCancelRestAll( $('input[name='+name+']') );
	$('#out-animation_seconds').html(second);
	console.log('@out animation works. name: '+name);
}

// ========================================================================================
// =================================== CSRF Setting =======================================
// ========================================================================================

$( document ).ready(function() {

	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				// Does this cookie string begin with the name we want?
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	function csrfSafeMethod(method) {
		// these HTTP methods do not require CSRF protection
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
  
	function sameOrigin(url) {
		// test that a given url is a same-origin URL
		// url could be relative or scheme relative or absolute
		var host = document.location.host; // host + port
		var protocol = document.location.protocol;
		var sr_origin = '//' + host;
		var origin = protocol + sr_origin;
		// Allow absolute or scheme relative URLs to same origin
		return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
			(url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
			// or any other URL that isn't scheme relative or absolute i.e relative.
			!(/^(\/\/|http:|https:).*/.test(url));
	}
	  
	$.ajaxSetup({
		crossDomain: false, // obviates need for sameOrigin test
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type)) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		}
	});
});