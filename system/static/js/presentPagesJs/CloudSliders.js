﻿$(window).load(function() {
	$(document).bind('touchmove',function(e) {
		e.preventDefault();
	});
	window.page_index = 0;
	LayoutSetting();
	ButtonContainerSetting();
	ButtonAnimationSetting();
	TextDisplaySetting();

	$('body').css('margin',0);
	$('body').css('overflow','hidden');

	for(var i = 0;i<window.photos.length;i++){
		PhotoContainerAppend( $("#photos_display") ,i );
	}
	
})

function LayoutSetting() {
	window.display_height = $(window).height();
	window.display_width = window.display_height/317*212;
	if ( window.display_width > $(window).width() ) {
		window.display_width = $(window).width();
		window.display_height = window.display_width/212*317;
	}

	window.display_rate=window.display_height/317;
	
	$("#display").width(window.display_width);
	$("#display").height(window.display_height);
	$("#loading_page").width(window.display_width);
	$("#loading_page").height(window.display_height);
	$("#loading_img").width(window.display_width);
	$("#loading_img").height(window.display_height);
	$("#loading_describe").width(window.display_width);
	$("#loading_describe").height(window.display_height/8);
	$("#loading_describe").css("line-height",window.display_height/8+"px");
	$("#loading_describe").css("margin-top",window.display_height - $("#loading_describe").height() );
	$("#photos_display").width(window.display_width);
	$("#photos_display").height(window.display_height);
	$("#text_display").width(window.display_width);
	$("#text_display").css("font-size", 20*window.display_rate + "px");
	$("#text_display").css("line-height", 35*window.display_rate + "px");
	$("#text_display").css("margin-top",window.display_height);
	$("#display").css("margin-top", ($(window).height() - window.display_height)/2);

}

function TextDisplaySetting() {
	window.text_marqueeing = false;
	$("#text_display").html(window.text);
}

function TextMarquee() {
	var second = $("#text_display").height()/25 + 1;
	var start_position = 0;
	var end_position = -( $("#text_display").height() + $('#display').height() );
	window.text_marqueeing = true;
	TweenMax.fromTo( $("#text_display"), second, {y: start_position }, {y: end_position, ease: 'Linear.easeNone', onComplete: UnMarqueeing} );
	// alert( "second: " + second + ", start_position: " + start_position + ", end_position: " + end_position + ", window height" + $(window).height() );
	function UnMarqueeing() {
		window.text_marqueeing = false;
	}
	
}

function AnimateProcess() {

	window.display_timeline = new TimelineMax({repeat:-1,paused:true});
	for(var i = 0;i<window.photos.length;i++){
		var start_name = ReadStartAnimateName( i );
		var start_sec = ReadStartAnimateSeconds( i );
		var display_time = ReadDislplaySeconds( i );
		var end_name = ReadEndAnimateName( i );
		var end_sec = ReadEndAnimateSeconds( i );
		
		window.display_timeline
			.call( PageIndex,[ i ] )
			.call( DisplayContainer, [$('#photo_container_'+i)] );
		ClidersAnimate( $('#photos_display'), $('#photo_container_'+i) , window.display_timeline , start_name, start_sec, -window.display_width , 0, -window.display_height, 0, 1, 1);
		window.display_timeline
			.addLabel(i)
			.add(TweenMax.to($('#photo_container'+i),0,{delay:display_time}));
		ClidersAnimate( $('#photos_display'), $('#photo_container_'+i) , window.display_timeline , end_name, end_sec, 0, window.display_width, 0,window.display_height, 1, 1);
		window.display_timeline.call( UndisplayContainer, [$('#photo_container_'+i)] );
	}
	//ClidersAnimate( target,obj, timeline , Name, sec, sX, eX, sY, eY, sScale, eScale )
	
	PageReady();
	
}

function PageReady() {
	$("#loading_describe").html("Click to continue...");
	$("#loading_page").click( function() {
		$("#loading_page").css("display","none");
		$("#photos_display").css("display","block");
		$("#text_display").css("display","block");
		$("#button_container").css("display","block");
																							  //<---------------------------------- 
		$("#photo_container_0").css({"display":"block","visibility":"visible","opacity":1});  //<---------------------------------- 咕雞bug修正，first picture can't display
		window.display_timeline.play();
		
		TextMarquee();
	})
}

function PageIndex( index ){
	window.page_index = index; 
}

function DisplayContainer(obj) {
	// console.log("display block!!  " + $(obj).attr("id") + " animate start!" );
	$(obj).css({"display":"block","visibility":"visible","opacity":1});
	TweenMax.set($(obj),{rotation:0,rotationY:0});
}

function UndisplayContainer(obj) {
	//TweenMax.set($(obj),{autoAlpha:0});
	$(obj).css({"display":"none","visibility":"hidden","opacity":0});
	TweenMax.set($(obj),{rotation:0,rotationY:0});
	// console.log("display none!!  " + $(obj).attr("id") + " animate end!" );
}

function PhotoContainerAppend( target, index ) {

	var photo_container = $('<div id="photo_container_' + index + '"></div>');
	
	var photo_obj = new Image();
	photo_obj.src = window.photos[index].url;
	$( photo_obj ).attr('id',index).css({'left':'0px','top':'0px','position':'relative'});
	photo_container.append(photo_obj).appendTo($(target));
	
	$(photo_obj).load( function() {
		var photo_id = $(this).attr('id');
		$(this).css("height",$(target).height()*window.photos[parseInt(photo_id)].zoom);
		$(this).css("width","auto");
		PhotoContainerAdjust( target, photo_id );
		var ratio = window.photos[ parseInt( photo_id ) ].zoom;
		var left = window.photos[ parseInt( photo_id ) ].positionLeft;
		var top = window.photos[ parseInt( photo_id ) ].positionTop;
		PhotoPositionAdjust( target, parseInt( photo_id ), left, top );
	})
	
}

function PhotoContainerAdjust( target, index ) {
	target_width = $(target).width();
	target_height = $(target).height();

	var zoom = window.photos[ index ].zoom ;
	var $container = $('#photo_container_'+index); 
	var $img = $('#'+index, $container);
	var container_width = target_width;
	var container_height = target_height;
	
	if ( container_width <= target_width ) {
		$container.css( "width", target_width );
		$container.css('margin-left', 0 );
	}
	else {
		$container.css( "width", container_width );
		$container.css('margin-left', -( container_width - target_width ) / 2 );
	}	
	if ( container_height <= target_height ) {
		$container.css( "height", target_height );
		$container.css('margin-top', 0 );
	}
	else {
		$container.css( "height", container_height );
		$container.css('margin-top', -( container_height - target_height ) / 2 );
	}

}

window.photo_processed = 0;

function PhotoPositionAdjust( target, index, left, top ){
	var ratio = window.photos[ index ].zoom;
	var img_width = $('#'+index,'#photo_container_'+index).width();
	var img_height = $('#'+index,'#photo_container_'+index).height();
	var div_width = $('#photo_container_'+index).width();
	var div_height = $('#photo_container_'+index).height();
	var photo_left = ( div_width - img_width )/2 + left*ratio*window.display_rate ;
	var photo_top = ( div_height - img_height)/2 + top*ratio*window.display_rate ;
	console.log( "left: " + left + ",  top: " + top + ",  ratio: " + ratio, ", photo_left: " + photo_left + ", photo_top: " + photo_top );
	/*
	if ( img_width <= $(target).width() ) {
		if ( photo_left < 0)
			photo_left = 0;
		else if ( photo_left > $(target).width()-img_width )
			photo_left = $(target).width()-img_width;
	}

	if ( img_height <= $(target).height() ) {
		if ( photo_top < 0 )
			photo_top = 0;
		else if ( photo_top > $(target).height()-img_height )
			photo_top = $(target).height()-img_height;
	}
	*/
	$('#'+index,'#photo_container_'+index).css({'left':photo_left,'top':photo_top});
	$("#photo_container_"+index).css("display","none").css("overflow","hidden");
	
	window.photo_processed++;
	if ( window.photo_processed == window.photos.length )
		AnimateProcess();
	
}



function ReadStartAnimateName( index ){
	var name = (window.photos[index].startAnimate).toString();
	return name;
}
function ReadStartAnimateSeconds( index ){
	var sec = (window.photos[index].startAnimateSeconds).toString();
	return sec;
}
function ReadDislplaySeconds( index ){
	var sec = parseFloat(window.photos[index].displaySeconds);
	return sec;
}
function ReadEndAnimateName (index){
	var name = (window.photos[index].endAnimate).toString();
	return name;
}
function ReadEndAnimateSeconds (index){
	var sec = (window.photos[index].endAnimateSeconds).toString();
	return sec;
}

function ButtonContainerSetting(){
	var button_container_width = window.display_width;
	var button_container_height = window.display_width/480*85;
	var width_base = button_container_width/480;
	var height_base = button_container_height/85;
	
	$('#button_container').width( button_container_width );
	$('#button_container').height( button_container_height ).css("margin-top", -1*button_container_height);
	
	var left = (button_container_width - width_base*103)/2;
	var top = button_container_height/2 - height_base*55/1.8;
	$('#animate_play_button').width( width_base*103 ).height( height_base*55 )
							 .css("left", left)
							 .css("top",  top);
							 
	$('#pause_button').width( width_base*104 ).height( height_base*56 )
					  .css("left", left)
					  .css("top", top );
					  
	left = left - width_base*76 - width_base*10;
	top = button_container_height/2 - height_base*55/2.5;
	$('#previous_button').width( width_base*76 ).height( height_base*50 )
						 .css("left", left )
						 .css("top", top);
						 
	left = button_container_width/2 + $('#animate_play_button').width()/2 + width_base*10;
	top = button_container_height/2 - height_base*55/2.5;
	$('#next_button').width( width_base*74 ).height( height_base*48 )
					 .css("left", left)
					 .css("top", top);
	
	left = left + $('#next_button').width() + width_base*20;
	top = button_container_height/2 - height_base*55/7;
	$('#text_play_button').width( width_base*79 ).height( height_base*43 )
						  .css("left",left)
						  .css("top", top);
}
function ButtonAnimationSetting(){
  	var button_timeline = new TimelineMax({paused:false,reversed:true});
	var text_boolean = false;
	var button_height = $('#button_container').height();
	button_timeline
		.add(TweenMax.fromTo($('#button_container'),1,{y:0,autoAlpha:1},{y:button_height,autoAlpha:0,ease:'Linear.easeNone'}),'+=5');
	button_timeline.play(0);
	$(window).on('tap',function(){
		button_timeline.resume(0).invalidate().play();
	});
	$(window).on('click',function(){
		button_timeline.resume(0).invalidate().play();
	});
	$('#pause_button').click(function(){
		pause_boolean = true;
		window.display_timeline.pause();
		$('#animate_play_button').css('display','block');
		$(this).css('display','none');
		$('#previous_button').css('display','block');
		$('#next_button').css('display','block');
		TweenMax.fromTo([$('#previous_button'),$('#next_button')],2,{autoAlpha:0},{autoAlpha:1,ease:'Power3.easeNone'});

		SwipeTurnPage();

	});
	$('#animate_play_button').click(function(){
		pause_boolean = false;
		window.display_timeline.play();
		$('#pause_button').css('display','block');
		$(this).css('display','none');
		TweenMax.fromTo([$('#previous_button'),$('#next_button')],1,{autoAlpha:1},{autoAlpha:0,ease:'Power3.easeNone',onComplete:BtnDisplayNone});
		
		SwipeTurnPageClear();

		function BtnDisplayNone(){
			$('#previous_button').css('display','none');
			$('#next_button').css('display','none');
		}
		
	});
	$('#text_play_button').click(function(){
		if ( window.text_marqueeing ) {
			var height = $("#text_display").height();
			var second = 1.5;
			var distance = (2*height)/(height/25+1)*second;
			TweenMax.killTweensOf($("#text_display"));
			TweenMax.to($("#text_display"), second, {y:'-='+distance, autoAlpha:0, ease: 'Linear.easeNone', onComplete:ResetTextDisplay});
		}	
		else
			TextMarquee();
			
		function ResetTextDisplay() {
			TweenMax.set($("#text_display"), {y:0, autoAlpha:0.6});
			$("#text_display").css("opacity",0.6);
			window.text_marqueeing = false;
		}
	});
	$('#previous_button').click(function(){
		if ( window.page_index > 0){
			var current = window.page_index;
			var previous = window.page_index - 1;
			TurnPageJudgement( current, previous );
		}
		else {
			var current = window.page_index;
			var previous = parseInt( window.photos.length - 1 );
			TurnPageJudgement( current, previous );
		}
		// console.log('page index: '+window.page_index);
	});
	$('#next_button').click(function(){
		if ( window.page_index < window.photos.length -1 ){		
			var next = window.page_index + 1;
			var current = window.page_index;
			TurnPageJudgement( current, next );
		}
		else {
			var next = 0
			var current = window.page_index;
			
			TurnPageJudgement( current, next );
		}
		// console.log('page index: '+window.page_index);
	});
	/* ============Screen Swipe left and swipe right =====================*/
	function SwipeTurnPage() {
		$('#photos_display').on('swipeRight',function(){
			if ( window.page_index > 0){
				var current = window.page_index;
				var previous = window.page_index - 1;
				TurnPageJudgement( current, previous );
			}
			else {
				var current = window.page_index;
				var previous = parseInt( window.photos.length - 1 );
				TurnPageJudgement( current, previous );
			}
		});
		$('#photos_display').on('swipeLeft',function(){
			if ( window.page_index < window.photos.length -1 ){		
				var next = window.page_index + 1;
				var current = window.page_index;
				TurnPageJudgement( current, next );
			}
			else {
				var next = 0
				var current = window.page_index;
				
				TurnPageJudgement( current, next );
			}
		});
	}
	function SwipeTurnPageClear(){
	 	$('#photos_display').unbind('swipeLeft');
		$('#photos_display').unbind('swipeRight');	
	}
	
}
function ReadCurrentLabel(){
	var current_label = window.page_index;
	return current_label;
}

function TurnPageJudgement(  current, turn_index ){
	var end_name = ReadEndAnimateName( current );
	var end_sec = ReadEndAnimateSeconds( current );
	var start_name = ReadStartAnimateName( turn_index );
	var start_sec = ReadStartAnimateSeconds( turn_index );
	var time = window.display_timeline.getLabelTime( turn_index );

	for(var i=0;i<window.photos.length;i++){
		UndisplayContainer( $('#photo_container_'+i) );	
		$('#photo_container_'+i+'_clone').css('display','none');
	}
	window.display_timeline.resume(time).pause();
	DisplayContainer( $('#photo_container_'+turn_index) );
	
	$('.CA_container').css('display','none');
	window.page_index = turn_index;
	// console.log('current: '+current+' '+typeof(current)+' ,turn index: '+turn_index+' '+typeof(turn_index)+', time: '+time);

}
