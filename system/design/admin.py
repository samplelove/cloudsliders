﻿# -*- coding: utf-8 -*-

from django.db import models
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from design.models import Active, SN, Banner, Customer, ThemeClass, Theme, Page, PageChild, Photo, Delivery, News, Animate, AnimateRelation ,Message

class ActiveAdmin(admin.ModelAdmin):
	list_display = ['name', 'DueDate']
	
class SNAdmin(admin.ModelAdmin):
	def active_name(self, obj):
		return ("%s" % (obj.active.name))
	list_display = ['active_name', 'sn']
	
class BannerAdmin(admin.ModelAdmin):
	list_display = ['bannerImage', 'picLink', 'picDescription']
  
class NewsAdmin(admin.ModelAdmin):
	list_display = ['title', 'url', 'date', 'pic', 'content']
	
class CustomerAdmin(admin.ModelAdmin):
	list_display = ['customer', 'name', 'sex', 'phone', 'address', 'birthday' ]

class CustomerInline(admin.StackedInline):
	model=Customer
	can_delete=False
	verbose_name_plural='customer'
	'''
	def has_add_permission(self, request):
		return False
	def has_delete_permission(self, request, obj=None):
		return False
	'''
	
class UserAdmin(UserAdmin):
	inlines=(CustomerInline, )

class ThemeClassAdmin(admin.ModelAdmin):
	list_display = ['name', 'pic', 'describe', 'setting_template_name', 'derectCreatePage', 'dayDuration']


class ThemeAdmin(admin.ModelAdmin):
	def themeClassName(self, obj):
		return ( "%s" % (obj.themeClass.name) )
	def themeSubClassName(self, obj):
		return ( "%s" % (obj.themeSubClass.name) )
	list_display = [ 'themeClassName', 'name', 'pic' ,'templateName', 'photoAmount']

class PageAdmin(admin.ModelAdmin):
	def customerName(self, obj):
		return ( "%s" % (obj.customer.name) )
	def themeClassName(self, obj):
		return ( "%s" % (obj.themeClass.name) )
	def themeName(self,obj):
		return ( "%s" % (obj.theme.name) )
	list_display = ['pageId', 'title', 'date', 'customerName', 'themeClassName', 'themeName', 'urlOfVideo', 'text', 'editDate', 'presentDueDate', 'path', 'qrCode']

class AnimateAdmin(admin.ModelAdmin):
	list_display = ['animate', 'chineseName' ,'animate_class']
	
class AnimateRelationAdmin(admin.ModelAdmin):
	def productName(self, obj):
		return ( "%s" % (obj.product.name) )
	def animate(self, obj):
		return ( "%s" % (obj.animate.animate) )
	list_display = ['productName', 'animate']
	
class PageChildAdmin(admin.ModelAdmin):
	def page(self, obj):
		return( "%s%s%s" %(obj.page.pageId) %(" ") %(obj.page.customer.name) )
	list_display = ['page','name','num','text']
	
class PhotoAdmin(admin.ModelAdmin):
	def pageId(self,obj):
		return ( "%s" % (obj.page.pageId) )
	list_display = ['pageId', 'ori_image', 'resize_image', 'positionLeft', 'positionTop', 'zoom', 'startAnimate', 'startAnimateSeconds', 'endAnimate', 'endAnimateSeconds', 'displaySeconds']
	
class DeliveryAdmin(admin.ModelAdmin):
	def buyer_name(self, obj):
		return ("%s"%(obj.buyer.name))
	list_display = ['buyer_name', 'phone', 'address']

class MessageAdmin(admin.ModelAdmin):
	list_display = ['parent','child','text','datetime']
	
# Register your models here.

admin.site.register(Active, ActiveAdmin)	
admin.site.register(SN, SNAdmin)
admin.site.register(News, NewsAdmin)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Theme, ThemeAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Delivery, DeliveryAdmin)
admin.site.register(Banner, BannerAdmin)
admin.site.register(Customer, CustomerAdmin ) # modify by cycho 2013_08_21
admin.site.register(ThemeClass, ThemeClassAdmin)
admin.site.register(PageChild, PageChildAdmin)
admin.site.register(Animate, AnimateAdmin)
admin.site.register(AnimateRelation, AnimateRelationAdmin)
admin.site.register(Message, MessageAdmin)


