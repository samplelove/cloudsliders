# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
import datetime
import uuid
import os
from django.utils.http import urlquote
#from phonenumber_field.modelfields import PhoneNumberField
# Create your models here.

# ========================================前端頁面資訊=======================================

def banner_upload_to(filename, folder):
	file_path = 'images/%s/%s' % (folder, filename)
	return file_path

def upload_banner(Banner, filename):
	return banner_upload_to(filename, 'offical/banner')

class Banner(models.Model):
  bannerImage=models.ImageField(upload_to=upload_banner)
  picLink=models.CharField(max_length=100)
  picDescription=models.TextField()

# =======================================活動與活動序號=======================================
  
class Active(models.Model):
	name=models.CharField(max_length=100)
	DueDate=models.DateTimeField()
	def __unicode__(self):
		return self.name

class SN(models.Model):
	active=models.ForeignKey(Active)
	sn=models.CharField(max_length=100)
	def __unicode__(self):   # modify by cycho
	  return self.sn         # modify by cycho

def newsImage_upload_to( filename, folder ):
	file_path = 'images/%s/%s' % (folder, filename)
	return file_path
	
def upload_NewsImage(News, filename):
	return newsImage_upload_to(filename, 'news_images')  

class News(models.Model):
	title=models.CharField(max_length=200)
	url=models.CharField(max_length=500)
	date=models.DateTimeField(null=True)
	pic=models.ImageField(upload_to=upload_NewsImage)
	content=models.CharField(max_length=500, blank=True, null=True)
	  
# =======================================使用者帳戶資訊======================================


SEX = [
	[ 1, 'male'],
	[ 2, 'female'],
]

class Customer(models.Model):
	customer=models.OneToOneField(User)
	name=models.CharField(max_length=30)
	sex=models.IntegerField(choices=SEX)
	#phone=models.IntegerField()
	phone=models.CharField(max_length=20, blank=True, null=True)
	#phtone=PhoneNumberField()
	address=models.CharField(max_length=50, blank=True, null=True)
	birthday=models.DateField(blank=True, null=True)
	def __unicode__(self):
		return self.name

# ===================================主題分類,主題Model===========================================

def themeClassImage_upload_to( className, filename, folder ):
	file_path = 'images/%s/%s/%s' % (folder, className, filename)
	return file_path
	
def upload_ThemeClassImage( ThemeClass , filename):
	return themeClassImage_upload_to(ThemeClass.name, filename, 'theme_class_img')
	
class ThemeClass(models.Model):
	name=models.CharField(max_length=30)
	pic=models.ImageField(upload_to=upload_ThemeClassImage)
	describe=models.CharField(max_length=1000)
	setting_template_name=models.CharField(max_length=100,blank=True,null=True)
	derectCreatePage=models.BooleanField(default=False)
	dayDuration=models.IntegerField(default=0)
	def __unicode__(self):
		return self.name


def themeImage_upload_to(themeName, filename, folder):
	file_path = 'images/%s/%s/%s' % (folder, themeName, filename)
	return file_path
	
def upload_ThemeImage(Theme, filename):
	return themeImage_upload_to(Theme.name, filename, 'theme_img')


class Theme(models.Model):
	themeClass=models.ForeignKey(ThemeClass)
	name=models.CharField(max_length=30)
	templateName=models.CharField(max_length=100)
	pic=models.ImageField(upload_to=upload_ThemeImage)
	photoAmount=models.IntegerField()
	def __unicode__(self):
		return self.name


# =========================================版面呈現資訊=====================================================
	
def qrCode_upload_to(pageId, filename, folder):
	file_path = 'images/%s/%s/%s' % (folder, pageId, filename)
	return file_path

def upload_qrCode(Page, filename):
	return qrCode_upload_to(Page.pageId, filename, 'qrCode')

class Page(models.Model):
	pageId=models.CharField(max_length=100)
	title=models.CharField(max_length=100, default="")
	date=models.DateField(default=datetime.date.today())
	customer=models.ForeignKey(Customer)
	themeClass=models.ForeignKey(ThemeClass)
	theme=models.ForeignKey(Theme)
	urlOfVideo=models.CharField(max_length=500,blank=True,null=True)
	text=models.CharField(max_length=3000, default="")
	editDate=models.DateTimeField(default=datetime.datetime.now(), null=True)
	presentDueDate=models.DateTimeField(null=True)
	path=models.CharField(max_length=500,blank=True,null=True)
	qrCode=models.ImageField(upload_to=upload_qrCode,blank=True,null=True)
	def __unicode__(self):
		return self.pageId
		
class PageChild(models.Model):
	#uid=models.CharField(max_length=100,default="")
	page=models.ForeignKey(Page)
	name=models.CharField(max_length=100, default="")
	num=models.IntegerField(default=0)
	text=models.CharField(max_length=500, default="")

# ==============================================================================

ANIMATE_CLASS = [
	[ 1, 'in'],
	[ 2, 'out'],
	[ 3, 'both'],
]

class Animate(models.Model):
	animate=models.CharField(max_length=100)
	chineseName=models.CharField(max_length=100, default="未知")
	animate_class=models.IntegerField(choices=ANIMATE_CLASS)
	def __unicode__(self):
		return self.animate
	
class AnimateRelation(models.Model):
	animate=models.ForeignKey(Animate)
	product=models.ForeignKey(ThemeClass)

# ==============================================================================
def photo_frame_upload_to( folder,themeClassName,filename ):
	themeClass=PhotoFrame.object.get(themeClass)
	themeClassName=ThemeClass.object.filter(name=themeClass)
	file_path = 'images/%s/%s/%s' % (folder, themeClassName , filename)
	return file_path	

def upload_photo_frame( PhotoFrame, filename ):
	return photo_frame_upload_to( 'PhotoFrame' , themeClassName , filename)

class PhotoFrame(models.Model):
	name=models.CharField(max_length=50,default="")
	themeClass=models.ForeignKey(ThemeClass)
	frameImage=models.ImageField(upload_to=upload_photo_frame)


def ori_photo_upload_to(pageId, filename, folder):
	file_name = uuid.uuid4().hex
	file_extension = os.path.splitext(filename)[-1]
	full_file_name = file_name + file_extension
	file_path = 'images/%s/%s/%s' % (folder, pageId, full_file_name)
	return file_path

def upload_ori_photo(Photo, filename):
	return ori_photo_upload_to(Photo.page.pageId, filename, 'ori_photos')

def resize_photo_upload_to(pageId, filename, folder):
	file_name = uuid.uuid4().hex
	file_extension = os.path.splitext(filename)[-1]
	full_file_name = file_name + file_extension
	file_path = 'images/%s/%s/%s' % (folder, pageId, full_file_name)
	return file_path

def upload_resize_photo(Photo, filename):
	return resize_photo_upload_to(Photo.page.pageId, filename, 'resize_photos')

class Photo(models.Model):
	page=models.ForeignKey(Page)
	ori_image=models.ImageField(upload_to=upload_ori_photo)
	resize_image=models.ImageField(upload_to=upload_resize_photo)
	positionLeft=models.FloatField(default=0)
	positionTop=models.FloatField(default=0)
	zoom=models.FloatField(default=1)
	startAnimate=models.CharField(max_length=100, default="Random_in")
	startAnimateSeconds=models.FloatField(default=1.5)
	endAnimate=models.CharField(max_length=100, default="Random_out")
	endAnimateSeconds=models.FloatField(default=1.5)
	displaySeconds=models.FloatField(default=3)
	describe=models.CharField(max_length=100, default="")
	photoFrame=models.ForeignKey(PhotoFrame, null=True)

# ==============================================================================
	
class Delivery(models.Model):
	buyer = models.ForeignKey(Customer)
	phone = models.CharField(max_length=20)
	address = models.CharField(max_length=50)

# ==============================================================================

class Message(models.Model):
	parent = models.ForeignKey(Page)
	child = models.ForeignKey(PageChild)
	text = models.CharField(max_length=300,default="")
	datetime = models.DateTimeField(null=True)


