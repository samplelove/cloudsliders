# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Banner'
        db.create_table(u'design_banner', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bannerImage', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('picLink', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('picDescription', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'design', ['Banner'])

        # Adding model 'Active'
        db.create_table(u'design_active', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('DueDate', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'design', ['Active'])

        # Adding model 'SN'
        db.create_table(u'design_sn', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('active', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['design.Active'])),
            ('sn', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'design', ['SN'])

        # Adding model 'News'
        db.create_table(u'design_news', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('pic', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('content', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
        ))
        db.send_create_signal(u'design', ['News'])

        # Adding model 'Customer'
        db.create_table(u'design_customer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('customer', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('sex', self.gf('django.db.models.fields.IntegerField')()),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
        ))
        db.send_create_signal(u'design', ['Customer'])

        # Adding model 'ThemeClass'
        db.create_table(u'design_themeclass', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('pic', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('describe', self.gf('django.db.models.fields.CharField')(max_length=1000)),
            ('setting_template_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'design', ['ThemeClass'])

        # Adding model 'Theme'
        db.create_table(u'design_theme', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('themeClass', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['design.ThemeClass'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('templateName', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('pic', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('photoAmount', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'design', ['Theme'])

        # Adding model 'Page'
        db.create_table(u'design_page', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pageId', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['design.Customer'])),
            ('themeClass', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['design.ThemeClass'])),
            ('theme', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['design.Theme'])),
            ('urlOfVideo', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('text', self.gf('django.db.models.fields.CharField')(default='', max_length=1000)),
            ('presentDueDate', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('path', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('qrCode', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'design', ['Page'])

        # Adding model 'PageChild'
        db.create_table(u'design_pagechild', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('page', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['design.Page'])),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
            ('num', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('text', self.gf('django.db.models.fields.CharField')(default='', max_length=500)),
        ))
        db.send_create_signal(u'design', ['PageChild'])

        # Adding model 'Animate'
        db.create_table(u'design_animate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('animate', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('chineseName', self.gf('django.db.models.fields.CharField')(default='\xe6\x9c\xaa\xe7\x9f\xa5', max_length=100)),
            ('animate_class', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'design', ['Animate'])

        # Adding model 'AnimateRelation'
        db.create_table(u'design_animaterelation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('animate', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['design.Animate'])),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['design.ThemeClass'])),
        ))
        db.send_create_signal(u'design', ['AnimateRelation'])

        # Adding model 'Photo'
        db.create_table(u'design_photo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('page', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['design.Page'])),
            ('ori_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('resize_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('positionLeft', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('positionTop', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('zoom', self.gf('django.db.models.fields.FloatField')(default=1)),
            ('startAnimate', self.gf('django.db.models.fields.CharField')(default='Random_in', max_length=100)),
            ('startAnimateSeconds', self.gf('django.db.models.fields.FloatField')(default=1.5)),
            ('endAnimate', self.gf('django.db.models.fields.CharField')(default='Random_out', max_length=100)),
            ('endAnimateSeconds', self.gf('django.db.models.fields.FloatField')(default=1.5)),
            ('displaySeconds', self.gf('django.db.models.fields.FloatField')(default=3)),
            ('describe', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
        ))
        db.send_create_signal(u'design', ['Photo'])

        # Adding model 'Delivery'
        db.create_table(u'design_delivery', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('buyer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['design.Customer'])),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'design', ['Delivery'])

        # Adding model 'Message'
        db.create_table(u'design_message', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['design.Page'])),
            ('child', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['design.PageChild'])),
            ('text', self.gf('django.db.models.fields.CharField')(default='', max_length=300)),
            ('datetime', self.gf('django.db.models.fields.DateTimeField')(null=True)),
        ))
        db.send_create_signal(u'design', ['Message'])


    def backwards(self, orm):
        # Deleting model 'Banner'
        db.delete_table(u'design_banner')

        # Deleting model 'Active'
        db.delete_table(u'design_active')

        # Deleting model 'SN'
        db.delete_table(u'design_sn')

        # Deleting model 'News'
        db.delete_table(u'design_news')

        # Deleting model 'Customer'
        db.delete_table(u'design_customer')

        # Deleting model 'ThemeClass'
        db.delete_table(u'design_themeclass')

        # Deleting model 'Theme'
        db.delete_table(u'design_theme')

        # Deleting model 'Page'
        db.delete_table(u'design_page')

        # Deleting model 'PageChild'
        db.delete_table(u'design_pagechild')

        # Deleting model 'Animate'
        db.delete_table(u'design_animate')

        # Deleting model 'AnimateRelation'
        db.delete_table(u'design_animaterelation')

        # Deleting model 'Photo'
        db.delete_table(u'design_photo')

        # Deleting model 'Delivery'
        db.delete_table(u'design_delivery')

        # Deleting model 'Message'
        db.delete_table(u'design_message')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'design.active': {
            'DueDate': ('django.db.models.fields.DateTimeField', [], {}),
            'Meta': {'object_name': 'Active'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'design.animate': {
            'Meta': {'object_name': 'Animate'},
            'animate': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'animate_class': ('django.db.models.fields.IntegerField', [], {}),
            'chineseName': ('django.db.models.fields.CharField', [], {'default': "'\\xe6\\x9c\\xaa\\xe7\\x9f\\xa5'", 'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'design.animaterelation': {
            'Meta': {'object_name': 'AnimateRelation'},
            'animate': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['design.Animate']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['design.ThemeClass']"})
        },
        u'design.banner': {
            'Meta': {'object_name': 'Banner'},
            'bannerImage': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'picDescription': ('django.db.models.fields.TextField', [], {}),
            'picLink': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'design.customer': {
            'Meta': {'object_name': 'Customer'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'sex': ('django.db.models.fields.IntegerField', [], {})
        },
        u'design.delivery': {
            'Meta': {'object_name': 'Delivery'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'buyer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['design.Customer']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'design.message': {
            'Meta': {'object_name': 'Message'},
            'child': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['design.PageChild']"}),
            'datetime': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['design.Page']"}),
            'text': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '300'})
        },
        u'design.news': {
            'Meta': {'object_name': 'News'},
            'content': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pic': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        u'design.page': {
            'Meta': {'object_name': 'Page'},
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['design.Customer']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pageId': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'presentDueDate': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'qrCode': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '1000'}),
            'theme': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['design.Theme']"}),
            'themeClass': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['design.ThemeClass']"}),
            'urlOfVideo': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'})
        },
        u'design.pagechild': {
            'Meta': {'object_name': 'PageChild'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'num': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['design.Page']"}),
            'text': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '500'})
        },
        u'design.photo': {
            'Meta': {'object_name': 'Photo'},
            'describe': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'displaySeconds': ('django.db.models.fields.FloatField', [], {'default': '3'}),
            'endAnimate': ('django.db.models.fields.CharField', [], {'default': "'Random_out'", 'max_length': '100'}),
            'endAnimateSeconds': ('django.db.models.fields.FloatField', [], {'default': '1.5'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ori_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['design.Page']"}),
            'positionLeft': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'positionTop': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'resize_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'startAnimate': ('django.db.models.fields.CharField', [], {'default': "'Random_in'", 'max_length': '100'}),
            'startAnimateSeconds': ('django.db.models.fields.FloatField', [], {'default': '1.5'}),
            'zoom': ('django.db.models.fields.FloatField', [], {'default': '1'})
        },
        u'design.sn': {
            'Meta': {'object_name': 'SN'},
            'active': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['design.Active']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sn': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'design.theme': {
            'Meta': {'object_name': 'Theme'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'photoAmount': ('django.db.models.fields.IntegerField', [], {}),
            'pic': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'templateName': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'themeClass': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['design.ThemeClass']"})
        },
        u'design.themeclass': {
            'Meta': {'object_name': 'ThemeClass'},
            'describe': ('django.db.models.fields.CharField', [], {'max_length': '1000'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'pic': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'setting_template_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['design']