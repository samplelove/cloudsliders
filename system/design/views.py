﻿ #-*- coding: utf-8 -*-
import json
import uuid
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.core.context_processors import csrf
from django.contrib.auth.models import User
from design.models import User, Active, SN, News, Banner, Customer, ThemeClass, Theme, Page, Photo, Delivery, AnimateRelation, Animate,PhotoFrame
from django.contrib.auth import authenticate
#from design.backends import EmailOrUsernameModelBackend
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
import re
import sqlite3
from django.utils.http import urlquote
from django.utils.http import urlunquote
import qrcode
import StringIO
from PIL import Image
import imghdr
from django.core.files import File
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.files.base import ContentFile
import os
import datetime
from django.utils.timezone import localtime
from django.core.mail import EmailMultiAlternatives

from time import mktime


# Create your views here.


#####################################################################################################
########################################## Page request #############################################
#####################################################################################################
gMemberAmountLimit = 1000
gNormalPageLimit = 3

def index(request):
	newss = News.objects.order_by('date')
	newsData = [];
	memberAmount = len(Customer.objects.all())
	remain = gMemberAmountLimit - memberAmount

	if memberAmount >= gMemberAmountLimit:
		register = { 'status': 'on', 'remain': remain }
	else:
		register = { 'status': 'off', 'remain': remain }

	registerJ = json.dumps(register)

	for news in newss:
		dateStr = str(news.date)
		newsData = newsData +[{ 'title' : news.title, 'url' : news.url, 'date' : dateStr, 'pic' : '/' + urlunquote(news.pic.url), 'content': news.content }]

	newsJData = json.dumps(newsData)
	return render(request, 'index.html', {'news' : newsJData, 'register': registerJ })
	

def pageSet(request):
	return render(request, 'pageSet.htm')
	
def ProductSetting_view(request, className=""):
	if(ThemeClass.objects.filter(name=className)):
		themeClass=ThemeClass.objects.get(name=className)
		if ( themeClass.derectCreatePage ):
			if not request.user.is_authenticated():
				return HttpResponse("<h2>請先登入會員!</h2>")

			themes=Theme.objects.filter(themeClass=themeClass).order_by('id')
			
			themeData=[]
			for theme in themes:
				themeData=themeData+[{'theme_number':theme.id, 'theme_name': theme.name, 'url': "/"+urlunquote(theme.pic.url)}]
			themeJData = json.dumps(themeData)
			
			animatesPermission=AnimateRelation.objects.filter(product=themeClass).order_by('id')
			in_animates=[]
			out_animates=[]
			for animateRelation in animatesPermission.order_by('id'):
				if ( animateRelation.animate.animate_class == 1 ):
					in_animates=in_animates + [{ "name": animateRelation.animate.animate, "chineseName" : animateRelation.animate.chineseName }]
				elif ( animateRelation.animate.animate_class == 2 ):
					out_animates=out_animates + [{ "name": animateRelation.animate.animate, "chineseName" : animateRelation.animate.chineseName }]
				else:
					in_animates=in_animates + [{ "name": animateRelation.animate.animate, "chineseName" : animateRelation.animate.chineseName }]
					out_animates=out_animates + [{ "name": animateRelation.animate.animate, "chineseName" : animateRelation.animate.chineseName }]
				
			in_animatesJData = json.dumps(in_animates)		
			out_animatesJData = json.dumps(out_animates)
			
			id = uuid.uuid4().hex
			while Page.objects.filter( pageId = id ):
				id = uuid.uuid4().hex
			
			page=Page( pageId=id, customer=Customer.objects.get(customer=request.user) , themeClass=theme.themeClass, theme=themes[0], editDate=datetime.datetime.now() )
			page.save();

			return render( request, themeClass.setting_template_name, { 'pageID': id, 'themes': themeJData, 'in_animations' : in_animatesJData, 'out_animations' : out_animatesJData} )
		else:
			theme=Theme.objects.filter(themeClass=themeClass)
			themeJName = json.dumps( { "name": theme[0].name } )
			classJName = json.dumps( {"name": themeClass.name} )
			pages = Page.objects.filter(customer=Customer.objects.get(customer=request.user),themeClass=themeClass).order_by('presentDueDate')
			page_list=[]
			for tempPage in pages:
				page_list = page_list + [{ 'pageId': tempPage.pageId , 'dueDate': str(localtime(tempPage.presentDueDate)) }]
			if( page_list==[] ):
				page_list_J = json.dumps( {"none":"none"} )
			else:
				page_list_J = json.dumps(page_list)

			return render( request, themeClass.setting_template_name,  { "class_name": classJName , "theme_name": themeJName , "project_list": page_list_J } )
	else:
		return HttpResponse("No such product!")


def SaveAblumTitleAndDate_view(request):

	if request.user.is_authenticated:
		customer = Customer.objects.get(customer=request.user)
	else:
		return HttpResponse( json.dumps( {"error" : "請先登入或註冊會員!!"} ) )

	pageId = request.POST.get('pageId')
	title = request.POST.get('title').encode('utf-8')
	yy = request.POST.get('yy').encode('utf-8')
	mm = request.POST.get('mm').encode('utf-8')
	dd = request.POST.get('dd').encode('utf-8')
	className = request.POST.get('className').encode('utf-8')
	themeName = request.POST.get('themeName').encode('utf-8')

	if ThemeClass.objects.filter(name=className):
		themeClass = ThemeClass.objects.get(name=className)
	else:
		return HttpResponse( json.dumps( {"error" : "該服務存取發錯誤,請重新整理頁面!!"} ) )

	if Theme.objects.filter(name=themeName):
		theme = Theme.objects.get(name=themeName)
	else:
		return HttpResponse( json.dumps( {"error" : "該服務存取發錯誤,請重新整理頁面!!"} ) )

	try:
		yy = int(yy)
	except valueError:
		return HttpResponse( json.dumps({ "error" : "日期格式錯誤!" }) )

	try:
		mm = int(mm)
	except valueError:
		return HttpResponse( json.dumps({ "error" : "日期格式錯誤!" }) )

	try:
		dd = int(dd)
	except valueError:
		return HttpResponse( json.dumps({ "error" : "日期格式錯誤!" }) )
	
	animatesPermission=AnimateRelation.objects.filter(product=themeClass).order_by('id')
	in_animates=[]
	out_animates=[]
	for animateRelation in animatesPermission.order_by('id'):
		if ( animateRelation.animate.animate_class == 1 ):
			in_animates=in_animates + [{ "name": animateRelation.animate.animate, "chineseName" : animateRelation.animate.chineseName }]
		elif ( animateRelation.animate.animate_class == 2 ):
			out_animates=out_animates + [{ "name": animateRelation.animate.animate, "chineseName" : animateRelation.animate.chineseName }]
		else:
			in_animates=in_animates + [{ "name": animateRelation.animate.animate, "chineseName" : animateRelation.animate.chineseName }]
			out_animates=out_animates + [{ "name": animateRelation.animate.animate, "chineseName" : animateRelation.animate.chineseName }]
		
	in_animatesJData = json.dumps(in_animates)		
	out_animatesJData = json.dumps(out_animates)
	
	photosD = []

	if pageId == '':
		uid = uuid.uuid4().hex
		while Page.objects.filter(pageId=uid):
			uid = uuid.uuid4().hex

		page = Page( pageId=uid, title=title, customer=customer, themeClass=themeClass, theme=theme, date=datetime.date(yy,mm,dd))
		page.save()

		photosJ=json.dumps(photosD)
		text=""
	else:
		if Page.objects.filter(pageId=pageId):
			page = Page.objects.get(pageId=pageId)
			if page.customer == customer:
				page.title = title
				page.date = datetime.date(yy,mm,dd)
				photos = Photo.objects.filter(page=page).order_by('id')
				for photo in photos:
					photosD = photosD + [{ 	"url": '/'+urlunquote(photo.resize_image.url),
											"positionLeft": photo.positionLeft,
											"positionTop": photo.positionTop,
											"zoom": photo.zoom,
											"startAnimate": photo.startAnimate,
											"startAnimateSeconds": photo.startAnimateSeconds,
											"endAnimate": photo.endAnimate,
											"endAnimateSeconds": photo.endAnimateSeconds,
											"displaySeconds": photo.displaySeconds,
											"describe": photo.describe
										}]

				photosJ = json.dumps(photosD)
				text = page.text
				page.save()
			else:
				return HttpResponse( json.dumps({ "error" : "權限錯誤,請重新登入!!" }) )
		else:
			return HttpResponse( json.dumps({ "error" : "該事件不存在!!" }) )

	return HttpResponse(json.dumps({"pageId": page.pageId, 'in_animations' : in_animatesJData, 'out_animations' : out_animatesJData, 'photos': photosJ, "text":text }))

#####################################################################################################
##################################### Account authentication ########################################
#####################################################################################################

@csrf_exempt
def Login_view(request):
	if request.is_ajax():
		account = request.POST.get('account')
		password = request.POST.get('password')
	user = authenticate(username=account,password=password)
	if user is not None:
		auth.login( request, user )
		return HttpResponse( json.dumps( { "success" : "success" } ) )
	else:
		return HttpResponse( json.dumps( {"error" : "The username and password were incorrect."} ) )

#=============================================================

def Logout_view(request):
	auth.logout( request )
	return HttpResponse("Logout!!")

#=============================================================
 
def GetUser_view(request):
	if request.user.is_authenticated():
		customer = Customer.objects.get(customer=request.user)
		userName = Customer.objects.get(customer=request.user).name

		pagesD = []
		pages = Page.objects.filter(customer=customer).order_by('id').reverse().order_by('date')
		
		for page in pages:
			if ( not page.qrCode ) or page.editDate is None or page.date is None:
				page.delete()
			elif page.themeClass.derectCreatePage:
				pagesD = pagesD + [{ 'id': page.id, 'date': str( page.date ), 'url': page.path, 'qrcode_url': '/' + urlunquote(page.qrCode.url), 'pageId': page.pageId }]
				
		JData = json.dumps({'name' : userName, 'user_pages' : pagesD })
		return HttpResponse(JData)
	else:
		JData = []
		return HttpResponse(JData)
 
#=============================================================

def Regist_view(request):
	account = request.POST.get('account').encode('utf-8')
	password = request.POST.get('password').encode('utf-8')
	repeat = request.POST.get('repeat').encode('utf-8')
	name = request.POST.get('name').encode('utf-8')
	sex = request.POST.get('sex').encode('utf-8')
	yy = request.POST.get('yy')
	mm = request.POST.get('mm')
	dd = request.POST.get('dd')
  
	if ( account=="" ):
		return HttpResponse( json.dumps({ "error" : "帳號未填寫" }) )
	
	if ( password=="" ):
		return HttpResponse( json.dumps({ "error" : "密碼未填寫" }) )
	
	if ( repeat=="" ):
		return HttpResponse( json.dumps({ "error" : "密碼重複未填寫" }) )
		
	if ( name=="" ):
		return HttpResponse( json.dumps({ "error" : "姓名未填寫" }) )
	
	if ( sex != 'male' and sex != 'female' ):
		return HttpResponse( json.dumps({ "error" : "請選擇性別" }) )
	
	if len(Customer.objects.all()) >= gMemberAmountLimit:
		return HttpResponse( json.dumps({ "error" : "目前註冊人數已滿!" }) )
	
	try:
		yy = int(yy)
	except valueError:
		return HttpResponse( json.dumps({ "error" : "日期格式錯誤!" }) )

	try:
		mm = int(mm)
	except valueError:
		return HttpResponse( json.dumps({ "error" : "日期格式錯誤!" }) )

	try:
		dd = int(dd)
	except valueError:
		return HttpResponse( json.dumps({ "error" : "日期格式錯誤!" }) )


	if ( password == repeat ):
		user = User.objects.create_user( account, account, password )
		user.save()
		if( sex == 'male' ):
			c = Customer(customer=user,name=name, sex=1, birthday=datetime.date(yy,mm,dd))
		else:
			c = Customer(customer=user,name=name, sex=2, birthday=datetime.date(yy,mm,dd))

		c.save()
		
		user_temp = authenticate(username=account,password=password)
		auth.login( request, user_temp )
		return HttpResponse( json.dumps({"success" : "success"}) )
	else:
		return HttpResponse( json.dumps({"error" : "密碼兩次輸入不相同"}) )

#=============================================================
# admin
	  
def login(request):
	c = {}
	c.update( csrf(request) ) #csrf(request) is object to pass through to login.html c
	return render_to_response( 'accounts/login.html', c )
  
def auth_view(request):
	username = request.POST.get('username', '' )
	password = request.POST.get('password', '' )
	user = auth.authenticate( username=username, password=password )

	if user is not None:
		auth.login( request, user )
		return HttpResponseRedirect( '/accounts/loggedin' )
	else:
		return HttpResponseRedirect( '/accounts/regist' )
  
def loggedin(request):
	return render_to_response('accounts/loggedin.html', {'full_name': request.user.username} )
  
def invalid_login(request):
	return render_to_response( 'accounts/invalid_login.html' )
  
def logout(request):
	auth.logout(request)
	return render_to_response('accounts/logout.html')
  
def regist(request):
	regist_Ctoken = {}
	regist_Ctoken.update( csrf(request) )
	return render_to_response('accounts/regist.html', regist_Ctoken )

def regist_view(request):
	userMail = request.POST.get('userMail','')
	userPass = request.POST.get('userPass','')
	repeatPass= request.POST.get('repeatPass','')
	userName = request.POST.get('userName','')
	userSex = request.POST.get('userSex','')
	userPhone = request.POST.get('userPhone','')
	userAddr = request.POST.get('userAddr','')
  

	if ( userMail == "" or userPass == "" or repeatPass == "" or userName == "" or userSex == "" or userPhone == "" or userAddr == "" ):
		return render_to_response('accounts/error.html')
	else:
		if (isEmailFormat( userMail ) == True):
			c = Customer(name=userName,sex=userSex,phone=userPhone,address=userAddr)
			c.save()
		return render_to_response('accounts/registration_complete.html')

def isEmailFormat( email ):
	if re.match(r'[\w.-]+@[\w.-]+', email ):
		return True
	else:
		return False

#####################################################################################################
##################################### Present Page Setting R/W ######################################
#####################################################################################################
		
def GetThemeClass_view(request):
	themeClasses = ThemeClass.objects.all().order_by('id')
	data = []
	for themeClass in themeClasses:
		data = data + [{'theme_class_id' : themeClass.id, 'theme_class_name' : themeClass.name , 'url' : "/" + urlunquote(themeClass.pic.url), 'info' : themeClass.describe }]
	JData = json.dumps(data)
	return HttpResponse(JData)


def SaveThemeSelected_view(request):
	if request.user.is_authenticated():
		theme=Theme.objects.get( name=request.POST.get('themeName') )
		id = uuid.uuid4().hex
		while Page.objects.filter( pageId = id ):
			id = uuid.uuid4().hex
		page=Page( pageId=id, customer=Customer.objects.get(customer=request.user) , themeClass=theme.themeClass, theme=theme)
		page.save()
		return HttpResponse( json.dumps( { 'pageId': id }))
	else:
		return HttpResponse( json.dumps( {"error" : "請先登入或註冊會員!!" }))

		
def UploadPhoto_view(request):
	pageId = request.POST.get('pageId')
	page = Page.objects.get(pageId=pageId)
	# if ( len(Photo.objects.filter(page=page)) >= page.theme.photoAmount ):
	# 	return HttpResponse( json.dumps({ "error" : "The amount of uploaded photos already reach the limit!!" }) )
	
	for upload_file in request.FILES.getlist('files[]'):
		if upload_file.size > 4000000:
			return HttpResponse( json.dumps({"error":"照片大小不得超過4MB!"}) )

	if ( page.customer == Customer.objects.get( customer=request.user ) ):
		
		photoInfo = []

		for img_file in request.FILES.getlist('files[]'):

			img_type = imghdr.what(img_file)

			im = Image.open(img_file)
			im_w, im_h = im.size

			if ( im_h > 800 ):
				new_h = 800
				new_w = im_w*800/im_h
				im_resize = im.resize((new_w, new_h), Image.ANTIALIAS)
				u_id = uuid.uuid4().hex
				im_resize.save('media/image_processing/' + u_id + '.' + img_type, format=img_type)
				photo = Photo(page=page,ori_image=img_file, resize_image = File(open('media/image_processing/' + u_id + '.' + img_type, 'r')))
				photo.save()
				os.remove('media/image_processing/' + u_id + '.' + img_type)
			else:
				photo = Photo(page=page,ori_image=img_file, resize_image=img_file)
				photo.save()
			
			photoInfo= photoInfo + [{'positionLeft':photo.positionLeft, 'positionTop':photo.positionTop, 'zoom':photo.zoom, 'startAnimate':photo.startAnimate, 'startAnimateSeconds':photo.startAnimateSeconds,'endAnimate':photo.endAnimate, 'endAnimateSeconds':photo.endAnimateSeconds, 'displaySeconds':photo.displaySeconds, 'url': "/"+urlunquote(photo.resize_image.url) }]
		
		return HttpResponse(json.dumps({"photo":photoInfo}))
	else:
		return HttpResponse(json.dumps({"error" : "Sorry! You dont have permission to set the page!"}))

def RemovePhoto_view(request):
	if not request.user.is_authenticated():
		return HttpResponse( json.dumps({"error": "請先登入或註冊會員!!"}) )
	
	pageId = request.POST.get('pageId')

	if not Page.objects.filter(pageId=pageId):
		return HttpResponse( json.dumps({"error": "該頁面不存在!!"}) )

	page = Page.objects.get(pageId=pageId)

	customer = Customer.objects.get(customer=request.user)

	if page.customer != customer:
		return HttpResponse( json.dumps({"error": "權限不足,請重新登入!!"}) )

	photos = Photo.objects.filter(page=page)

	for photo in photos:
		if ( '/' + urlunquote( photo.resize_image.url ) == request.POST.get('url') ):
			url = request.POST.get('url')
			
			photo.delete()
			return HttpResponse( json.dumps({"remove": url}) )

	return HttpResponse( json.dumps({"error": "搜尋不到此照片!!"}) )


def GetUploadPhotos_view(request):
	page = Page.objects.get( pageId=request.POST.get('pageId'))
	
	if ( page.customer == Customer.objects.get( customer=request.user ) ):
		photos = Photo.objects.filter(page=page).order_by('id')
		data = []
		for photo in photos:
			data = data + [{'positionLeft':photo.positionLeft, 'positionTop':photo.positionTop, 'zoom':photo.zoom, 'startAnimate':photo.startAnimate, 'startAnimateSeconds':photo.startAnimateSeconds,'endAnimate':photo.endAnimate, 'endAnimateSeconds':photo.endAnimateSeconds, 'displaySeconds':photo.displaySeconds, 'url': "/"+urlunquote(photo.resize_image.url) }]
		
		return HttpResponse( json.dumps({"photos":data}) )
	
	else:
		return HttpResponse(json.dumps({"error" : "Sorry! You dont have permission to set the page!"}))
		
def SavePhotoSetting_view(request):
	page = Page.objects.get( pageId=request.POST.get('pageId'))
	
	error_msg = ""	

	if ( page.customer == Customer.objects.get( customer=request.user ) ):

		db_photos = Photo.objects.filter(page=page).order_by('id')
		for db_photo in db_photos:
			i = 0;
			while ( i < len(db_photos) and request.POST.get('photos[' + str(i) + '][url]') != ( '/' + urlunquote(db_photo.resize_image.url) ) ):
				i = i+1
			describe = request.POST.get('photos[' + str(i) + '][describe]')
			zoom = float( str( request.POST.get('photos[' + str(i) + '][zoom]') ));
			startAnimateSeconds = float(request.POST.get('photos[' + str(i) + '][startAnimateSeconds]'))
			endAnimateSeconds = float(request.POST.get('photos[' + str(i) + '][endAnimateSeconds]'))
			displaySeconds = float(request.POST.get('photos[' + str(i) + '][displaySeconds]'))
			
			if ( zoom > 1.5 or zoom < 0.5 ):
				return HttpResponse( json.dumps({"error":"Zoom value of this photo out of range: " + urlunquote(db_photo.resize_image.url)}) )
				
			if ( startAnimateSeconds > 5 or startAnimateSeconds < 0 ):
				return HttpResponse( json.dumps({"error":"Time of start animate of this photo out of range: " + urlunquote(db_photo.resize_image.url)}) )
				
			if ( endAnimateSeconds > 5 or endAnimateSeconds < 0 ):
				return HttpResponse( json.dumps({"error":"Time of end animate of this photo out of range: " + urlunquote(db_photo.resize_image.url)}) )
				
			if ( displaySeconds > 9 or displaySeconds < 0 ):
				return HttpResponse( json.dumps({"error":"Time of display of this photo out of range: " + urlunquote(db_photo.resize_image.url)}) )

			if ( i < len(db_photos) ):
				if len(describe) > 50:
					error_msg = "照片描述不得超過50個字!!"
				else:
					describe = str(describe.encode('utf-8'))
					db_photo.positionLeft = float(request.POST.get('photos[' + str(i) + '][positionLeft]'))
					db_photo.positionTop = float(request.POST.get('photos[' + str(i) + '][positionTop]'))
					db_photo.zoom = zoom
					db_photo.startAnimate = request.POST.get('photos[' + str(i) + '][startAnimate]')
					db_photo.startAnimateSeconds = startAnimateSeconds
					db_photo.endAnimate = request.POST.get('photos[' + str(i) + '][endAnimate]')
					db_photo.endAnimateSeconds = endAnimateSeconds
					db_photo.displaySeconds = displaySeconds
					db_photo.describe = describe
					db_photo.save()
			else:
				return HttpResponse( json.dumps({'error' : "This Photo has not be fount: url: " + urlunquote(db_photo.resize_image.url)}));

		if error_msg == "":
			return HttpResponse(json.dumps({"success" : "success"}))
		else :
			return HttpResponse(json.dumps({"error" : error_msg}))	
	else:
		return HttpResponse(json.dumps({"error" : "Sorry! You dont have permission to set the page!"}))
		
def GetTextContent_view(request):
	page = Page.objects.get( pageId=request.POST.get('pageId'))
	
	if ( page.customer == Customer.objects.get( customer=request.user ) ):
		text = page.text
		return HttpResponse(json.dumps({"text" : text}))
	else:
		return HttpResponse(json.dumps({"error" : "Sorry! You dont have permission to set the page!"}))
		
def SaveTextContent_view(request):
	page = Page.objects.get( pageId=request.POST.get('pageId'))
	
	if ( page.customer == Customer.objects.get( customer=request.user ) ):
		text = request.POST.get('text')
		if len(text) > 2500:
			return HttpResponse(json.dumps({"error" : "日記不得超過500字!"}))
		page.text = text.encode('utf-8')
		page.save()
		return HttpResponse(json.dumps({"success" : "success"}))
	else:
		return HttpResponse(json.dumps({"error" : "Sorry! You dont have permission to set the page!"}))

def GenerateQR_view(request):
	page = Page.objects.get( pageId=request.POST.get('pageId'))
	if ( page.customer == Customer.objects.get( customer=request.user ) ):

		pageURL="http://10.68.68.132:8000/page/"+request.POST.get('pageId')
		# pageURL="http://clide.rs/page/"+request.POST.get('pageId')

		qr = qrcode.QRCode(
			version=1,
			error_correction=qrcode.constants.ERROR_CORRECT_L,
			box_size=10,
			border=2,
		)
		qr.add_data(pageURL)
		qr.make(fit=True)
		
		page.path = pageURL
		img = qr.make_image()
		
		qrImg_IO = StringIO.StringIO()
		img.save(qrImg_IO, 'PNG')
		img_file = InMemoryUploadedFile(qrImg_IO, None, None, 'image/png', qrImg_IO.len, None)
		
		fileName = page.pageId + ".png"
		page.qrCode.save( fileName, img_file, save=True)
		page.save()
		data = { 'path': page.path, 'qrImgPath': '/' + urlunquote(page.qrCode.url) }
		'''
		title=u'您的QRCode! 感謝使用Cliders!'.encode('utf-8')
		html_message=(u'<img src="http://clide.rs/'+ urlunquote(page.qrCode.url) + u'" / > <BR/> <p>url: '+ pageURL + u'</p> <BR/><BR/> <p>Clide.rs</p><p>動資科技 DonZ Inc.</p><p>E-mail: donz@donz.tw</p> ').encode('utf-8')	
		msg = EmailMultiAlternatives(title, '', 'do-not-reply@clide.rs', [request.user.username])
		msg.attach_alternative(html_message, 'text/html')
		msg.send()
		'''
		return HttpResponse(json.dumps(data))		
	else:
		return HttpResponse(json.dumps({"error" : "Sorry! You dont have permission to set the page!"}))
	
def ProducePage_view(request, uid=None):

	if ( Page.objects.filter(pageId=uid) ):
		page = Page.objects.get(pageId=uid)

		db_photos = Photo.objects.filter(page=page).order_by('id')
		phdata=[]

		if( len(db_photos) > 0):
			preview_photo = ''
		else:
			preview_photo = '/media/images/theme_img/CloudSliders/officialDemo.jpg'
		for photo in db_photos:
			phdata = phdata + [{'positionLeft':photo.positionLeft, 'positionTop':photo.positionTop, 'zoom':photo.zoom, 'startAnimate':photo.startAnimate, 'startAnimateSeconds':photo.startAnimateSeconds,'endAnimate':photo.endAnimate, 'endAnimateSeconds':photo.endAnimateSeconds, 'displaySeconds':photo.displaySeconds, 'url': "/"+urlunquote(photo.resize_image.url), 'describe': photo.describe }]
			preview_photo =  preview_photo + '<link rel="image_src" href="'+photo.resize_image.url+'"/>' 
		
		photos = json.dumps(phdata)
		
		text = page.text
		contents = json.dumps(text)
		
		product = page.themeClass;
		animates_relation = AnimateRelation.objects.filter(product=product).order_by('id')
		
		in_ani = [];
		out_ani = [];
		
		for animate_relation in animates_relation:
			if ( animate_relation.animate.animate_class == 1):
				in_ani = in_ani + [animate_relation.animate.animate]
			elif ( animate_relation.animate.animate_class == 2 ):
				out_ani = out_ani + [animate_relation.animate.animate]
			else:
				in_ani = in_ani + [animate_relation.animate.animate]
				out_ani = out_ani + [animate_relation.animate.animate]
		
		in_animate = json.dumps(in_ani)
		out_animate = json.dumps(out_ani)
		
		title = page.title.encode('utf-8')
		page_title = json.dumps(title)

		def date_handler(obj):
			return obj.isoformat() if hasattr(obj,'isoformat') else obj

		editDate = page.editDate
		page_editDate = json.dumps(date_handler(editDate))
		description = '與你分享生活中的點滴'
		des_str = unicode(description,'utf-8')
		
		page_des_str = '<meta name="title" content="'+title+'"><meta name="description" content="'+des_str+'">'+preview_photo

		return render(request, page.theme.templateName, { 'photos': photos, "contents" : contents, "in_animations": in_animate, "out_animations": out_animate, "page_title":page_title,"page_editDate":page_editDate,"description_fb":page_des_str})
	else:
		return HttpResponseRedirect( 'http://clide.rs' )

# ===================== cliders wedding project =================================================

def AddNewPage_view(request, className=None, themeName=None, pageId=None):
	
	if not request.user.is_authenticated:
		return HttpResponse( json.dumps( {"error": "請先登入或註冊會員!!"} ) )

	if className is None or themeName is None:
		return HttpResponse( json.dumps( {"error": "該服務不存在!!"} ) )

	if not ( ThemeClass.objects.filter(name=className) and Theme.objects.filter(name=themeName) ):
		return HttpResponse( json.dumps( {"error": "該服務不存在!!"} ) )

	themeClass = ThemeClass.objects.get(name=className)
	theme = Theme.objects.get(name=themeName)

	if themeClass.derectCreatePage:

		photos_D=[]
		text=""
		title=""
		date=""
		if pageId == "new":
			photos_J = json.dumps(photos_D)
			text_J = json.dumps(text)
			pId = ""
			pageId_J = json.dumps(pId)
			title_J=json.dumps(title)
			date_J=json.dumps(date)
			return render( request, themeClass.setting_template_name, { "class_name": themeClass.name, "theme_name": theme.name, "photos": photos_J, "text": text_J, "pageId": pageId_J, 'page_title': title_J, 'page_date': date_J } )
		else:
			if Page.objects.filter(pageId=pageId):
				page = Page.objects.get(pageId=pageId)
			else:
				return HttpResponse( json.dumps({ "error": "頁面不存在,請重新整理頁面!" }) )

			photos = Photo.objects.filter(page=page)
			for photo in photos:
				photos_D = photos_D + [{ 	'url': '/' + urlunquote(photo.resize_image.url), 
											'positionLeft': photo.positionLeft, 
											'positionTop':photo.positionTop, 
											'zoom': photo.zoom, 
											'startAnimate': photo.startAnimate, 
											'startAnimateSeconds': photo.startAnimateSeconds, 
											'endAnimate': photo.endAnimate, 
											'endAnimateSeconds': photo.endAnimateSeconds,
											'displaySeconds': photo.displaySeconds,
											'describe': photo.describe }]
			photos_J = json.dumps(photos_D)

			text = page.text
			text_J = json.dumps(text)

			pId=pageId
			pageId_J = json.dumps(pId)

			title=page.title
			title_J = json.dumps(title)

			date = str(page.date)
			date_J = json.dumps(date)

			return render( request, themeClass.setting_template_name, { "class_name": themeClass.name, "theme_name": theme.name, "photos": photos_J, "text": text_J, "pageId": pageId_J, 'page_title': title_J, 'page_date': date_J } )
	else:
		id = uuid.uuid4().hex
		while Page.objects.filter( pageId = id ):
			id = uuid.uuid4().hex

		page = Page( pageId=id, customer=Customer.objects.get(customer=request.user), themeClass=ThemeClass.objects.get(name=class_name), theme=Theme.objects.get(name=theme_name), editDate=datetime.datetime.now(), presentDueDate=datetime.datetime.now()+datetime.timedelta(days=themeClass.dayDuration) )
		page.save()
		return HttpResponse( json.dumps({ "pageId": page.pageId, "dueDate": str(localtime(page.presentDueDate) ) }) )


# ===================== last judge view =========================================================
def JudgeThemeClassName_view(request, uString=None):
	if uString is None or uString == "":
		return index(request)
	else:
		if ThemeClass.objects.filter(name=uString):
			themeClass=ThemeClass.objects.get(name=uString)
			if themeClass.derectCreatePage:
				return index(request)
			else:
				if not request.user.is_authenticated:
					return HttpResponse( json.dumps( {"error": "請先登入或註冊會員!!"} ) )

				theme=Theme.objects.filter(themeClass=themeClass)
				themeJName = json.dumps( { "name": theme[0].name } )
				classJName = json.dumps( {"name": themeClass.name} )
				pages = Page.objects.filter(customer=Customer.objects.get(customer=request.user),themeClass=themeClass).order_by('presentDueDate')
				page_list=[]
				for tempPage in pages:
					page_list = page_list + [{ 'pageId': tempPage.pageId, 'dueDate': str(localtime(tempPage.presentDueDate)) }]
				if( page_list==[] ):
					page_list_J = json.dumps( {"none":"none"} )
				else:
					page_list_J = json.dumps(page_list)

				return render( request, themeClass.setting_template_name,  { "class_name": classJName , "theme_name": themeJName , "project_list": page_list_J } )
		else:
			return index(request)

def WeddingSetting_view(request, uid=None):
	if not request.user.is_authenticated:
		return HttpResponse("請先登入或註冊會員")
	if uid is None:
		return HttpResponse("專案不存在")
	pagetheme = Page.objects.get(pageId=uid).themeClass
	photoframes = PhotoFrame.objects.filter(themeClass=pagetheme)
	photoframe_all = [];
	for frame in photoframes:
		photoframe_all = photoframe_all + [{ 'name':frame.name ,'url': '/' + urlunquote(frame.frameImage.url)}]
	photoframes_J = json.dumps(photoframe_all)
	load_photos = Photo.objects.filter(page=Page.objects.get(pageId=uid))
	photo_all  = [];
	for photo in load_photos:
		photo_all = photo_all + [{ 'positionLeft': photo.positionLeft,'positionTop':photo.positionTop,'zoom':photo.zoom ,'url': '/' + urlunquote(photo.resize_image.url)}]
	photo_J = json.dumps(photo_all)
	return render(request,'WeddingCardSetting.htm',{'pageID': uid,'photoframes':photoframes_J,'photos':photo_J})

def SavePhotoFrame_view(request):
	uid = request.POST.get('pageId')
	name = request.POST.get('name')
	if not request.user.is_authenticated:
		return HttpResponse("請先登入或註冊會員")
	if uid is None:
		return HttpResponse("專案不存在")
	if name is None:
		return json.dumps( {"error" : "請選擇一個主題！"} )
	photos = Photo.objects.filter(page=Page.objects.get(pageId=uid))
	for photo in photos:
		photo = PhotoFrame.objects.filter(name=name)

def SavePage_view(request):
	if not request.user.is_authenticated:
		return HttpResponse("請先登入或註冊會員")

	customer = Customer.objects.get(customer=request.user)

	yy = request.POST.get('yy')
	mm = request.POST.get('mm')
	dd = request.POST.get('dd')

	try:
		yy = int(yy)
	except valueError:
		return HttpResponse( json.dumps({ "error" : "日期格式錯誤!" }) )
	try:
		mm = int(mm)
	except valueError:
		return HttpResponse( json.dumps({ "error" : "日期格式錯誤!" }) )
	try:
		dd = int(dd)
	except valueError:
		return HttpResponse( json.dumps({ "error" : "日期格式錯誤!" }) )

	pages = Page.objects.filter(customer=customer, date=datetime.date(yy,mm,dd))
	page_info=[];
	for page in pages:
		photos = Photo.objects.filter(page=page)
		print(photos)
		page_info = page_info +[{ 'title':page.title, 'pageId':page.pageId, 'url':page.path }]
	return  HttpResponse( json.dumps( {'page_info':page_info} ) )

def RemovePage_view(request):
	if not request.user.is_authenticated:
		return HttpResponse("請先登入或註冊會員")
	customer = Customer.objects.get(customer=request.user)
	pageId = request.POST.get('pageId')
	target_page = Page.objects.get(pageId=pageId)
	if ( target_page.customer == customer ):
		target_page.delete()
		return HttpResponse(json.dumps({'success':'success'}))
	else:
		return HttpResponse(json.dumps({"error":"Sorry! You dont have permission to set the page!"}))

def EditPage_view(request):
	if not request.user.is_authenticated:
		return HttpResponse("請先登入或註冊會員")
	pageId = request.POST.get('pageId')
	customer = Customer.objects.get(customer=request.user)
	target_page = Page.objects.get(pageId=pageId)

	if ( target_page.customer == customer ):
		page_text = target_page.text
		page_photos = Photo.objects.filter(page=target_page)
		photo_all = [];
		for photo in page_photos:
			photo_all = photo_all + [{'positionLeft':photo.positionLeft, 'positionTop':photo.positionTop, 'zoom':photo.zoom, 'startAnimate':photo.startAnimate, 'startAnimateSeconds':photo.startAnimateSeconds,'endAnimate':photo.endAnimate, 'endAnimateSeconds':photo.endAnimateSeconds, 'displaySeconds':photo.displaySeconds, 'url': "/"+urlunquote(photo.resize_image.url), 'describe':photo.describe }]
		return render( 'CloudSliderSetting.htm',json.dumps( { 'text': page_text,'pageId': pageId,'photos' : photo_all } ))
	else:
		return HttpResponse(json.dumps({"error" : "Sorry! You dont have permission to set the page!"}))

