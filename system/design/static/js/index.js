﻿window.$official_slides = $("#official_slides");
window.$official_win = $("#offcial_win");
window.current_official_slide = 1;
window.userObj = [];


$(	function() {
	GetUser() ;				//後台銜接後執行此行
})


/* ============================================================================= */
/* ========================= official slide ==================================== */
/* ============================================================================= */

function GetUser() {
	$.ajax( {
		type: 'GET',
		url: "GetUser/",
		dataType: 'json',
		success: function(Jdata) {
			window.userObj = Jdata;
			InitialOfficialPage();
			InitalPageList();
			HashChange();
		},
		error: function(Jdata) {
			alert("AUTH ERROR");
		}
	})
}
function InitalPageList(){
	$(".return_home_page").click(function(e){
		e.preventDefault();
		viewModel.currentPage(0);
	});
}
$(window).resize(function(){
	var $fb_share_btn = $(".fb-share-button");
	
	WindowResizeElementAdjust();

	if( $(window).width() < 1170 ){
		$fb_share_btn.html("FB分享");
	}
	else{
		$fb_share_btn.html("FaceBook分享");
	}
	
});

function WindowResizeElementAdjust(){
	var $add_page_btn_div = $("#add_page_btn_div");
	var $personal_date_picker_div = $("#personal_date_picker_div");
	var $data_content_date = $(".data_content_date");
	var $date_container_list = $("#date_container_list");
	var $picker_frame = $("#picker_frame");
	var $personal_page = $("#personal_page");
				 
	//set position of the button
	var add_btn_left = parseInt($picker_frame.css("margin-left"))+$picker_frame.width()+70-$add_page_btn_div.width()*2/3;
	$add_page_btn_div.css("margin-left",add_btn_left);
	$add_page_btn_div.css("margin-top",-$add_page_btn_div.height()/2);

	var content_date_width = $(".panel").width()-10-$("#return_datepicker_btn_div").width();
	$data_content_date.width(content_date_width);
	
}
function Loading(){
	var $loading_mask = $("#loading_mask");
	$loading_mask.width($(window).width()).height($(window).height());
}

function HashChange(){
	$(window).on('hashchange',function(e){
		e.preventDefault();
		var hash = window.location.hash.split('#')[1];
		var $loading_mask = $("#loading_mask");
		
		if( hash == "photo_diary" ){
			$loading_mask.css("display","none");
			HideDateList(function(){
				ShowDatePicker();
			});
			
		}
		else{ // hash = yyyy-mm-dd
			var yy = parseInt(hash.split('-')[0]);
			var mm = parseInt(hash.split('-')[1]);
			var dd = parseInt(hash.split('-')[2]);
			$loading_mask.css("display","none");
			if(window.select_boolean)
				window.select_boolean=false;
			else
				AppendPagListElement(yy,mm,dd);
			HideDatePicker(function(){
				ShowDateList();
			});	
		}
	});	
}
function ShowDatePicker(){
	viewModel.currentPage(1);
	var $date_picker_group = $("#personal_date_picker,#add_page_btn_div,#picker_frame");
	TweenMax.fromTo($date_picker_group,0.35,{"opacity":0},{"opacity":1,ease:"Power0.easeNone",onStart:WindowResizeElementAdjust});
}
function ShowDateList(){
	viewModel.currentPage(2);
	var $data_content = $("#date_container_list,#return_datepicker_btn_div,.data_content_date");
	TweenMax.fromTo( $data_content,0.35,{"opacity":0},{"opacity":1,ease:"Power0.easeNone",onStart:WindowResizeElementAdjust,onComplete:ShowDateListComplete} );
	
	function ShowDateListComplete(){
		$(".data_content_date").css("opacity","");
	}
}
function HideDatePicker( callback ){
	var $date_picker_group = $("#personal_date_picker,#add_page_btn_div,#picker_frame");
	TweenMax.fromTo($date_picker_group,0.35,{"opacity":1},{"opacity":0,ease:"Power0.easeNone",onComplete:HideDatePickerComplete});
	
	function HideDatePickerComplete(){
		callback();
	}
}
function HideDateList( callback ){
	var $data_content = $("#date_container_list,#return_datepicker_btn_div,.data_content_date");
	TweenMax.fromTo( $data_content,0.35,{"opacity":1},{"opacity":0,ease:"Power0.easeNone",onComplete:HideDateListComplete});	
	
	function HideDateListComplete(){
		callback();
	}
}

// function LoadMsgPage( process ){
// 	viewModel.currentPage(3);
// }

// function PhotoDiaryAnimation(){
// 	var $date_container_list = $("#date_container_list");
// 	var $return_datepicker_btn_div = $("#return_datepicker_btn_div");
// 	var $personal_date_picker = $("#personal_date_picker");
// 	var $add_page_btn_div = $("#add_page_btn_div");
// 	var $personal_page = $("#personal_page");
// 	var $picker_frame = $("#picker_frame");
	
// 	TweenMax.fromTo( [$date_container_list,$return_datepicker_btn_div],0.35,{"opacity":1},{"opacity":0,ease:"Power0.easeNone",onComplete:DisplayDateList} );	
// 	function DisplayDateList(){
// 		viewModel.currentPage(1);
// 		TweenMax.fromTo( [ $personal_date_picker, $picker_frame, $add_page_btn_div ],0.35,{"opacity":0},{"opacity":1,ease:"Power0.easeNone",onStart:DisplayNoneReturnBtn} );
// 		function DisplayNoneReturnBtn(){
// 			$return_datepicker_btn_div.css("display","none");
// 			$add_page_btn_div.css("display","block");
// 			$personal_date_picker.css("display","block");
// 			WindowResizeElementAdjust();

// 		}	
// 	}	
// }
// function YYMMDDAnimation(){
// 	var $personal_date_picker = $("#personal_date_picker");
// 	var $add_page_btn_div = $("#add_page_btn_div");
// 	var $date_container_list = $("#date_container_list");
// 	var $return_datepicker_btn_div = $("#return_datepicker_btn_div");
// 	var $picker_frame = $("#picker_frame");

// 	TweenMax.fromTo( [ $personal_date_picker, $picker_frame, $add_page_btn_div ],0.35,{"opacity":1},{"opacity":0,ease:"Power0.easeNone",onComplete:DisplayPhotoDiary} );
	

// 	function DisplayPhotoDiary(){
// 		viewModel.currentPage(2);
// 		TweenMax.fromTo( [ $date_container_list,$return_datepicker_btn_div ],0.35,{"opacity":0},{"opacity":1,ease:"Power0.easeNone",onStart:DisplayBlockReturnBtn} );
		
// 		function DisplayBlockReturnBtn(){
// 			$date_container_list.css("display","block");
// 			$return_datepicker_btn_div.css("display","inline-block");
// 			$add_page_btn_div.css("display","none");
// 			WindowResizeElementAdjust();
// 		}
// 	}
// }



function InitialOfficialPage() {
	var $login_button = $("#login_button");
	var $register_button = $("#register_button");
	var $register_info = $($('#register_info'),$('#welcome'));
	var $return_datepicker_btn_div = $("#return_datepicker_btn_div");

	// $return_datepicker_btn_div.css("display","none");

	if ( window.userObj == null ) {
		$login_button.text("登入");
		$register_button.text('註冊');
		if( window.register.status == 'off' ){
			$('<p><button id="welcome_login_button" type="button" style="margin-bottom:10px;" class="btn btn-primary  btn-md">登入</button><button id="welcome_register_button" type="button" style="margin-left:10px; margin-bottom:10px;" class="btn btn-primary btn-md">註冊</button></p>').appendTo($("#welcome"));
			$register_info.html('請先註冊會員(尚餘'+ window.register.remain +'個名額)');
		}
		else if ( window.register.status == 'on' ){
			$('<p><button id="welcome_login_button" type="button" style="margin-bottom:10px;" class="btn btn-primary  btn-md">登入</button></p>').appendTo($("#welcome"));
			$register_info.html(' (註冊人數已滿)');
		}
		else
			;
		SetUnloggedIconClick();

	}
	else {
		$register_info.remove();
		$login_button.text(window.userObj.name);
		$login_button.unbind("click");
		$register_button.text('登出');
		$register_button.unbind("click");
		$register_button.attr("href","Logout/");

		$("#functional_nav").append($('<li><a id="personal_page_nav" href="#">我的日誌</a></li>'));
		SetLoggedButtonClick();
		viewModel.currentPage(1);
		InitialPersonalPage();		
	}

	NewsInfomationAppend();
	
	SetButtonClick();

}

function AppendPagListElement( yy,mm,dd ){
	var $personal_date_picker = $("#personal_date_picker");
	var $add_page_btn_div = $("#add_page_btn_div");
	var $personal_date_picker_div = $("#personal_date_picker_div");
	var $title_container = $("#title_container");
	var $container =  $("#date_container_list");;
	var $date_title = $('<div class="data_content_date"> \
							'+yy+'年'+mm+'月'+dd+'日\
						</div>');
	//switch to loading message page
	// viewModel.currentPage(3);

	//clear the content of last review
	$container.html("");
	$(".data_content_date").remove();

	$("#return_datepicker_btn_div").before($date_title);
	
	$.ajax({
		type:'POST',
		datatype:'json',
		data:{'yy':yy,'mm':mm,'dd':dd},
		url:"SavePage/",
		success:function(JData){
			if(JData.error)
				alert(JData.error);
			else{
				var pages_info = new Array();
				var pages = JSON.parse(JData);
				window.pages = pages;
				page_info = pages.page_info;

				for(var i=0;i<page_info.length;i++){
					var $data_content = $('<div class="col-xs-12 data_content"> \
											<div id="'+page_info[i].pageId+'" class="panel panel-info"> \
												<div class="panel-heading" style="word-wrap: break-word; word-break: normal;"> \
													標題: <a href="'+page_info[i].url+'"> '+page_info[i].title+'</a> \
												</div> \
												<div class="panel-body"> \
													<button data-id="'+page_info[i].pageId+'" style="margin-right:10px;" class="col-xs-3 panel_edit_btn btn btn-primary">編輯</button>\
													<a class="fb-share-button btn btn-primary col-xs-3" data-id="'+page_info[i].pageId+'" data-href="'+page_info[i].url+'" style="margin-right:10px;">Facebook分享</a>\
													<button data-id="'+page_info[i].pageId+'" class="col-xs-3 panel_delete_btn btn btn-danger">刪除</button>\
												</div>\
											</div> \
										</div>');
					$data_content.appendTo($container);
				}

				var $fb_share_btn = $(".fb-share-button");
				var $panel_delete_btn = $(".panel_delete_btn");
				var $panel_edit_btn = $(".panel_edit_btn");
				var $return_datepicker_btn = $("#return_datepicker_btn");

				if( $(window).width() < 1170 ){
					$fb_share_btn.html("FB分享");
				}
				else{
					$fb_share_btn.html("FaceBook分享");
				}

				$return_datepicker_btn.click(function(e){
					e.preventDefault();
					var $this = $(this);
		           	$container.html("");
		           	$("#personal_date_picker").datepicker( "refresh" );
		           	window.location.hash = "photo_diary";       		
		        });

				$fb_share_btn.click(function(e){
					e.preventDefault();
					var url = $(this).attr('data-href');
					FB.ui({
						method:'feed',
						display:'popup',
						link: url,
					});
				});

				
				$panel_delete_btn.click(function(e){
					e.preventDefault();
					var $this = $(this);
					var pageId = $this.attr('data-id');

					$.ajax({
						type:'POST',
						datatype:'json',
						data:{'pageId':pageId},
						url:'RemovePage/',
						success:function(JData){
							if(JData.error){
								alert(JData.error)
							}
							else{
								ObjectDelete( pageId );
								var $panel_footer = $this.parent();
								var $panel = $panel_footer.parent();
								var $data_content = $panel.parent();

								$data_content.remove();
							}
						},
						error:function(JData){
							alert('頁面刪除失敗！');
						}
					});
				});

				$panel_edit_btn.click(function(e){
					e.preventDefault();
					var $this = $(this);
					var pageId = $this.attr('data-id');
					window.location.href = '/AddNewPage/CloudSliders/CloudSliders/'+pageId ;
				})
			}
			// YYMMDDAnimation();
			window.location.hash = yy+"-"+mm+"-"+dd;
		},
		error:function(){
			alert("重新設定照片失敗！");
		}
	});

	

}

function InitialPersonalPage() {
	window.location.hash = "photo_diary";
	var $add_page_btn = $("#add_page_btn");
	var $add_page_btn_div = $("#add_page_btn_div");
	var $personal_page = $("#personal_page");
	
	var user_pages = window.userObj.user_pages;
	var current_date_calendar = $("td",".ui-datepicker-calendar");	
	

	$("#personal_date_picker").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat:'yy-mm-dd',
		buttonImage:"",
		monthNames:["ㄧ月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
		monthNamesShort:["ㄧ月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
		dayNames:["日","一","二","三","四","五","六"],
		dayNamesMin:["日","一","二","三","四","五","六"],
		beforeShowDay: function(date) {
			
            var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
            for (var i = 0; i < user_pages.length; i++) {
            	var pageId = user_pages[i].pageId;
				var page_date = user_pages[i].date;
				
				if ( parseInt(m+1) < 10)
					var mm = '0' + (m+1);
				else 
					var mm = m+1;

				if ( parseInt(d) < 10)
					var dd = '0' + d;
				else 
					var dd = d;

        	    if( (y+'-'+ mm +'-'+dd) == page_date){
                    return [true, 'ui-state-active',''];
                }
            }

            return [false,'gray',''];
			
        },
        onSelect: function(date) {
        	window.select_boolean = true;
			var yy = parseInt(date.split('-')[0]);
			var mm = parseInt(date.split('-')[1]);
			var dd = parseInt(date.split('-')[2]);
			Loading();
        	AppendPagListElement( yy,mm,dd );

        	// var $return_datepicker_btn_div = $("#return_datepicker_btn_div");
        	// $return_datepicker_btn_div.css("margin-left",$("#date_container_list").width()*2/3);
        }
    });

	var $personal_date_picker_div = $("#personal_date_picker_div");
	var $personal_date_picker = $("#personal_date_picker");
	var $picker_frame = $("#picker_frame");
	$(".ui-datepicker-inline", $personal_date_picker).css("margin","0px auto");
	$picker_frame.width( $(".ui-datepicker-inline", $personal_date_picker).width()+2).height( $(".ui-datepicker-inline", $personal_date_picker).height()+3);

	// $personal_date_picker.css({"border-top":"#428bca 25px solid","border-bottom":"#428bca 25px solid"});
	
	// $(".ui-datepicker").css({"border-top":"#428bca  20px solid","border-bottom":"#428bca 20px solid"});

	
	//set position of the button
	WindowResizeElementAdjust()
	
	$add_page_btn.css("border-radius",$add_page_btn_div.width()/2).css("background-color","#69D2E7").css("border-color","transparent");
	
	

	$add_page_btn.click(function(e){
		e.preventDefault();	
		location.href = '/AddNewPage/CloudSliders/CloudSliders/new';
	});

	
	
}
function ObjectDelete(pageId){
	var index = 0;
	for(var i=0;i<window.userObj.user_pages.length;i++){
		if(window.userObj.user_pages[i].pageId==pageId)
			index = i;
		else;
	}
	window.userObj.user_pages.splice(index,1);


}

function SetLoggedButtonClick() {
	$("#login_button").click( function(e){
		e.preventDefault();
		viewModel.currentPage(1);
		window.location.hash = "photo_diary";
	})
	$("#list_button").click( function(e){
		e.preventDefault();
	});
	$("#personal_page_nav").click(function(e){
		e.preventDefault();
		viewModel.currentPage(1);
		window.location.hash = "photo_diary";
	});

}

function SetButtonClick() {

	$("#cata_cliders").click( function(e) {
		e.preventDefault();
		viewModel.currentPage(0);
	})

}



function NewsInfomationAppend() {

	for ( var i = 0; i < window.news.length; i++ ) {
		$('<div class="media"></div>').append( $('<a href="#" class="pull-left"> <img id="news_image'+ i +'"src="' + window.news[i].pic + '" alt="" class="media-object news_image" /> </a>') )
									  .append( $('<div class="media-body"> <h4 class="media-heading">'+window.news[i].title+'</h4> <p>'+window.news[i].content+'</p> </div>') )
									  .appendTo( $("#news_container") );	
	}

}


function SetUnloggedIconClick() {
	
	$("#cata_home").click( function(e) {
		e.preventDefault();
		TweenMax.set($("#register_panel"), { "height": 0} );
		TweenMax.set( $("#login_panel"), { "height":0} );
		$($('li'), $("#login_register_nav")).removeClass('active');
		$(this).parent().addClass('active');
	})
	
	$("#cata_service").click( function(e) {
		e.preventDefault();
	})
	
	$("#cata_news").click( function(e) {
		e.preventDefault();
	})
	
	$("#cata_contact").click( function(e) {
		e.preventDefault();
	})
	
	$("#login_button,#welcome_login_button").click( function(e) {
		e.preventDefault();
		if ( $("#login_panel").height() == 0 ) {
			TweenMax.set($("#register_panel"), { "height": 0} );
			TweenMax.to( $("#login_panel"), 1, { "height":150} );
			$('#login_account').focus();
			$($('li'), $("#login_register_nav")).removeClass('active');
			$(this).parent().addClass('active');
		}
		else {
			$($('li'), $("#login_register_nav")).removeClass('active');
			$("#cata_home").parent().addClass('active');
			TweenMax.to( $("#login_panel"), 1, { "height":0} );
		}
	})

	$("#register_button,#welcome_register_button").click( function(e) {
		e.preventDefault();
		if ( $("#register_panel").height() == 0 ) {
			TweenMax.set( $("#login_panel"), { "height":0} );
			TweenMax.to($("#register_panel"), 1, { "height": 315} );
			$('#register_account').focus();
			$($('li'), $("#login_register_nav")).removeClass('active');
			$(this).parent().addClass('active');
		}
		else {
			$($('li'), $("#login_register_nav")).removeClass('active');
			$("#cata_home").parent().addClass('active');
			TweenMax.to($("#register_panel"), 1, { "height": 0} );
		}
	})
	
	$("#login_submit").click( function(e) {
		e.preventDefault();
		var account=$('#login_account').val();
		var password=$('#login_password').val();
		var data = { "account" : account, "password" : password } ;
		$.ajax({
			type: 'POST',
			url: "Login/",
			data: data,
			dataType: 'json',
			success: function(JData) {
				if ( JData.error ) {
					alert(JData.error);
				}
				else
					window.location.reload();
					viewModel.currentPage(1);
			},
			error: function() {
				alert("ERROR!!!");
			}
		});
	})

	$("#register_date_picker").datepicker({
		yearRange: "1910:2010",
		firstday:0,
		showWeek:false,
		changeMonth:true,
		changeYear:true,
		dateFormat:'yy-mm-dd',
		defaultDate: '1995-01-01',
		monthNames:["ㄧ月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
		monthNamesShort:["ㄧ月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"]
	});

	$("#register_date_picker").focus(function () {
    	$("thead",".ui-datepicker-calendar").hide();
	});

	$("#register_submit").click( function(e) {
		e.preventDefault();
		var account=$(register_account).val();
		var password=$(register_password).val();
		var repeat=$(register_repeat).val();
		var name=$(register_name).val();
		var birth=$("#register_date_picker").val();;
		var sex=$('input[name=register_sex]:checked').val();
		var yy = birth.split('-')[0];
		var mm = birth.split('-')[1];
		var dd = birth.split('-')[2];
		var data = { "account" : account, "password" : password, "repeat" : repeat, "name" : name, "sex" : sex ,"yy":yy,"mm":mm,"dd":dd};
		
		$.ajax({
			type: 'POST',
			url: "Regist/",
			data: data,
			dataType: 'json',
			success: function(JData) {
				if ( JData.error ) {
					alert(JData.error);
				}
				else
					window.location.reload();
			},
			error: function() {
				alert("REGISTER ERROR!!!");
			}
		});
	})
}


//------------------------------------------------------------------ Add_By_Cycho
$( document ).ready(function() {

	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				// Does this cookie string begin with the name we want?
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	function csrfSafeMethod(method) {
		// these HTTP methods do not require CSRF protection
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
  
	function sameOrigin(url) {
		// test that a given url is a same-origin URL
		// url could be relative or scheme relative or absolute
		var host = document.location.host; // host + port
		var protocol = document.location.protocol;
		var sr_origin = '//' + host;
		var origin = protocol + sr_origin;
		// Allow absolute or scheme relative URLs to same origin
		return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
			(url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
			// or any other URL that isn't scheme relative or absolute i.e relative.
			!(/^(\/\/|http:|https:).*/.test(url));
	}
	  
	$.ajaxSetup({
		crossDomain: false, // obviates need for sameOrigin test
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type)) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		}
	});
});
//------------------------------------------------------------------ Add_By_Cycho

