﻿//============================================== 2013/11/04 updated by Paul Ku 

window.frame_url ="";

$(window).load( function (){
	$(document).on('touchmove',function(e) {  //禁止拖拉畫面
		e.preventDefault();
	});

	/*====================================================*/
	FramePreviewAppend();
	ClickIconFunction();
	CurrentArrowState();
	/*====================================================*/

	fixLayout();

	if ( /Android/i.test(navigator.userAgent) ) {
		setTimeout( "fixLayout()" ,200 );

		$( window ).on( "orientationchange", function( e ) {
			e.preventDefault();
			setTimeout( "fixLayout()" ,200 );
		});
	}
	else if ( /webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$( window ).on( "orientationchange", function( e ) {
			e.preventDefault();
			fixLayout();
		});
	}
	else {
		$(window).resize( function(e) {
			e.preventDefault();
			fixLayout();
		});
	}
	
	PhotoAppend(function(){
		PhotoAppendAdjust();
	});
});

function PhotoAppend( callback ){
	if( window.photos ){
		for(var i=0;i<window.photos.length;i++){
			PhotoContainerAppend( i );
		}
	}
	else
		;
	callback();
}

function PhotoAppendAdjust(){
		if( window.photos ){
		for(var i=0;i<window.photos.length;i++){
			var left = ReadPhotoPositionLeft(i);
			var top = ReadPhotoPositionTop(i);
			PhotoContainerAdjust( i );
			PhotoPositionAdjust( i,left,top  );
		}
	}
	else
		;

	viewModel.currentSettingPhoto(0);
}

function fixLayout() {
	var win_width = $(window).width();
	var win_height = $(window).height();
	var half_win_width = win_width/2;
	var half_win_height = win_height/2;
	var top_height = 0;
	var foot_height = 0;
	var display_width = win_width;
	var display_height = win_height;

	var $frame_area = $("#frame_area");

	if ( win_width/212*317 + 150 <= win_height) {
		// 寬滿版配置
		// console.log("width_max_1");

		display_width = win_width;
		display_height=win_width/212*317;

		$frame_area.width(display_width);
		$frame_area.height(display_height);
	}
	else if ( win_height*0.75/317*212 <= win_width ) {
		// console.log("height_max");
		// console.log(win_width + ",  " + win_height);

		display_width = win_height*0.75/317*212;
		display_height = win_height*0.75;

		$frame_area.height(display_height);
		$frame_area.width(display_width);
	}
	else {
		// console.log("width_max_2");
		// console.log(win_width + ",  " + win_height);

		display_width = win_width;
		display_height=win_width/212*317;

		$frame_area.width(display_width);
		$frame_area.height(display_height);
	}
	var $album_window = $("#album_window");
	var $display_area = $("#display_area");

	$album_window.width(display_width).height(display_height);
	$album_window.css("margin-top", (win_height - display_height)*0.3 + "px" );
	$album_window.css("margin-left", half_win_width - display_width/2 + "px" );
	$frame_area.css("margin-top", 0);
	$frame_area.css("margin-left", 0);
	$(".frame_img").width(display_width).height(display_height);
	
	// $("#emp_img").width(display_width);
	// $("#emp_img").height(display_height);

	top_height = (win_height - display_height)*0.3;
	foot_height = (win_height - display_height)*0.7;


	$('#display_area').width( display_width*0.9 ).height( display_height*0.8 );
	$('#display_area').css( 'margin-left',display_width*0.05 ).css( 'margin-top', display_height*0.1 );




	
	// ==================================== top component setting ===========================================
	$("#top").width(display_width).height(top_height).css("margin-left", half_win_width - display_width/2 + "px" );
	$("#page_info").width(display_width).height(top_height).css("line-height", top_height+ 10 + "px");
	
	$("#complete_photo_setting")
		.height(top_height*0.8)
		.width(display_width*0.15)
		.css("margin-top",top_height*0.4)
		.css("margin-left",display_width*0.8 + "px");



	// ==================================== foot component setting ===========================================
	$("#foot").width(display_width);
	$("#foot").height(foot_height);
	$("#foot").css("margin-top",top_height + display_height + "px");
	$("#foot").css("margin-left",half_win_width - display_width/2 + "px");

	var animation_width = display_width*0.9;
	var animation_height = foot_height*0.4;


	$('#effect').height(animation_height*0.8)
				.css("line-height", animation_height*0.8+ "px")
				.css("font-size", "14px")
				.css("margin-left", display_width*0.03 + "px")
				.css("margin-right", display_width*0.05 + "px")
				.css("margin-top", animation_height*0.1 );
	$('#animations_div').height(animation_height).css("overflow-y","hidden").css("overflow-x","scroll");
	$('#animations_div').css('margin-top',animation_height*0.1);

	$('.frame_icon').width( (animation_height*0.8)*212/317 ).height( animation_height*0.8 );
	$('.frame_icon').css('margin-right',animation_height*0.1).css('margin-left',animation_height*0.05);

	$('#animations_container').height(animation_height);
	$('#animations_container').width(animation_width-$('#effect').width());
	$('#animations_container').css('margin-top',animation_height*0.1);

	var button_amount = 4 ;
	var button_size = foot_height*0.5;
	var button_margin = button_size*0.25;

	$("#foot_buttons_div").height(button_size).width(display_width*0.9).css("margin-left",display_width*0.05);
	$("#foot_button_container").width((button_size+button_margin)*button_amount).height(button_size);
	$( ".foot_buttons", $("#foot_button_container") ).width(button_size).height(button_size).css("margin-right", button_margin + "px" );
	$( ".edit_zoom", $("#foot_button_container") ).width(0).height(button_size).css("opacity", "0");
	$("#zoom_in").css("margin-left", (display_width*0.9 - button_size) + "px");
	

	$("#photo_upload_area").width(button_size).height(button_size).css("margin-right", button_margin + "px" );
	$("#upload_drop_area").width(button_size).height(button_size).css({"margin-right": button_margin + "px" ,"padding":0});
	$("#upload_drop_area").css('background','transparent').css('border','none').css('border-radius','none');

	$('#animate_window').width( $('#foot_buttons_div').width() ).height( display_height *0.4 );
	$('#animate_window').css('margin-left', -$('#foot_buttons_div').width()/2 );
	$('#animate_window').css('margin-top',-display_height *0.4-$('#display_area').height()*0.2);
	$('#animate_window').css('display','none');
	
	// $('#display_div').css('margin-top', display_height *0.4);
	

	// var display_button_size = ($('#foot_buttons_div').width()*0.9)/3;

	// $('#fast_display').height( display_button_size ).width( display_button_size ).css('margin-left',($('#foot_buttons_div').width()*0.1)/4);
	// $('#middle_display').height( display_button_size ).width( display_button_size ).css('margin-left',($('#foot_buttons_div').width()*0.1)/4);
	// $('#slow_display').height( display_button_size ).width( display_button_size ).css('margin-left',($('#foot_buttons_div').width()*0.1)/4);
	// $('#label_display')
	// 	.width( $('#foot_buttons_div').width() )
	// 	.height( $('#animate_window').height()-display_button_size-display_height*0.4*0.05-20 )
	// 	.css('margin-top', display_height *0.4*0.05);
	// $('.display').css('margin-top', (display_height*0.4-display_button_size-$('#label_display').height)/2 );
	// ==================================== turn page size component setting ===========================================
	var $next_page = $("#next_page");
	var $previous_page = $("#previous_page");

	$next_page.css( 'margin-left', display_width - $('#next_page').width()) ;
	$next_page.css( 'margin-top', (display_height - $('#next_page').height())/2 );
	$previous_page.css( 'margin-left',0);
	$previous_page.css( 'margin-top', (display_height - $('#previous_page').height())/2 );
	// ==================================== text edit button component setting ===========================================
	// $('#edit_text').width( display_width*0.9 ).height( display_height*0.8 );
	// $('#edit_text').css( 'margin-left',display_width*0.05).css( 'margin-top', display_height*0.1+5 );
	
	// $('#text_window').width( display_width ).height( display_height );
	// $('#text_window').css("margin-top",-display_height);
	// $('#text_window').css("margin-left",display_width);


	window.display_rate = display_width/212;
}

function FramePreviewAppend(){
	var $frame_area = $("#frame_area");
	var photo_frame_name = 'null';

	for (var i=0;i<window.picture_frames.length;i++){
		var frame_url = window.picture_frames[i].url;
		var frame_name = window.picture_frames[i].name;
		var frame_icon = $('<div class="frame_icon"></div>').attr('id',i);
		
		frame_icon.css("background-image","url('"+frame_url+"')");
		frame_icon.appendTo($('#animations_container'));

		$( $('#'+i), $('#animations_container')).on('click',function(i){
			$($('.frame_img'),$frame_area).remove();

			var frame_img = new Image();
			var frame_id = $(this).attr('id');	
			var frame_width = $frame_area.width();
			var frame_height = $frame_area.height();
			var frame_zindex = $('#display_area').css('z-index')+10;
			
			$(frame_img)
				.attr('id',frame_name)
				.addClass('frame_img')
				.css('z-index',frame_zindex)
				.width(frame_width)
				.height(frame_height);
			frame_img.src=window.picture_frames[frame_id].url;
			$(frame_img).appendTo($frame_area);

			photo_frame_name = window.picture_frames[frame_id].name;
			console.log("photo_frame_name: "+photo_frame_name);
		});
	}

	$("#complete_photo_setting").click(function(){
		SavePhotoFrame( photo_frame_name );
	});
}

function SavePhotoFrame( name ){
	console.log('name'+name+', pageId'+window.pageId);
	$.ajax({
		type:'POST',
		url:'/SavePhotoFrame/',
		dataType:'json',
		data: { 'name':name,'pageId':window.pageId },
		success: function(){
			if ( JData.error ){
				alert(JData.error);
			}
			else {
				//alert(JData.succsse);
			}
		},
		error: function(){
			alert( "Save Photo Frame error!" );
		}
	})
}

function GetUploadPhotos() {
	return $.ajax({
		type: 'POST',
		url: '/GetUploadPhotos/',
		dataType: 'json',
		data: { "pageId": window.pageId},
		success: function(JData) {
			if ( JData.error ) {
				alert(JData.error);
				return false;
			}
			else {
				window.photos = JData.photos;
				return true;
			}
		},
		error: function() {
			alert("ERROR!!");
			return false;
		}
	})
}

// ============================== zoom gesture @mobile ============================  updated by Paul Ku 2013/11/05
/*function ZoomGestureMobile (){
	$$('#photo_container_'+window.photo_index).pinchingOut(function(){
		if ( window.photos[window.photo_index].zoom <1.5 ){
			var pointer_left = parseInt( $('#zoom_line_pointer').css('left'), 10 )  + 10 ;
			$('#zoom_line_pointer').css( 'left' , pointer_left ); 
			PhotoZoomRatioPresent();
			var zoom = window.photos[ window.photo_index ].zoom;
			var left = ReadPhotoPositionLeft();
			var top = ReadPhotoPositionTop();
			PhotoContainerAdjust( window.photo_index );
			PhotoPositionAdjust( window.photo_index, left, top );
			SavePhotoPosition();
		}
	});

	$$('#photo_container_'+window.photo_index).pinchingIn(function(){
		if ( window.photos[window.photo_index].zoom >0.5 ){
			var pointer_left = parseInt( $('#zoom_line_pointer').css('left'), 10 )  - 10 ;
			$('#zoom_line_pointer').css( 'left' , pointer_left ); 
			PhotoZoomRatioPresent();
			var zoom = window.photos[ window.photo_index ].zoom;
			var left = ReadPhotoPositionLeft();
			var top = ReadPhotoPositionTop();
			PhotoContainerAdjust( window.photo_index );
			PhotoPositionAdjust( window.photo_index, left, top );
			SavePhotoPosition();
		}
	});
		
}*/


function UploadPhotoSetting(callback) {
	
		$.ajax({
		type: "POST",
		url: "/UploadPhotoSetting/",
		dataType: 'json',
		data: { "pageId" : window.pageId, "photos" : window.photos},
		success: function(JData) {
			if (JData.error) {
				alert(JData.error);
			}
			else {
				callback();
			}
		},
		error: function() {
			alert("ERROR!!");

		}
		
	})
	
}

function GetTextContent() {
	return $.ajax({
		type:'POST',
		dataType:'json',
		url:'/GetTextContent/',
		data:{"pageId": window.pageId},
		success: function(JData) {
			if ( JData.error ) {
				alert(JData.error);
				return false;
			}
			else {
				window.text=JData.text;
				return true;
			}
		},
		error: function() {
			alert("ERROR!!");
			return false;
		}
		
	})
}

function UploadText(callback) {
	window.text = $("#text_edit_div").val().replace(/\n/g,"<br>").replace(/ /g,"&nbsp").replace(/"/g,'&#34;').replace(/'/g,"&#39;");
	console.log("pageId: " + window.pageId + ",  Upload: " + '"' + window.text + '"');
	$.ajax({
		type: "POST",
		url: "/UploadText/",
		dataType: 'json',
		data: { "pageId" : window.pageId, "text" : window.text},
		success: function(JData) {
			if (JData.error) {
				alert(JData.error);
			}
			else {
				callback();
			}
		},
		error: function() {
			alert("ERROR!!");
		}
	})
}

// =================================================================================
// ========================= click icon functions ================================
function ClickIconFunction(){
	// var zoom_boolean = false;
	// var click_boolean = false;
	// var text_click_boolean = false;

	$('#edit_button').click(function(){
		//ZoomGestureMobile();
		PhotoDraggable();
		var $img = $('#'+window.photo_index,'#photo_container_'+window.photo_index);
		var zoom = window.photos[window.photo_index].zoom;

		if ( window.photos[window.photo_index].zoom<1.5 ){
			zoom = zoom +0.1;
		}
		else{
			zoom = 1;	
		}
		window.photos[ window.photo_index ].zoom = zoom;
		$img.height( $("#display_area").height()* zoom);
		$img.css( "width" , "auto");
		
		var left = ReadPhotoPositionLeft();
		var top = ReadPhotoPositionTop();
		PhotoContainerAdjust( window.photo_index );
		PhotoPositionAdjust( window.photo_index, left, top );
		SavePhotoPosition();
	});

	/* text preview animation */
	/*var text_boolean = false;
	$('#time_button').click(function(){
		var frame_width = $('#album_window').width();

		if( text_boolean ){
			TweenMax.fromTo([$('#text_window'),$('#frame_area')],2,{x:-frame_width},{x:0,ease:'Power2.easeInOut'});
			text_boolean = false;
			$(this).removeClass('selected');
			$('#edit_button').addClass('selected');
		}
		else{
			$('#edit_button').removeClass('selected');
			$(this).addClass('selected');
			TweenMax.fromTo([$('#text_window'),$('#frame_area')],2,{x:0},{x:-frame_width,ease:'Power2.easeInOut'});
			text_boolean = true;
		}
	});*/

	/* text preview animation */																																																																																																																																																																																																																																																																															

	$('#next_page').click(function(){
		console.log( 'turn page. next_page' );
		if ( window.photo_index < window.photos.length-1 ){
			//turn page index
			window.photo_index = window.photo_index +1;
			CurrentArrowState();
			var left = ReadPhotoPositionLeft();
			var top = ReadPhotoPositionTop();
			PhotoContainerAdjust( window.photo_index );
			PhotoPositionAdjust( window.photo_index, left, top );
			viewModel.currentSettingPhoto(window.photo_index);
			var page_index = window.photo_index+1;
			$('#page_info').html(  page_index+' / '+ window.photos.length  );
		}		
	});
	$('#previous_page').click(function(){
		console.log( 'turn page. previous_page' );
		if ( window.photo_index > 0 ){
			//turn page index
			window.photo_index = window.photo_index -1;
			CurrentArrowState();
			var left = ReadPhotoPositionLeft();
			var top = ReadPhotoPositionTop();
			PhotoContainerAdjust( window.photo_index );
			PhotoPositionAdjust( window.photo_index, left, top );
			viewModel.currentSettingPhoto(window.photo_index);	
			var page_index = window.photo_index+1;
			$('#page_info').html(  page_index+' / '+ window.photos.length  );
		}
	});

}

function SwipeFunctionSetting(){

	CurrentArrowState();

	$('#'+window.photo_index,'#photo_container_'+window.photo_index).on('swipeLeft',function(){
		console.log('swipe left.');	
		if ( window.photo_index < window.photos.length-1 ){
			AnimationNameClickClear();
			//turn page index
			window.photo_index ++;
			viewModel.currentSettingPhoto(window.photo_index);	
			var page_index = window.photo_index+1;
			$('#page_info').html(  page_index+' / '+ window.photos.length  );
		}	
	});
	$('#'+window.photo_index,'#photo_container_'+window.photo_index).on('swipeRight',function(){
		console.log('swipe right.');
		if ( window.photo_index > 0 ){
			AnimationNameClickClear();
			//turn page index
			window.photo_index --;
			viewModel.currentSettingPhoto(window.photo_index);	
			var page_index = window.photo_index+1;
			$('#page_info').html(  page_index+' / '+ window.photos.length  );
		}		
	});
}

function CurrentArrowState(){
	if(window.photos.length>1){
		if(window.photo_index==0){
			$('#next_page').css('display','block');
			$('#previous_page').css('display','none');
		}
		else if( window.photo_index == (window.photos.length - 1) ){
			$('#next_page').css('display','none');
			$('#previous_page').css('display','block');
		}
		else{
			$('#next_page').css('display','block');
			$('#previous_page').css('display','block');
		}		
	}
	else{
		$('#next_page').css('display','none');
		$('#previous_page').css('display','none');
	}

}


/*==============  text related function ====================*/
/*function TextAppend(){
	$('{% block content %}<textarea type="text" id="edit_text"></textarea>{% endblock %}').appendTo($('#text_window'));
	$('<div id="text_preview_div"></div>').appendTo($('#text_window'));
}
function textEdit() {
	$("#text_preview_div").css("display","none");
	$("#edit_text").css("display", "block");
	TweenMax.to( $("#text_edit_div"), 0.5, {autoAlpha:1} );
}
function textPreview() {
	var text = $("#edit_text").val().replace(/\n/g,"<br>").replace(/ /g,"&nbsp");
	$("#text_preview_div").html(text);
	$("#edit_text").css("display","none");
	$("#text_preview_div").css("display", "block");
	TweenMax.to( $("#text_preview_div"), 1, {autoAlpha:0.6});
	
	var display_height = $("#display_area").height();
	var text_div_height = $("#text_preview_div").height();


	var second = text_div_height/25 + 1;
	var start_position = display_height;
	var end_position = -text_div_height;	

	TweenMax.fromTo($("#text_preview_div"), second, {y:start_position}, {y:end_position , ease: 'Linear.easeNone', onComplete:textEdit} );
}
*/

function UploadText(callback) {
	window.text = $("#edit_text").val().replace(/\n/g,"<br>").replace(/ /g,"&nbsp").replace(/"/g,'&#34;').replace(/'/g,"&#39;");
	console.log("pageId: " + window.pageId + ",  Upload: " + '"' + window.text + '"');
	$.ajax({
		type: "POST",
		url: "/UploadText/",
		dataType: 'json',
		data: { "pageId" : window.pageId, "text" : window.text},
		success: function(JData) {
			if (JData.error) {
				alert(JData.error);
			}
			else {
				callback();
			}
		},
		error: function() {
			alert("ERROR!!");
		}
	})
}
/*==============  text related function ====================*/
/*
function EditZoomFunction(){

	$('#zoom_out').click(function(){
		// console.log('zoom click.');
		var $img = $('#'+window.photo_index,'#photo_container_'+window.photo_index);
		var zoom = window.photos[window.photo_index].zoom;
		
		if( window.photos[window.photo_index].zoom>0.5 ){
			zoom = zoom - 0.1;
			window.photos[ window.photo_index ].zoom = zoom;
			$img.height( $("#display_area").height()* zoom);
			$img.css( "width" , "auto");
			
			var left = ReadPhotoPositionLeft();
			var top = ReadPhotoPositionTop();
			PhotoContainerAdjust( window.photo_index );
			PhotoPositionAdjust( window.photo_index, left, top );
			SavePhotoPosition();
			console.log("zoom in: ->>>>>" + window.photos[ window.photo_index ].zoom );
		}

	});

	$('#zoom_in').click(function(){

		var $img = $('#'+window.photo_index,'#photo_container_'+window.photo_index);
		var zoom = window.photos[window.photo_index].zoom;

		if ( window.photos[window.photo_index].zoom<1.5 ){
			zoom = zoom +0.1;
			window.photos[ window.photo_index ].zoom = zoom;
			$img.height( $("#display_area").height()* zoom);
			$img.css( "width" , "auto");
			
			var left = ReadPhotoPositionLeft();
			var top = ReadPhotoPositionTop();
			PhotoContainerAdjust( window.photo_index );
			PhotoPositionAdjust( window.photo_index, left, top );
			SavePhotoPosition();
			console.log("zoom in: ->>>>>" + window.photos[ window.photo_index ].zoom );

		}
	});
}
*/
/*function DisableZoom(){
	$('#zoom_out').unbind('click');
	$('#zoom_in').unbind('click');
}*/
function PhotoPositionDraggable(){
	$('img','#photo_container_'+window.photo_index).on('mousedown',function(){
		PhotoDraggable();
	});
}

/*
function AnimationClickeSetting() {
	$('#fast_display').click(function(){
		SaveDisplaySeconds( 3 );
		SelectedDisplayTime($(this));
	});
	$('#middle_display').click(function(){
		SaveDisplaySeconds( 5 );
		SelectedDisplayTime($(this));
	});
	$('#slow_display').click(function(){
		SaveDisplaySeconds( 8 );
		SelectedDisplayTime($(this));
	});

	function SelectedDisplayTime( object ) {

		if( object.attr('id').toString() == 'fast_display' ){
			$('#fast_display').css('background-image',"url('/static/images/display_selected-fast.png')");
			$('#middle_display').css('background-image',"url('/static/images/display_setting-middle.png')");
			$('#slow_display').css('background-image',"url('/static/images/display_setting-slow.png')");
			console.log('fast');
		}
		else if( object.attr('id').toString() == 'middle_display' ){
			$('#fast_display').css('background-image',"url('/static/images/display_setting-fast.png')");
			$('#middle_display').css('background-image',"url('/static/images/display_selected-middle.png')");
			$('#slow_display').css('background-image',"url('/static/images/display_setting-slow.png')");
			console.log('middle');
		}
		else if( object.attr('id').toString() == 'slow_display' ){
			$('#fast_display').css('background-image',"url('/static/images/display_setting-fast.png')");
			$('#middle_display').css('background-image',"url('/static/images/display_setting-middle.png')");
			$('#slow_display').css('background-image',"url('/static/images/display_selected-slow.png')");
			console.log('slow');
		}
		else 
			cosole.log( 'object id type error. :' );
	}
	function SelectedAnimateTime( object ) {
		for(var i=0;i<3;i++){
			var animate = $('.animate'); 
			if( animate[i].id == object.attr('id') )
				$('#'+animate[i].id).addClass('selected');
			else
				$('#'+animate[i].id).removeClass('selected');
			
		}
	}
}

function AnimationTimeSettingPresent( index ){
	var display_second = ReadDisplaySeconds(index);
	if ( display_second == 3){
		SelectedDisplayTime( $('#fast_display') );
	}
	else if( display_second == 5 ){
		SelectedDisplayTime( $('#middle_display') );
	}
	else{
		SelectedDisplayTime( $('#slow_display') );
	}
	
	function SelectedDisplayTime( object ) {
		if( object.attr('id').toString() == 'fast_display' ){
			$('#fast_display').css('background-image',"url('/static/images/display_selected-fast.png')");
			$('#middle_display').css('background-image',"url('/static/images/display_setting-middle.png')");
			$('#slow_display').css('background-image',"url('/static/images/display_setting-slow.png')");
		}
		else if( object.attr('id').toString() == 'middle_display' ){
			$('#fast_display').css('background-image',"url('/static/images/display_setting-fast.png')");
			$('#middle_display').css('background-image',"url('/static/images/display_selected-middle.png')");
			$('#slow_display').css('background-image',"url('/static/images/display_setting-slow.png')");
		}
		else if( object.attr('id').toString() == 'slow_display' ){
			$('#fast_display').css('background-image',"url('/static/images/display_setting-fast.png')");
			$('#middle_display').css('background-image',"url('/static/images/display_setting-middle.png')");
			$('#slow_display').css('background-image',"url('/static/images/display_selected-slow.png')");
		}
		else 
			cosole.log( 'object id type error.' );
	}
}
*/

// =================================================================================
// =========================== photo append functions ==============================
function PhotoContainerAppend( index ) {
	var photo_container = $('<div id="photo_container_' + index + '" data-bind="css:{ noneDisplay: currentSettingPhoto() != ' + index + '}"></div>').css('z-index',0);
	$('#page_info').html( ( window.photo_index + 1 ).toString() + ' / ' + window.photos.length.toString() );
	
	var photo_obj = new Image();
	photo_obj.src = window.photos[index].url;
	$( photo_obj ).attr('id',index).css({'left':'0px','top':'0px','position':'relative','z-index':1,'margin-top':5});
	photo_container.append(photo_obj);
	// $('#text_preview_div').before(photo_container);
	$(photo_container).appendTo($('#display_area'));
	
	$(photo_obj).load( function() {
		var photo_id = $(this).attr('id');
		$(this).css("height",$("#display_area").height()*window.photos[parseInt(photo_id)].zoom);
		$(this).css("width","auto");
		PhotoContainerAdjust( photo_id );
		var ratio = window.photos[ parseInt( photo_id ) ].zoom;
		var left = window.photos[ parseInt( photo_id ) ].positionLeft;
		var top = window.photos[ parseInt( photo_id ) ].positionTop;
		PhotoPositionAdjust( parseInt( photo_id ), left, top );
		ko.applyBindings(viewModel);
	});
}

function PhotoContainerAdjust( index ){
	
	var zoom = window.photos[ index ].zoom ;
	var $container = $('#photo_container_'+index); 
	var $img = $('#'+index, $container);
	var container_width = $img.width()*2-$("#display_area").width();
	var container_height = $img.height()*2-$("#display_area").height();
	
	var target_width = $("#display_area").width();
	var target_height = $("#display_area").height();

	if ( container_width <= target_width ) {
		$container.css( "width", target_width );
		$container.css('margin-left', 0 );
	}
	else {
		$container.css( "width", container_width );
		$container.css('margin-left', -( container_width - target_width ) / 2 );
	}

	if ( container_height <= target_height ) {
		$container.css( "height", target_height );
		$container.css('margin-top', 0 );
	}
	else {
		$container.css( "height", container_height );
		$container.css('margin-top', -( container_height - target_height ) / 2 );
	}
}

function PhotoPositionAdjust( index, left, top ){
	var ratio = window.photos[ index ].zoom;
	var img_width = $('#'+index,'#photo_container_'+index).width();
	var img_height = $('#'+index,'#photo_container_'+index).height();
	var div_width = $('#photo_container_'+index).width();
	var div_height = $('#photo_container_'+index).height();
	var photo_left = ( div_width - img_width )/2 + left*ratio*window.display_rate ;
	var photo_top = ( div_height - img_height)/2 + top*ratio*window.display_rate ;
	
	if ( img_width <= $("#display_area").width() ) {
		if ( photo_left < 0)
			photo_left = 0;
		else if ( photo_left > $("#display_area").width()-img_width )
			photo_left = $("#display_area").width()-img_width;
	}

	if ( img_height <= $("#display_area").height() ) {
		if ( photo_top < 0 )
			photo_top = 0;
		else if ( photo_top > $("#display_area").height()-img_height )
			photo_top = $("#display_area").height()-img_height;
	}
		
	$('#'+index,'#photo_container_'+index).css({'left':photo_left,'top':photo_top});
	
}


// =================================================================================
// ========================= Zoom setting functions ================================
function ReadPhotoZoomRatio(){
	return window.photos[window.photo_index].zoom;
}
function ZoomRatioSetting(){
	var zoom = ReadPhotoZoomRatio();
	$('#zoom_line_pointer').css('left', (( zoom - 1)*100 +55) );  
	$('#zoom_ratio_display').html( parseInt(zoom * 100) + '%' );  
}
function SavePhotoZoomRatio( scale ){
	window.photos[ window.photo_index ].zoom = scale; 
}
function PhotoZoomRatioPresent() {
	var left = SavePhotoZoomRatio();
	var $img = $('#'+window.photo_index,'#photo_container_'+window.photo_index);
	$img.height( $("#display_area").height()* window.photos[window.photo_index].zoom );
	$img.css( "width" , "auto");
}
function ZoomRatioReset() {
	SavePhotoZoomRatio();
	$('#zoom_ratio_display').html( '100%' );
	$('#zoom_line_pointer').css('left', 55);
}


// =================================================================================
// =========================== position setting functions ==========================
/*function PhotoMoveVertical(top) {
	
	var photo_top =  parseFloat( $("#" + window.photo_index).css("top") ) + top;
	var div_height = $("#photo_container_" + window.photo_index).height();
	var img_height = $("#"+window.photo_index, $("#photo_container_" + window.photo_index)).height();
	
	if ( img_height <= $("#center_panel").height() ) {
		if ( photo_top < 0 )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",0) ;
		else if ( photo_top > $("#center_panel").height()-img_height )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",$("#center_panel").height()-img_height) ;
		else
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",photo_top) ;
	}
	else {
		if ( photo_top < 0 )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",0) ;
		else if ( photo_top > div_height - img_height )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",div_height - img_height) ;
		else
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("top",photo_top) ;
	}
}
function PhotoMoveHorizontal(left) {
	
	var photo_left =  parseFloat( $("#" + window.photo_index).css("left") ) + left;
	var div_width = $("#photo_container_" + window.photo_index).width();
	var img_width = $("#"+window.photo_index, $("#photo_container_" + window.photo_index)).width();
	
	if ( img_width <= $("#center_panel").width() ) {
		if ( photo_left < 0 )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left", 0) ;
		else if ( photo_left > $("#center_panel").width()-img_width )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left", $("#center_panel").width()-img_width) ;
		else
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left",photo_left) ;
	}
	else {
		if ( photo_left < 0 )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left",0) ;
		else if ( photo_left > div_width - img_width )
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left",div_width - img_width) ;
		else
			$("#"+window.photo_index, $("#photo_container_" + window.photo_index)).css("left",photo_left) ;
	}
}
*/
function ReadPhotoPositionLeft() {
	return window.photos[window.photo_index].positionLeft;
}
function ReadPhotoPositionTop() {
	return window.photos[window.photo_index].positionTop;
}
function SavePhotoPosition()  {
	var index = window.photo_index;
	var ratio = window.photos[ index ].zoom;
	var left = parseFloat( $('#'+index, $('#photo_container_' + index)).css('left'));
	var top = parseFloat( $('#'+index, $('#photo_container_' + index)).css('top'));
	var img_width = $('#'+index,'#photo_container_'+index).width();
	var img_height = $('#'+index,'#photo_container_'+index).height();
	var div_width = $('#photo_container_'+index).width();
	var div_height = $('#photo_container_'+index).height();
	window.photos[index].positionLeft = ( left - (div_width - img_width)/2 ) / ratio / window.display_rate ; 
	window.photos[index].positionTop = ( top - (div_height - img_height)/2 ) / ratio / window.display_rate ;
	// console.log('==============='+index+'=========================');
	// console.log("ratio: " + ratio + ", img_left: " + left + ", img_width: " + img_width + ", container_width: " + div_width + ", positionLeft: " + window.photos[index].positionLeft);
}


function PhotoDraggable (){
	for (i=0;i<window.photos.length;i++){
		$('#'+i,'#photo_container_'+i ).draggable({
			containment: '#photo_container_'+i,
			stop: function() {
				SavePhotoPosition();
			}
		});
	}
	PhotoEnableDraggable();
}
function PhotoEnableDraggable(){
	for (i = 0;i<window.photos.length;i++){
		$('#'+ i,'#photo_container_'+i ).draggable( 'enable' );
	}
}
function PhotoDisableDraggable(){
	for (i = 0;i<window.photos.length;i++){
		$('#'+ i,'#photo_container_'+i ).draggable( 'disable' ).css('opacity',1);
	}
}


// ======================================================================================
// ============================== in-animation setting functions ========================
/*function ReadStartAnimateSecond( index ) {
	var second = parseFloat( window.photos[ index ].startAnimateSeconds );
	// console.log('@read: seconds='+second);
	return second;
}
function ReadStartAnimateName( index ) {
	var name = window.photos[index].startAnimate;
	return name;
}
function SaveStartAnimateSecond( second ) {
	window.photos[window.photo_index].startAnimateSeconds = second ;
}
function SaveStartAnimateName( name ) { 
	window.photos[window.photo_index].startAnimate = name ;
}
*/
	/*======================================================= update by Paul Ku 2013/10/26  */  
/*function InCancelRestAll( obj ){
	for (var i = 0;i<window.in_animation_name.length;i++){
		$('input[name ='+window.in_animation_name[ i ]+']').prop('checked',false);
	}
	$('input[name=Random_in]').prop('checked',false);
	obj.prop('checked',true);
	return obj.prop('name');
}
function InAnimationReset( index ){
	var second = ReadStartAnimateSecond( index );
	var name = ReadStartAnimateName( index );
	InCancelRestAll( $('input[name='+name+']') );
	$('#in-animation_seconds').html(second);
	// console.log('@in animation works. name: '+name);
}*/
	/*======================================================= update by Paul Ku 2013/10/26  */  


// ==========================================================================================
// ============================== display time setting function =============================
/*function ReadDisplaySeconds( index ){
	var display_sec = window.photos[ index ].displaySeconds;
	return display_sec;
}
function SaveDisplaySeconds( sec ){
	window.photos[ window.photo_index ].displaySeconds = sec;
}
function DisplaySecondsReset ( index ){
	var display_sec =  ReadDisplaySeconds( index );
	$('#display-time_seconds').html(display_sec);
}*/
/*======================================================= update by Paul Ku 2013/10/26  */ 

// ==========================================================================================
// ============================== out-animation setting function ============================
/*function ReadEndAnimateSecond( index ) {
	var second = parseFloat( window.photos[ index ].endAnimateSeconds );
	// console.log('@read: seconds='+second);
	return second;
}
function ReadEndAnimateName( index ) {
	var name = window.photos[index].endAnimate;
	return name;
}
function SaveEndAnimateSecond( second ) {
	window.photos[window.photo_index].endAnimateSeconds = second ;
}
function SaveEndAnimateName( name ) { 
	window.photos[window.photo_index].endAnimate = name ;
}
function OutCancelRestAll( obj ){
	for (var i = 0;i<window.out_animation_name.length;i++){
		$('input[name ='+window.out_animation_name[ i ]+']').prop('checked',false);
	}
	$('input[name=Random_out]').prop('checked',false);
	obj.prop('checked',true);
	return obj.prop('name');
}
function OutAnimationReset( index ){
	var second = ReadEndAnimateSecond( index );
	var name = ReadEndAnimateName( index );
	OutCancelRestAll( $('input[name='+name+']') );
	$('#out-animation_seconds').html(second);
	// console.log('@out animation works. name: '+name);
}*/
// ========================================================================================
// ====================================QRcode export=======================================
// ========================================================================================
function CompleteClick(){
	$('#complete_photo_setting').click(function(){
		UploadPhotoSetting(function(){

		})
	});
}


function UploadPhotoSetting(callback) {
	$.ajax({
		type: "POST",
		url: "/UploadPhotoSetting/",
		dataType: 'json',
		data: { "pageId" : window.pageId, "photos" : window.photos},
		success: function(JData) {
			if (JData.error) {
				alert(JData.error);
			}
			else {
			}
		},
		error: function() {
			alert("ERROR!!");

		}
		
	})
	
}

/*function FinishPanelDisplay() {
	
	$.ajax({
		type: 'POST',
		url: '/GenerateQR/',
		dataType: 'json',
		data: {"pageId": window.pageId},
		success: function(JData) {
			if ( JData.error ) {
				alert("ERROR!!");
			}
			else {
				$('<div id="qrDescribe"  style="opacity:0; height:50px; color:#ffffbd; text-align:center; line-height:50px;">QRCode for your page:</div>')
					.width($('#top').width())
					.css('margin-top',$('#top').height()-50)
					.appendTo($("#top"));
				TweenMax.to( $('#top'),1,{autoAlpha:1,ease:'Power2.easeInOut'} );
				TweenMax.to($("#qrDescribe"), 1, {autoAlpha:1});
			
				var qrImg = new Image();
				qrImg.src = JData.qrImgPath;
				$(qrImg).attr('id','qrImg').attr('onclick','window.open("' + JData.path + '", "_blank");')
					    .css("opacity",0).css("cursor","pointer").appendTo($("#display_area"));
				$(qrImg).load(function(){
					$(this).width(150).height(150)
						   .css("margin-top",$("#display_area").height()/2-75)
						   .css("margin-left",$("#display_area").width()/2-75);
					TweenMax.to( $(this), 1, {autoAlpha: 1});
					
				})
			}
		},
		error: function() {
			alert("ERROR!!");
		}
	})
	
}*/



// ========================================================================================
// =================================== CSRF Setting =======================================
// ========================================================================================

$( document ).ready(function() {

	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				// Does this cookie string begin with the name we want?
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	function csrfSafeMethod(method) {
		// these HTTP methods do not require CSRF protection
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
  
	function sameOrigin(url) {
		// test that a given url is a same-origin URL
		// url could be relative or scheme relative or absolute
		var host = document.location.host; // host + port
		var protocol = document.location.protocol;
		var sr_origin = '//' + host;
		var origin = protocol + sr_origin;
		// Allow absolute or scheme relative URLs to same origin
		return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
			(url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
			// or any other URL that isn't scheme relative or absolute i.e relative.
			!(/^(\/\/|http:|https:).*/.test(url));
	}
	  
	$.ajaxSetup({
		crossDomain: false, // obviates need for sameOrigin test
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type)) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		}
	});
});