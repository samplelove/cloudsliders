$(window).load(function() {
	$(document).bind('touchmove',function(e) {
		e.preventDefault();
	});
	window.page_index = 0;
	
	
	for(var i = 0;i<window.photos.length;i++){
		PhotoContainerAppend( $("#photos_display") ,i );
	}
	//TextDisplaySetting();
	LayoutSetting();
	ButtonContainerSetting();
	ButtonAnimationSetting();
	

	$('body').css('margin',0);
	$('body').css('overflow','hidden');

	$("#title").html(window.page_title);
	$("#text_diary").html(window.text);

	var title = window.page_editDate.split('T',1);
	$("#page_time").html(title);
	if(window.photos.length==0){
		$("#loading_describe").html('');
	}
	
})

function LayoutSetting() {
	window.display_height = $(window).height();
	window.display_width = window.display_height/317*212;
	if ( window.display_width > $(window).width() ) {
		window.display_width = $(window).width();
		window.display_height = window.display_width/212*317;
	}

	window.display_rate=window.display_height/317;
	
	var $display = $("#display");
	var $loading_page = $("#loading_page");
	var $loading_img = $("#loading_img");
	var $loading_describe = $("#loading_describe");
	var $photo_display = $("#photos_display");
	var $text_window = $("#text_window");
	var $text_display = $("#text_display");
	var $title = $("#title");
	var $page_time = $("#page_time");
	var $text_diary = $("#text_diary");

	$display.width(window.display_width);
	$display.height(window.display_height);
	$display.css("margin-top", ($(window).height() - window.display_height)/2);

	$loading_page.width(window.display_width);
	$loading_page.height(window.display_height);
	
	$loading_img.width(window.display_width);
	$loading_img.height(window.display_height);
	
	$loading_describe.width(window.display_width);
	$loading_describe.height(window.display_height/12);
	$loading_describe.css("line-height",window.display_height/12+"px");
	$loading_describe.css("margin-top",window.display_height - $loading_describe.height() );

	$page_time.height(window.display_height/24);
	$page_time.css("font-size",window.display_height/24+"px");
	$page_time.css("margin-left",window.display_width*0.1).css("margin-top",window.display_height*0.1);
	$page_time.css("line-height",window.display_height/24+"px");

	$title.height(window.display_height/24);
	$title.css("font-size",window.display_height/24+"px");
	$title.css("margin-left",window.display_width*0.1).css("margin-top",window.display_height*0.1+$page_time.height()+10);
	$title.css("line-height",window.display_height/24+"px");

	$text_diary.height(window.display_height*2/3).width(window.display_width*0.8);
	$text_diary.css("font-size",window.display_height/24+"px");
	$text_diary.css("margin-left",window.display_width*0.1).css("margin-top",window.display_height*0.1+$page_time.height()+$title.height()+20);
	$text_diary.css("line-height",window.display_height/24+"px");

	$photo_display.height(window.display_height - $loading_describe.height() );
	$photo_display.width( $photo_display.height()*212/317 );
	$photo_display.css("margin-top",$loading_describe.height()).css("margin-left",($display.width()-$photo_display.width())/2);
	
	
	$text_window.width(window.display_width*0.8).height(window.display_height*0.8);
	$text_window.css('margin-left',window.display_width*0.1).css('margin-top',window.display_height*0.1)
	
	$text_display.width( window.display_width*0.8 );
	$text_display.css("font-size", 20*window.display_rate*0.6 + "px");
	$text_display.css("line-height", 20*window.display_rate*0.6 + "px");
	if( $text_display.height() < $text_window.height() ){
		$text_display.css("margin-top",($text_window.height()-$text_display.height())/2);
	}
	else{
		$text_display.css("margin-top",0);
	}
		

	

}

// function TextDisplaySetting() {
// 	window.text_marqueeing = false;
// 	$("#text_display").html(window.text);
// }

// function TextMarquee() {
// 	var $text_display = $("#text_display");
// 	var $text_window = $("#text_window");
// 	var $photo_describe = $(".photo_describe");

// 	var second = 0.5
// 	var start_position = 0;

// 	window.text_marqueeing = true;
	
// 	$photo_describe.css("opacity",0);
// 	TweenMax.fromTo( $text_display, second, {autoAlpha:0}, {autoAlpha:0.6, ease: 'Linear.easeNone', onComplete: UnMarqueeing} );

// 	function UnMarqueeing() {
// 		var $text_display = $('#text_display');
// 		var $text_window = $('#text_window');
		
// 		if( $text_display.height() <= $text_window.height() ) //文字內容的高度比動畫外框的高度還小或是相等
// 			;
// 		else{ //文字內容的高度比動畫外框的高度還高
// 			$text_display.draggable({
// 				cursor:"move",
// 				//cursorAt:{ top: -$text_display.height()/4 },
// 				axis:"y",
// 				drag: function( event, ui ) {
// 					if(  ui.position.top < $text_window.height()-$text_display.height() ){
// 						ui.position.top = $text_window.height()-$text_display.height();
						
// 					}
// 					else if( ui.position.top > 0 ){
// 						ui.position.top =0;
// 					}
// 					else if( ui.position.top<0 ){
						
// 					}
// 					else
// 						;
// 				}
// 			});

// 			$text_display.draggable('enable');
// 			// var y_end = -$text_window.height()-$text_preview_div.height();
			
// 			// $text_display.on("doubleTap dbclick",function(){
// 			// 	$text_display.css('top',0);
// 			// 	TweenMax.to( $text_preview_div, 2, {y:y_end,ease:'Power2.easeInOut',onComplete:ResetBoolean} );
// 			// });
// 		}				

// 		function ResetBoolean(){
// 			window.text_marqueeing = false;	
// 		}
		
// 	}
	
// }

function AnimateProcess() {

	window.display_timeline = new TimelineMax({repeat:-1,paused:true});
	for(var i = 0;i<window.photos.length;i++){
		var start_name = ReadStartAnimateName( i );
		var start_sec = ReadStartAnimateSeconds( i );
		var display_time = ReadDislplaySeconds( i );
		var end_name = ReadEndAnimateName( i );
		var end_sec = ReadEndAnimateSeconds( i );

		$("#photo_describe_0").css("display","block");
		
		window.display_timeline
			.call( PageIndex,[ i ] )
			.call( DisplayContainer, [$('#photo_container_'+i)] );
		ClidersAnimate( $('#photos_display'), $('#photo_container_'+i) , window.display_timeline , start_name, start_sec, -window.display_width , 0, -window.display_height, 0, 1, 1);
		window.display_timeline
			.addLabel(i)
			.add(TweenMax.to($('#photo_container'+i),0,{delay:display_time}));
		ClidersAnimate( $('#photos_display'), $('#photo_container_'+i) , window.display_timeline , end_name, end_sec, 0, window.display_width, 0,window.display_height, 1, 1);
		window.display_timeline.call( UndisplayContainer, [$('#photo_container_'+i)] );
	}
	//ClidersAnimate( target,obj, timeline , Name, sec, sX, eX, sY, eY, sScale, eScale )
	if( window.photos.length == window.photo_processed ){
		PageReady();
	}
}

function PageReady() {	
	var $loading_describe = $("#loading_describe");
	var $loading_page = $("#loading_page");
	var	up_distance = $loading_page.height()-$loading_describe.height();
	$loading_describe.html("相片動畫");
	$("#photo_container_0").css({"display":"block","visibility":"visible","opacity":1});		
	var count = 0;
	$loading_describe.click(function(e){
		e.preventDefault();
		if (count == 0){
			$("#photos_display").css("display","block");
			$("#text_display").css("display","block");
			$("#button_container").css("display","block");
			window.display_timeline.play();
			TweenMax.to($loading_page,2,{y:-up_distance,ease:'Power2.easeNone',onComplete:LoadPageComplete});

			function LoadPageComplete(){
				$loading_describe.html('日誌本文');
				count++;
			}
		}
		else if(count>0 && count%2==1){
			TweenMax.to($loading_page,2,{y:0,ease:'Power2.easeNone',onComplete:DiaryCountPlus});
		}
		else {
			TweenMax.to($loading_page,2,{y:-up_distance,ease:'Power2.easeNone',onComplete:PhotoCountPlus});
		}
		function DiaryCountPlus(){
			count++;
			$loading_describe.html('相片動畫');
		}
		function PhotoCountPlus(){
			count++;
			$loading_describe.html('日誌本文');
		}
	});



}

function PageIndex( index ){
	window.page_index = index; 
	for(var i=0;i<window.photos.length;i++){
		var $photo_describe = $("#photo_describe_"+i);
		if( i == index )
			$photo_describe.css("display","block");
		else
			$photo_describe.css("display","none");
			

	}
}

function DisplayContainer(obj) {
	$(obj).css({"display":"block","visibility":"visible","opacity":0});
	TweenMax.set($(obj),{rotation:0,rotationY:0});
}

function UndisplayContainer(obj) {
	//TweenMax.set($(obj),{autoAlpha:0});
	$(obj).css({"display":"none","visibility":"hidden","opacity":0});
	TweenMax.set($(obj),{rotation:0,rotationY:0});
}

function PhotoContainerAppend( target, index ) {
	var $photos_display = $("#photos_display");
	var photo_container = $('<div id="photo_container_' + index + '"></div>');
	var photo_describe = $('<div id="photo_describe_'+index+'" class="photo_describe panel panel-default" style="max-width:'+($photos_display.width()*0.8)+'px; word-wrap: break-word; word-break: normal; z-index:999; border-color:transparent; display:none;"> \
		<div id="photo_' + index + '_describe" style="max-width:'+($photos_display.width()*0.8)+'px; class="panel-body"></div></div>');
	
	
	var photo_obj = new Image();
	var $photo_panel = $("#")
	

	photo_obj.src = window.photos[index].url;
	$( photo_obj ).attr('id',index).css({'left':'0px','top':'0px','position':'relative'});
	
	
	photo_container.append(photo_obj).appendTo($(target));
	
	if( window.photos[index].describe ){
		var text = ReplaceInputStrToText(window.photos[index].describe);
		
		//photo describe setting
		photo_describe.appendTo($photos_display);
		$('#photo_' + index + '_describe').html(text);
		photo_describe.width( photo_describe.width()+3 );
		for(var i=0;i<window.photos.length;i++){
			var $photo_describe = $("#photo_describe_"+index);
			$photo_describe.css("top","").css("left","");
			$photo_describe.css("top",$photos_display.height()-$photo_describe.height()-10)
						   .css("left",$photos_display.width()-$photo_describe.width());	
		}
	}
	else
		;
	
	
	
	$(photo_obj).load( function() {
		var photo_id = $(this).attr('id');
		$(this).css("height",$(target).height()*window.photos[parseInt(photo_id)].zoom);
		$(this).css("width","auto");
		PhotoContainerAdjust( target, photo_id );
		var ratio = window.photos[ parseInt( photo_id ) ].zoom;
		var left = window.photos[ parseInt( photo_id ) ].positionLeft;
		var top = window.photos[ parseInt( photo_id ) ].positionTop;
		PhotoPositionAdjust( target, parseInt( photo_id ), left, top );
	})
	
}

function ReplaceInputStrToText(uStr) {
	return uStr.replace(/<br>/g,"\n").replace(/&nbsp/g," ").replace(/&#34;/g,'"').replace(/&#39;/g,"'");
}

function PhotoContainerAdjust( target, index ) {
	target_width = $(target).width();
	target_height = $(target).height();

	var zoom = window.photos[ index ].zoom ;
	var $container = $('#photo_container_'+index); 
	var $img = $('#'+index, $container);
	var container_width = target_width;
	var container_height = target_height;
	
	if ( container_width <= target_width ) {
		$container.css( "width", target_width );
		$container.css('margin-left', 0 );
	}
	else {
		$container.css( "width", container_width );
		$container.css('margin-left', -( container_width - target_width ) / 2 );
	}	
	if ( container_height <= target_height ) {
		$container.css( "height", target_height );
		$container.css('margin-top', 0 );
	}
	else {
		$container.css( "height", container_height );
		$container.css('margin-top', -( container_height - target_height ) / 2 );
	}

}

window.photo_processed = 0;

function PhotoPositionAdjust( target, index, left, top ){
	var ratio = window.photos[ index ].zoom;
	var img_width = $('#'+index,'#photo_container_'+index).width();
	var img_height = $('#'+index,'#photo_container_'+index).height();
	var div_width = $('#photo_container_'+index).width();
	var div_height = $('#photo_container_'+index).height();
	var photo_left = ( div_width - img_width )/2 + left*ratio*window.display_rate ;
	var photo_top = ( div_height - img_height)/2 + top*ratio*window.display_rate ;
	/*
	if ( img_width <= $(target).width() ) {
		if ( photo_left < 0)
			photo_left = 0;
		else if ( photo_left > $(target).width()-img_width )
			photo_left = $(target).width()-img_width;
	}

	if ( img_height <= $(target).height() ) {
		if ( photo_top < 0 )
			photo_top = 0;
		else if ( photo_top > $(target).height()-img_height )
			photo_top = $(target).height()-img_height;
	}
	*/
	$('#'+index,'#photo_container_'+index).css({'left':photo_left,'top':photo_top});
	$("#photo_container_"+index).css("display","none").css("overflow","hidden");
	
	window.photo_processed++;
	
	if ( window.photo_processed == window.photos.length )
		AnimateProcess();
	
}



function ReadStartAnimateName( index ){
	var name = (window.photos[index].startAnimate).toString();
	return name;
}
function ReadStartAnimateSeconds( index ){
	var sec = (window.photos[index].startAnimateSeconds).toString();
	return sec;
}
function ReadDislplaySeconds( index ){
	var sec = parseFloat(window.photos[index].displaySeconds);
	return sec;
}
function ReadEndAnimateName (index){
	var name = (window.photos[index].endAnimate).toString();
	return name;
}
function ReadEndAnimateSeconds (index){
	var sec = (window.photos[index].endAnimateSeconds).toString();
	return sec;
}

function ButtonContainerSetting(){
	var button_container_width = window.display_width;
	var button_container_height = window.display_width/10;
	var width_base = button_container_width/3;
	var height_base = button_container_height;
	
	var $button_container = $('#button_container');
	
	$button_container.width( button_container_width );
	$button_container.height( button_container_height ).css("margin-top", -1*button_container_height);
	$button_container.css("background-color","transparent").css("border-color","transparent");

	var left = (button_container_width - width_base)/2;
	var top = button_container_height - height_base;



	$('#animate_play_button').width( width_base ).height( height_base )
	 						 .css("font-size",height_base*2/3).css("left", left).css('top',top);
	
	$('#pause_button').width( width_base ).height( height_base )
	 						 .css("font-size",height_base*2/3).css("left", left).css('top',top);

	left = left - width_base;
	$('#previous_button').width( width_base ).height( height_base )
						 .css("font-size",height_base*2/3).css("left", left ).css("top", top);
						 
	left = width_base + $("#animate_play_button").width();
	$('#next_button').width( width_base ).height( height_base )
					 .css("font-size",height_base*2/3).css("left", left).css("top", top);

	// var left = (button_container_width - width_base*103)/2;
	// var top = button_container_height/2 - height_base*55/1.8;
	// $('#animate_play_button').width( width_base*103 ).height( height_base*55 )
	// 						 .css("left", left)
	// 						 .css("top",  top);
							 
	// $('#pause_button').width( width_base*104 ).height( height_base*56 )
	// 				  .css("left", left)
	// 				  .css("top", top );
					  
	// left = left - width_base*76 - width_base*10;
	// top = button_container_height/2 - height_base*55/2.5;
	// $('#previous_button').width( width_base*76 ).height( height_base*50 )
	// 					 .css("left", left )
	// 					 .css("top", top);
						 
	// left = button_container_width/2 + $('#animate_play_button').width()/2 + width_base*10;
	// top = button_container_height/2 - height_base*55/2.5;
	// $('#next_button').width( width_base*74 ).height( height_base*48 )
	// 				 .css("left", left)
	// 				 .css("top", top);
	

}
function ButtonAnimationSetting(){
  	var button_timeline = new TimelineMax({paused:false,reversed:true,onStart:RepositionDescribe});
	var text_boolean = false;
	var button_height = $('#button_container').height();
	button_timeline
		.add(TweenMax.fromTo($('#button_container'),1,{y:0,autoAlpha:1},{y:button_height,autoAlpha:0.5,ease:'Linear.easeNone'}),'+=5')
		.call( InitialDescribe );
	
	function RepositionDescribe(){
		var $photos_display = $("#photos_display");
		for(var i=0;i<window.photos.length;i++){
			var $photo_describe = $("#photo_describe_"+i);
			$photo_describe.css("top",$("#loading_describe").height()+10).css("left",25);
		}
		
	}

	function InitialDescribe(){
		
		var $photos_display = $("#photos_display");
		for(var i=0;i<window.photos.length;i++){
			var $photo_describe = $("#photo_describe_"+i);
			$photo_describe.css("top","").css("left","");
			$photo_describe.css("top",$photos_display.height()+$("#loading_describe").height()-$photo_describe.height()-30)
						   .css("left",$photos_display.width()-$photo_describe.width()-15);	
		}
	}

	button_timeline.play(0);
	$(window).on('tap',function(){
		button_timeline.resume(0).invalidate().play();
	});
	$(window).on('click',function(){
		button_timeline.resume(0).invalidate().play();
	});
	$('#pause_button').click(function(){
		pause_boolean = true;
		window.display_timeline.pause();
		$('#animate_play_button').css('display','block');
		$(this).css('display','none');
		$('#previous_button').css('display','block');
		$('#next_button').css('display','block');
		TweenMax.fromTo([$('#previous_button'),$('#next_button')],2,{autoAlpha:0},{autoAlpha:1,ease:'Power3.easeNone'});

		SwipeTurnPage();

	});
	$('#animate_play_button').click(function(){
		pause_boolean = false;
		window.display_timeline.play();
		$('#pause_button').css('display','block');
		$(this).css('display','none');
		TweenMax.fromTo([$('#previous_button'),$('#next_button')],1,{autoAlpha:1},{autoAlpha:0,ease:'Power3.easeNone',onComplete:BtnDisplayNone});
		
		SwipeTurnPageClear();

		function BtnDisplayNone(){
			$('#previous_button').css('display','none');
			$('#next_button').css('display','none');
		}
		
	});
	$('#text_play_button').click(function(){
		if ( window.text_marqueeing ) {
			var $text_display = $("#text_display");

			TweenMax.to( $text_display,1,{autoAlpha:0,ease:'Power2.easeNone',onComplete:ResetTextDisplay});
			
		}	
		else
			TextMarquee();
			
		function ResetTextDisplay() {
			var $photo_describe = $('.photo_describe');
			$photo_describe.css("opacity",1);

			window.text_marqueeing = false;
		}
	});
	$('#previous_button').click(function(){
		if ( window.page_index > 0){
			var current = window.page_index;
			var previous = window.page_index - 1;
			TurnPageJudgement( current, previous );
		}
		else {
			var current = window.page_index;
			var previous = parseInt( window.photos.length - 1 );
			TurnPageJudgement( current, previous );
		}
	});
	$('#next_button').click(function(){
		if ( window.page_index < window.photos.length -1 ){		
			var next = window.page_index + 1;
			var current = window.page_index;
			TurnPageJudgement( current, next );
		}
		else {
			var next = 0
			var current = window.page_index;
			
			TurnPageJudgement( current, next );
		}
	});
	/* ============Screen Swipe left and swipe right =====================*/
	function SwipeTurnPage() {
		$(window).on('swipeRight',function(){
			if ( window.page_index > 0){
				var current = window.page_index;
				var previous = window.page_index - 1;
				TurnPageJudgement( current, previous );
			}
			else {
				var current = window.page_index;
				var previous = parseInt( window.photos.length - 1 );
				TurnPageJudgement( current, previous );
			}
		});
		$(window).on('swipeLeft',function(){
			if ( window.page_index < window.photos.length -1 ){		
				var next = window.page_index + 1;
				var current = window.page_index;
				TurnPageJudgement( current, next );
			}
			else {
				var next = 0
				var current = window.page_index;
				
				TurnPageJudgement( current, next );
			}
		});
	}
	function SwipeTurnPageClear(){
	 	$(window).unbind('swipeLeft');
		$(window).unbind('swipeRight');	
	}
	
}
function ReadCurrentLabel(){
	var current_label = window.page_index;
	return current_label;
}

function TurnPageJudgement(  current, turn_index ){
	var end_name = ReadEndAnimateName( current );
	var end_sec = ReadEndAnimateSeconds( current );
	var start_name = ReadStartAnimateName( turn_index );
	var start_sec = ReadStartAnimateSeconds( turn_index );
	var time = window.display_timeline.getLabelTime( turn_index );
	var $photo_container_turn_index = $("#photo_container_"+turn_index);

	for(var i=0;i<window.photos.length;i++){
		UndisplayContainer( $('#photo_container_'+i) );	
		$('#photo_container_'+i+'_clone').css('display','none');
		$('#photo_describe_'+i).css("display","none");
	}
	window.display_timeline.resume(time).pause();
	DisplayContainer( $photo_container_turn_index );
	$('#photo_describe_'+turn_index).css("display","block");

	$photo_container_turn_index.css("opacity",1);
	$('.CA_container').css('display','none');
	window.page_index = turn_index;

}
