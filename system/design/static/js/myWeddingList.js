﻿$(window).load( function(){
	$("#logo").click( function(e) { e.preventDefault(); } );
	GetUser();
	SetProjectList();
	EditProject();

	$(window).resize(function(){
		WindowResizeAdjust();

	});
})

function WindowResizeAdjust(){

	var album_edit = $('#album_edit_frame');
	var win_height = $(window).height();
	var win_width = $('#edit_album_page_container').width();
	var frame_height = (win_height*0.9);
	var frame_width = frame_height*2/3;
	
	$(album_edit)
		.height( frame_height )
		.width( frame_width )
		.css('margin-top',0)
		.css('margin-left',(win_width-frame_width)/2);
}

function SetProjectList() {
	if ( window.project_list.none )
		;
	else {
		for ( var i = 0; i < window.project_list.length; i++ ) {
			$('<div class="panel panel-info well well-sm"> \
				<div class="panel-heading"> \
					<h3 class="panel-title"> \
						婚禮專案 \
					</h3> \
				</div> \
				<div class="panel-footer"> \
					<div class="row"> \
						<p class="col-xs-12 col-md-6" style="margin-top:10px">有效期限: '+window.project_list[i].dueDate.split('+')[0]+'</p> \
						<div class="btn-group col-xs-12 col-md-6"> \
							<button id="eidt_project_' + i + '" value="'+window.project_list[i].pageId+'" type="button" class="btn btn-default">電子婚紗編輯</button> \
						</div> \
					</div> \
				</div> \
			</div>').prependTo($('#project_content'));
		}	
	}
}

function GetUser() {
	$.ajax({
		tpye: 'GET',
		url: '/GetUser/',
		dataType: 'json',
		success: function(JData) {
			window.user_obj = JData;
			if (window.user_obj != null) {
				$("#functional_nav_bar").append($('<li><a id="add_new_project" href="#"><span class="glyphicon  glyphicon-plus" style="margin-right:5px;"></span>新增專案</a></li>'))
									.append($('<li><a id="user_name" href="#"></a></li>'))
									.append($('<li><a id="logout" href="/Logout/">Log out</a></li>'))
									.append($('<li><a id="back_to_view" style="display:none;">回到總覽</a></li>'))
									.append($('<li><a id="picture_edit" style="display:none;">相片編輯</a></li>'));

				$("#user_name").html(window.user_obj.name);
				$("#user_name").click( function(e) { e.preventDefault(); } );
				$("#add_new_project").click( function(e) { e.preventDefault();
					$.ajax({
						type: 'POST',
						url: '/AddNewPage/',
						dataType: 'json',
						data: {"class_name": window.class_name, "theme_name": window.theme_name},
						success: function(JData) {
							if (JData.error) {
								alert(JData.error);
							}
							else {
								console.log(JData.pageId);
								console.log(JData.dueDate);
								
								var project_time = JData.dueDate;
								var project_id = JData.pageId;

								$('<div class="panel panel-info well well-sm"> \
									<div class="panel-heading"> \
										<h3 class="panel-title"> \
											婚禮專案 \
										</h3> \
									</div> \
									<div class="panel-footer"> \
										<div class="row"> \
											<p class="col-xs-12 col-md-6" style="margin-top:10px">有效期限: '+project_time.split('.')[0]+'</p> \
											<div class="btn-group col-xs-12 col-md-6"> \
												<button id="eidt_project" type="button" value="'+project_id+'" class="btn btn-default">電子婚紗編輯</button> \
											</div> \
										</div> \
									</div> \
								</div>').prependTo($('#project_content'));
								EditProject();
							}
						},
						error: function() {
							alert("ERROR!!");
						}
					});

				});

			}
			else {
				$("#functional_nav_bar").append($('<li><a id="go_to_official" href="/">前往註冊</a></li>'))
										.append($('<li><a id="login" href="#">登入</li>'));

				$("#login").click( function(e) {
					e.preventDefault();
					if ( $("#login_panel").height() == 0 ) {
						TweenMax.to( $("#login_panel"), 1, { "height":150, "autoAlpha":1} );
						$('#login_account').focus();
						$($('li'), $("#functional_nav_bar")).removeClass('active');
						$(this).parent().addClass('active');
					}
					else {
						$($('li'), $("#functional_nav_bar")).removeClass('active');
						TweenMax.to( $("#login_panel"), 1, { "height":0, "autoAlpha":0} );
					}
				})

				$("#login_submit").click( function(e) {
					e.preventDefault();
					fileUpload( document.getElementById('login_form'), '/Login/', 'login_res');
				})
			}
			
		},
		error: function(JData) {
			alert("ERROR!!");
		}

	})

}

function EditProject(){
	$('.btn').click(function(){
		var page_id=$(this).attr('value');
		var page_width = $(window).width();
		TweenMax.to( $('#project_content'),1,{x:-page_width,ease:'Power1.easeInOut',onComplete:ProjecDisplayNone} );

		$('<iframe id="album_edit_frame" style="opacity:0;" src="/WeddingAlbumSetting/'+page_id+'" class="frame"></iframe>').appendTo($('#edit_album_page_container'));
		AppendPictureFrame();

		function ProjecDisplayNone(){
			
			TweenMax.to($('#album_edit_frame'),0.8,{autoAlpha:1,ease:'Power2.easeInOut'});
			$('.panel').css('display','none');
			NavBarButtonClear();			
			$('#picture_edit').css('display','block');
			$('#back_to_view').css('display','block');
			BackToView();
		}
	});
}


function NavBarButtonClear(){
	$('#add_new_project').css('display','none');
	$('#user_name').css('display','none');
	$('#logout').css('display','none');
	$('#picture_edit').css('display','none');
	$('#back_to_view').css('display','none');
}

function BackToView(){
	$('#back_to_view').click(function(e){
		e.preventDefault();
		
		$('.panel').css('display','block');
		TweenMax.to( $('#project_content'),2,{x:0,ease:'Power1.easeInOut',onComplete:ViewButtonDisplay} );		
	});

	function ViewButtonDisplay(){
		NavBarButtonClear();
		$('#add_new_project').css('display','block');
		$('#user_name').css('display','block');
		$('#logout').css('display','block');

	}
}

function AppendPictureFrame(){
	$('#edit_album_page_container').css('display','block');

	var album_edit = $('#album_edit_frame');
	var win_height = $(window).height();
	var win_width = $('#edit_album_page_container').width();
	var frame_height = (win_height*0.9);
	var frame_width = frame_height*2/3;
	
	$(album_edit)
		.height( frame_height )
		.width( frame_width )
		.css('margin-top',0)
		.css('margin-left',(win_width-frame_width)/2);

}



//------------------------------------------------------------------

function fileUpload(form, action_url, div_id) {
    // Create the iframe...
    var iframe = document.createElement("iframe");
    iframe.setAttribute("id", "upload_iframe");
    iframe.setAttribute("name", "upload_iframe");
    iframe.setAttribute("width", "0");
    iframe.setAttribute("height", "0");
    iframe.setAttribute("border", "0");
    iframe.setAttribute("style", "width: 0; height: 0; border: none;");
 
    // Add to document...
    form.parentNode.appendChild(iframe);
    window.frames['upload_iframe'].name = "upload_iframe";
 
    iframeId = document.getElementById("upload_iframe");
 
    // Add event...
    var eventHandler = function () {
 
            if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
            else iframeId.removeEventListener("load", eventHandler, false);
 
            // Message from server...
            if (iframeId.contentDocument) {
                content = iframeId.contentDocument.body.innerHTML;
            } else if (iframeId.contentWindow) {
                content = iframeId.contentWindow.document.body.innerHTML;
            } else if (iframeId.document) {
                content = iframeId.document.body.innerHTML;
            }
 			
 			JData = JSON.parse(content);
 			if ( JData.error )
            	alert(JData.error);
            else {
            	window.location.reload();
            }
 
            // Del the iframe...
            if (iframeId != null)
            	setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
        }
 
    if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
    if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);
 
    // Set properties of form...
    form.setAttribute("target", "upload_iframe");
    form.setAttribute("action", action_url);
    form.setAttribute("method", "post");
    form.setAttribute("enctype", "multipart/form-data");
    form.setAttribute("encoding", "multipart/form-data");
 
    // Submit the form...
    form.submit();
 
    document.getElementById(div_id).innerHTML = "Uploading...";
}



//------------------------------------------------------------------ Add_By_Cycho
$( document ).ready(function() {

	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				// Does this cookie string begin with the name we want?
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	function csrfSafeMethod(method) {
		// these HTTP methods do not require CSRF protection
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
  
	function sameOrigin(url) {
		// test that a given url is a same-origin URL
		// url could be relative or scheme relative or absolute
		var host = document.location.host; // host + port
		var protocol = document.location.protocol;
		var sr_origin = '//' + host;
		var origin = protocol + sr_origin;
		// Allow absolute or scheme relative URLs to same origin
		return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
			(url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
			// or any other URL that isn't scheme relative or absolute i.e relative.
			!(/^(\/\/|http:|https:).*/.test(url));
	}
	  
	$.ajaxSetup({
		crossDomain: false, // obviates need for sameOrigin test
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type)) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		}
	});
});
//------------------------------------------------------------------ Add_By_Cycho