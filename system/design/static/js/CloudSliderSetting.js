﻿window.describe_array = new Array();

$(window).load(function(){

	window.location.hash = "setting-title-n-date";

	InitialPageSetting();
	AlbumDatepickerSetting();
	InitialTitleAppend();
	$(window).resize(function(){
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) )
			;
		else {
			ContainerResize();
			ContainerAdjustPosition();		
			PhotoListResize();
				
		}
	});
	
	$(window).on('hashchange',function(){
		var hash = window.location.hash.split("#")[1];
		var $photo_setting = $("#photo_setting"); 
		if( hash == 'setting-title-n-date'){
			viewModel.currentSettingPhoto(0);
					
			$photo_setting.css("display","none");
		}
		else if( hash == 'setting-page' ){
			viewModel.currentSettingPhoto(1);
			PhotoListResize();
		}
		else if( hash == 'edit-dairy'){
			viewModel.currentSettingPhoto(2);
		}
		else if( hash == 'photo-edit' ){
			viewModel.currentSettingPhoto(999);

			$photo_setting.css("display","block");
		}
		else;
	});


	ContainerResize();
	ContainerAdjustPosition();
	ButtonClickSetting();		
});

function WindowFixNScroll(){

}


function InitialPhotoList() {
	var $target = $("#photo_setting_container");

	for ( var i = 0; i < window.photos.length; i++ ) {

		sp = window.photos[i].url.split('/');
		var file_name = sp[sp.length-1].split('.')[0];

		var $container = $('<div id="' + i +'_container" style="overflow:hidden;" class="row list_photo_container"></div>');
	  	var $image_div = $('<div id="' + i +'_img_div" style="background-color:black; padding:0;" class="col-xs-4 col-sm-3 col-md-2 list_photo_img_div"></div>');
	  	var $detail_div = $('<div id="' + i +'_detail_div" class="col-xs-8 col-sm-9 col-md-10 list_photo_detail" style="z-index:0;"></div>');
	  	var $describe = $('<input id="'+ i + '_describe" style="height:50%; max-height:40px;" maxlength="50" type="text" placeholder="相片敘述" class="form-control col-xs-12 list_photo_describe" >');

	  	$container.appendTo($target);
	  	$image_div.appendTo($container);
	  	$detail_div.appendTo($container);
	  	$describe.appendTo($detail_div);
	  	$describe.val(ReplaceInputStrToText(window.photos[i].describe));
	  	
	  	$container.css("margin-bottom","10px");

	  	var height = $image_div.width();
	  	$container.height(height+10);
	  	$image_div.height(height);
	  	$detail_div.height(height);

	  	// ====================================

		var url = window.photos[i].url;
		var ph_index = i;

		$describe.attr("id",ph_index+"_describe");

		img = new Image();
		img.src = url;
		$(img).addClass('list_photo_img').attr('id', file_name + "_img");
		$(img).attr('alt','圖檔錯誤,' + url);
		$(img).appendTo( $image_div );

		$(img).load(function() {
			if ( $(this).height() > $(this).width() ) {
				$(this).height( $(this).parent().height() );
				var left = $(this).parent().width()/2 - $(this).width()/2;
				$(this).css('margin-left',left + 'px');
			}
			else {
				$(this).width( $(this).parent().width() );
				var top = $(this).parent().height()/2 - $(this).height()/2;
				$(this).css('margin-top', top + 'px');
			}
		})

		var $describe_list = $('<div id="dropdown_list_'+ph_index+'" class="dropdown describe_dropdown"> \
						  <ul class="dropdown-menu col-xs-12" style="max-height:100px; overflow-x:hidden; overflow-y:scroll; -webkit-transform: translateZ(0px);" role="menu" aria-labelledby="dropdown_list_'+ ph_index +'"></ul> \
						</div>');
		var $index_describe = $("#"+ph_index+"_describe");
		$index_describe.data('ph_index', ph_index);
		$describe_list.appendTo($detail_div);

		$index_describe.on('click',function(e){
			e.stopPropagation();
			$(".describe_dropdown").removeClass('open');
			//automatically scroll to target div
			var index = parseInt($(this).data('ph_index'));
			var scroll_top = ($(this).parent().parent().height())*index;
			TweenMax.to( $target,0.5,{ scrollTop:scroll_top,ease:'Power0.easeNone' } );
			//clear dropdown-menu
			var $container =  $(".dropdown-menu", $(this).parent());
			$container.html('');
			//judge the list element is repeat or not
			for(var i=0;i<window.photos.length;i++){
				var $describe = $("#"+i+"_describe");
				
				if( $describe.val().length ){

					var found = false;
					for (var k = 0; k < window.describe_array.length && !found; k++) {
						if ( window.describe_array[k] == $describe.val() )
							found = true;
					}

					if (!found) {
						window.describe_array.push( $describe.val() );
						if ( window.describe_array.length )
							;
					}
						
				}
				else ;
			}

			//append list element to drop-menu
			for (var m = 0; m < window.describe_array.length; m++ ){
				var $element = $('<li></li>');
				var $element_a = $('<a class="dropdown_element" tabindex="-1"></a>');
				
				$element_a.html( window.describe_array[m] ).data('index',$(this).data('ph_index'));
				$element.append($element_a).appendTo($container);

				$element_a.click(function(e) {
					var index = $(this).data('index');
					
					$("#"+index+"_describe").val($(this).html());
					$(".describe_dropdown").removeClass('open');
				});
			}
			//open drop-menu
			if (window.describe_array.length)
				$(".describe_dropdown", $(this).parent()).addClass('open');
			else;
			
		});
		//close the drop-menu
		$('#photo_setting_container').on('click',function(e){
			e.stopPropagation();
			$(".describe_dropdown").removeClass('open');
		});

		$edit_btn = $('<button style="margin-top:5px; margin-right:10px;" class="col-xs-6 col-sm-4 btn btn-primary btn-xs list_photo_btn">編輯相片</button>');
		$edit_btn.data('ph_index', ph_index);
		$edit_btn.appendTo($detail_div);


		$edit_btn.click( function(e) {
			e.preventDefault();
			// 編輯照片

			$inputs = $('input');
			var amount = $inputs.length;

			for( var i = 0 ; i < amount; i++ ) {
				$inputs[i].blur();
			}
			
			var index = parseInt($(this).data('ph_index'));
			window.photo_index = index;
			var url = window.photos[index].url;
			var $photo_setting = $("#photo_setting");
    		var $display_area = $("#display_area");
    		var $photo_container = $('<div id="photo_container_'+index+'"></div>');

    		$photo_setting.css("display","block");
    		$display_area.css("display","block");

    		var photo_obj = new Image();
			photo_obj.src = window.photos[index].url;
			$(photo_obj).height( $display_area.height()).width('auto');
			$(photo_obj).attr('id',index).css({'left':'0px','top':'0px','position':'relative','z-index':1});
			$photo_container.append(photo_obj);
			$photo_container.appendTo($display_area);
			
			$(photo_obj).load( function() {
				var photo_id = $(this).attr('id');
				$(this).css("height",$display_area.height()*window.photos[ parseInt(photo_id) ].zoom);
				$(this).css("width","auto");
				PhotoContainerAdjust( parseInt(photo_id) );
				var ratio = window.photos[ parseInt(photo_id)].zoom;
				var left = window.photos[ parseInt(photo_id) ].positionLeft;
				var top = window.photos[ parseInt(photo_id) ].positionTop;
				PhotoPositionAdjust( parseInt(photo_id), left, top );
			});

			ZoomGestureMobile( index );
			PhotoDraggable( index );	
			SaveAllPhotoDescribe(function(){
				viewModel.currentSettingPhoto(999);
			});

			
	    });

		$rmv_btn = $('<button style="margin-top:5px;" class="col-xs-4 col-sm-2 btn btn-danger btn-xs list_photo_btn">刪除</button>');
		$rmv_btn.data('url', url);
		$rmv_btn.appendTo($detail_div);

		$rmv_btn.click( function(e) {
			e.preventDefault();
			// 刪除照片

			var $target = $(this).parent().parent();
			var index = $(this).data('ph_index');
			var url = $(this).data('url');
    		$.ajax({
        		type:'POST',
        		dataType: 'JSON',
        		data:{"pageId":window.pageId,"url":url},
        		url:'/RemovePhoto/',
        		success: function(JData){
        			if( JData.error )
        				 alert(JData.error);
        			else if ( JData.remove ){
      	  				$target.remove();
      	  				var ph_index = -1;
      	  				for ( var i = 0; i < window.photos.length; i++ ) {

      	  					if ( window.photos[i].url == JData.remove ) {
      	  						ph_index = i;
      	  					}
      	  				}
      	  				if ( ph_index != -1 )
      	  					window.photos.splice(ph_index,1);	
           			}
        			else   				
        				;
        		},
        		error: function(){
        			alert("/RemovePhoto/ function error!");
        		}
        	})
		});

		//append empty div
		if( i== window.photos.length-1 ){
			var $empty_div = $('<div class="empty_div"></div>');
			var ext_height = $target.height() - $(".list_photo_container").height();
			$empty_div.height( ext_height ).appendTo($target);
		}
			

	}	

}


function InitialTitleAppend() {
	var $edit_title = $("#edit_title");
	var $edit_date = $("#edit_date");
	
	$edit_date.val(window.page_date);
	$edit_title.val(ReplaceInputStrToText( window.page_title ));
}

function InitialTextArea() {
	var $text_diary_in = $("#text_diary_in");
	var text_diary = ReplaceInputStrToText(window.text);
	$text_diary_in.val(text_diary);
}

function ReplaceInputStr(uStr) {
	return uStr.replace(/\n/g,"<br>").replace(/ /g,"&nbsp").replace(/"/g,'&#34;').replace(/'/g,"&#39;").replace(/\\/g,"&nbsp");
}

function ReplaceInputStrToText(uStr) {
	return uStr.replace(/<br>/g,"\n").replace(/&nbsp/g," ").replace(/&#34;/g,'"').replace(/&#39;/g,"'");
}

function PhotoListResize() {
	if ( $('.list_photo_conatiner', $('#photo_setting_container') ) ) {
		var height = $( '.list_photo_img_div' ).width();

		$('.list_photo_conatiner', $("#photo_setting_container")).height( height );
		$('.list_photo_img_div', $("#photo_setting_container")).height( height );
		$('.list_photo_detail', $("#photo_setting_container")).height( height );

		var imgs = $('.list_photo_img',  $("#photo_setting_container") );

		$.each( imgs, function( index, item ) {
			if ( $(item).height() > $(item).width() ) {
				$(item).height( $(item).parent().height() );
				var left = $(item).parent().width()/2 - $(item).width()/2;
				$(item).css('margin-left',left + 'px');
			}
			else {
				$(item).width( $(item).parent().width() );
				var top = $(item).parent().height()/2 - $(item).height()/2;
				$(item).css('margin-top', top + 'px');
			}
		})

		if ( $(window).width() >= 768 ) {
			$('.list_photo_btn', $("#photo_setting_container")).removeClass('btn-xs');
		}
		else {
			$('.list_photo_btn', $("#photo_setting_container")).addClass('btn-xs');
		}


	}
	else;
}


function ContainerAdjustPosition(){

	var window_height = $(window).height();
	var window_width = $(window).width();
	var half_win_width = window_width/2;
	var half_win_height = window_height/2;
	var display_width = window_width;
	var display_height = window_height;

	var $album_primary_setting= $("#album_primary_setting");
	var $photo_setting = $("#photo_setting");
	var $photo_setting_container = $("#photo_setting_container");
	var $display_area = $("#display_area");
	
	$album_primary_setting.css("margin-top", -50+(window_height-$album_primary_setting.height())/2 );

	$photo_setting.css("margin-top",(window_height - $display_area.height())*0.3 + "px" );
			
	var display_left =  ($photo_setting.width()-$display_area.width())/2;

	$display_area.css("margin-top",0 );
	$display_area.css("margin-left",display_left );

	/*================================================================================================================*/
	var $photo_setting_return = $("#photo_setting_return");
	var $photo_setting_zoom = $("#photo_setting_zoom");

	$photo_setting_return.css("margin-left",display_left-15).css("margin-bottom",10);
	$photo_setting_zoom.css("margin-bottom",10);

	/*================================================================================================================*/
	var $text_diary_return = $("#text_diary_return");
	var $text_diary_setting = $("#text_diary_setting");
	var $text_diary_in = $("#text_diary_in");

	$text_diary_setting.css("margin-top",window_height *0.1);
	$text_diary_return.css("margin-left",display_left -3 );
	$text_diary_return.css("margin-top",-window_height*0.05);

	var text_in_left = ( ( $text_diary_setting.width() - $text_diary_in.width())/2); 
	$text_diary_in.css("margin-left",text_in_left);

	var $select_date_div = $("#select_date_div");
	var $parent_div = $("#select_date_div").parent();
	//set the datepicker in the middle of window 
	$select_date_div.css("margin-left",($parent_div.width()-$(".ui-datepicker",$select_date_div).width())/2);
}

function ContainerResize(){
	var win_width = $(window).width();
	var win_height = $(window).height();
	var half_win_width = win_width/2;
	var half_win_height = win_height/2;
	var display_width = win_width;
	var display_height = win_height;

	var $photo_setting = $("#photo_setting");
	var $photo_setting_container = $("#photo_setting_container")
	var $display_area = $("#display_area");
	var $text_diary_setting = $("#text_diary_setting");

	$photo_setting_container.height( win_height-100 );
			
	$display_area.height( win_height - 160 );
	$display_area.width( $photo_setting.height()*212/317 );


	/*==================================================================*/
	if ( win_width/212*317 + 150 <= win_height) {
		// 寬滿版配置
		
		display_width = win_width;
		display_height=win_width/212*317;

		$display_area.width(display_width);
		$display_area.height(display_height);
	}
	else if ( win_height*0.75/317*212 <= win_width ) {
		display_width = win_height*0.75/317*212;
		display_height = win_height*0.75;

		$display_area.height(display_height);
		$display_area.width(display_width);
	}
	else {
		display_width = win_width;
		display_height=win_width/212*317;

		$display_area.width(display_width);
		$display_area.height(display_height);
	}
	/*------------------ text edit -----------------------------*/
	var $text_diary_in = $("#text_diary_in");
	var $text_diary_setting = $("#text_diary_setting");
	var $text_diary_return = $("#text_diary_return");

	$text_diary_setting.height( win_height - 100 );
	$text_diary_in.width($display_area.width()).height($display_area.height());
	
	var text_btn_width = ( $text_diary_setting.width() - $text_diary_in.width() )/2;
	
	$text_diary_return.width( $display_area.width() );	
	/*----------------- photo setting button -------------------*/
	var $photo_setting_return = $("#photo_setting_return");
	var $photo_setting_zoom = $("#photo_setting_zoom");
	var $photo_display_area = $("#display_area");

	var btn_width = Math.ceil($photo_display_area.width()/2);
	
	$photo_setting_return.width(btn_width); 
	$photo_setting_zoom.width(btn_width); 
	
	window.display_rate = $display_area.width()/212;

}

function InitialPageSetting(){

	var $add_new_photo = $("#add_new_photo_button");
	var $edit_dairy = $("#edit_dairy");
	var $return_primary_setting = $("#return_primary_setting");
	var $page_setting_complete = $("#page_setting_complete");

	$add_new_photo.css("display","none");
	$edit_dairy.css("display","none");
	$return_primary_setting.css("display","none");
	$page_setting_complete.css("display","none");

	var $text_diary_in = $('{% block content %}<textarea id="text_diary_in"></textarea>{% endblock %}');
	var $text_diary_setting = $("#text_diary_setting");
	

	$text_diary_in.appendTo($text_diary_setting);

}

function DisplayPhotoSettingButton(){
	var $add_new_photo = $("#add_new_photo_button");
	var $edit_dairy = $("#edit_dairy");
	var $return_primary_setting = $("#return_primary_setting");
	var $page_setting_complete = $("#page_setting_complete");

	$add_new_photo.css("display","block");
	$edit_dairy.css("display","block");
	$return_primary_setting.css("display","block");
	$page_setting_complete.css("display","block");	
}



function AlbumDatepickerSetting(){
	var $edit_date = $("#edit_date");
	var today = new Date();
	var $select_date_div = $("#select_date_div");
	var $parent_div = $("#select_date_div").parent();
	var t_date ="";

	$select_date_div.datepicker({
		// autoSize:true,
		altField:"#edit_date",
		setDate:today,
		firstday:0,
		showWeek:false,
		changeMonth:true,
		changeYear:true,
		dateFormat:'yy-mm-dd',
		monthNames:["ㄧ月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
		monthNamesShort:["ㄧ月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
		dayNames:["日","一","二","三","四","五","六"],
		dayNamesMin:["日","一","二","三","四","五","六"],
		beforeShowDay: function(date){
			var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
            if ( parseInt(m+1) < 10)
				var mm = '0' + (m+1);
			else 
				var mm = m+1;
			if ( parseInt(d) < 10)
				var dd = '0' + d;
			else 
				var dd = d;
			t_date = y+'-'+ mm +'-'+dd;
    	    if( (t_date) == window.page_date){
                return [false, '',''];
            }

            return [true,'',''];
		}

	});

	//set the datepicker in the middle of window 
	$select_date_div.css("margin-left",($parent_div.width()-$(".ui-datepicker",$select_date_div).width())/2);
}


function ButtonClickSetting(){
	var $album_primary_submit = $("#album_primary_submit");
	var $edit_dairy = $("#edit_dairy");
	var $photo_setting_return = $("#photo_setting_return");
	var $text_diary_return = $("#text_diary_return");
	var $photo_setting_zoom = $("#photo_setting_zoom");
	var $display_area = $("#display_area");
	var $return_primary_setting = $("#return_primary_setting");
	var $page_setting_complete = $("#page_setting_complete");
	var $photo_setting_container = $("#photo_setting_container");

	$album_primary_submit.click(function(e){
		e.preventDefault();
		$photo_setting_container.html("");
		var album_date = $("#edit_date").val();
		var album_yy = album_date.split('-')[0];
		var album_mm = album_date.split('-')[1];
		var album_dd = album_date.split('-')[2];
		var album_title = ReplaceInputStr( $("#edit_title").val() );
		var pageId = window.pageId;

		
 		
		if( album_date.length > 0 && album_title.length > 0 ){
			var data = { "pageId" : pageId, "title" : album_title,"date" : album_date, "yy":album_yy ,"mm":album_mm,"dd":album_dd,"className":"CloudSliders","themeName":"CloudSliders" };
			window.location.hash = "setting-page";
			$.ajax({
				type: 'POST',
				url: "/SaveAblumTitleAndDate/",
				data: data,
				dataType: 'json',
				success: function(JData) {
					if ( JData.error ) {
						alert(JData.error);
					}
					else{

						window.text = JData.text;
						window.pageId = JData.pageId;
												
						var in_animationsJ = JData.in_animations;
						window.in_animation_name = JSON.parse(in_animationsJ);
						
						var out_animationsJ = JData.out_animations;
						window.out_animation_name = JSON.parse(out_animationsJ);
						
						var photosJ = JData.photos;
						window.photos = JSON.parse(photosJ);

						window.text = JData.text;

												
						DisplayPhotoSettingButton();
						viewModel.currentSettingPhoto(1);
						ContainerResize();
						ImageUploadSetting();
						InitialTextArea();
						InitialPhotoList();
						WindowFixNScroll();
					}
						

				},
				error: function() {
					alert("ERROR!!!");
				}
			});	
		}
		else{
			alert("請填寫標題和選擇日期！");
		}
	});
	
	$edit_dairy.click(function(e){ //編輯日誌內容
		e.preventDefault();		
		
		window.location.hash = "edit-dairy";

		SaveAllPhotoDescribe(function(){
			viewModel.currentSettingPhoto(2);
			if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) )
				;
			else {
				ContainerResize();
				ContainerAdjustPosition();
			}
		});

	});
	
	$photo_setting_return.click(function(e){ //返回設定頁面
		e.preventDefault();
		
		var $photo_setting = $("#photo_setting"); 
		
		UploadPhotoSetting(function(){});
		
		$display_area.html("");
		$photo_setting.css("display","none");
	 	viewModel.currentSettingPhoto(1);
	 	PhotoListResize();
	 	window.location.hash = "setting-page";
	});

	$photo_setting_zoom.click(function(e){
		e.preventDefault();
		// var index = parseInt($("img",$("#display_area")).attr("id"));
		var $photo_container = $("#photo_container_"+window.photo_index);

		var $img = $('#'+window.photo_index,'#photo_container_'+window.photo_index);
		var zoom = window.photos[window.photo_index].zoom;

		if ( window.photos[window.photo_index].zoom<1.5 ){
			zoom = zoom +0.1;
			window.photos[window.photo_index].zoom = Math.floor(zoom*10)/10;

			$img.height( $display_area.height()* zoom);
			$img.css( "width" , "auto");
			
			var left = ReadPhotoPositionLeft();
			var top = ReadPhotoPositionTop();
			PhotoContainerAdjust( window.photo_index );
			PhotoPositionAdjust( window.photo_index, left, top );
			SavePhotoPosition( window.photo_index );
		}
		else{
			zoom = 1;
			window.photos[window.photo_index].zoom = Math.floor(zoom*10)/10;

			$img.height( $display_area.height()* zoom);
			$img.css( "width" , "auto");
			
			var left = ReadPhotoPositionLeft();
			var top = ReadPhotoPositionTop();
			PhotoContainerAdjust( window.photo_index );
			PhotoPositionAdjust( window.photo_index, left, top );
			SavePhotoPosition( window.photo_index );
		}

	});

	$text_diary_return.click(function(e){
		e.preventDefault();
		var text = ReplaceInputStr( $("#text_diary_in").val() );
		window.text = text;

		UploadText(function(){
			viewModel.currentSettingPhoto(1);
			PhotoListResize();

		});
		
	});

	$return_primary_setting.click(function(e){
		e.preventDefault();

		SaveAllPhotoDescribe(function(){
			UploadPhotoSetting(function(){ 
				window.photos=[];
				$("#photo_setting_container").html("");
			});		
		});
		viewModel.currentSettingPhoto(0);
		var $select_date_div = $("#select_date_div");
		var $parent_div = $select_date_div.parent();
		$select_date_div.css("margin-left",($parent_div.width()-$(".ui-datepicker",$select_date_div).width())/2);
		window.location.hash = "setting-title-n-date";

	});

	$page_setting_complete.click(function(e){
		e.preventDefault();
		SaveAllPhotoDescribe(function(){
			UploadPhotoSetting(Complete);		
		});
		
	});
}

function Complete() {
	var path="";
	$.ajax({
		type: 'POST',
		url: '/GenerateQR/',
		dataType: 'json',
		data: {"pageId": window.pageId},
		success:function(JData){
			if ( JData.error ) {
				alert(JData.error);
			}
			else {
				window.location.href = 'http://clider.rs/';
			}
		},
		error: function(){
			alert("ERROR!!");
		}
	})
}

function UploadText(callback) {
	window.text = ReplaceInputStr( $("#text_diary_in").val() );
	$.ajax({
		type: "POST",
		url: "/UploadText/",
		dataType: 'json',
		data: { "pageId" : window.pageId, "text" : window.text},
		success: function(JData) {
			if (JData.error) {
				alert(JData.error);
			}
			else 
				callback();
		},
		error: function() {
			alert("ERROR!!");
		}
	})
}

function UploadPhotoSetting(callback){

	photosJ = JSON.stringify( window.photos );

	$.ajax({
		type:'POST',
		dataType:'json',
		data: { "pageId" : window.pageId, "photos" : window.photos, "photosJ" : photosJ },
		url:'/UploadPhotoSetting/',
		success:function(JData){
			if(JData.error)
				alert(JData.error);
			else {
				callback();
			}
		},
		error:function(){
			alert("照片設定上傳失敗！");
		}
	});
}

function SaveAllPhotoDescribe(callback){
	
	ph_details = $('.list_photo_detail');

	for ( var i = 0; i < ph_details.length; i++ ) {
		window.photos[i].describe  = ReplaceInputStr( $($('.list_photo_describe', ph_details)[i]).val() );
	}

	callback();

}


function ImageUploadSetting(){
	var $add_new_photo = $("#add_new_photo");
	var $photo_setting_container = $("#photo_setting_container");
	var $preview = $("#preview");
 
	$add_new_photo.fileupload({
		url:'/UploadPhoto/',
        dataType: 'json',
        formData:{'pageId':window.pageId},
		autoUpload: true,
		disableImageResize: true,
		previewMaxWidth: 50,
		previewMaxHeight: 50,
		previewCrop: true,
        add: function (e, data) {
        	//remove empty div
           	$(".empty_div").remove();
           	

        	window.photo_index = $(".list_photo_container").length;
        	var $target = $("#photo_setting_container");
        	var $container = $('<div id="' + photo_index +'_container" class="row list_photo_container"></div>');
	      	var $image_div = $('<div id="' + photo_index +'_img_div" style="background-color:black; padding:0;" class="col-xs-4 col-sm-3 col-md-2 list_photo_img_div"></div>');
	      	var $detail_div = $('<div id="' + photo_index +'_detail_div" class="col-xs-8 col-sm-9 col-md-10 list_photo_detail"></div>');
	      	var $describe = $('<input id="'+ photo_index + '_describe" style="height:50%; max-height:40px;" maxlength="50" type="text" placeholder="相片敘述" style="margin-top:5px;" class="form-control col-xs-12 list_photo_describe" >');
	      	var $bar = $('	<div style="padding:0px;" class="progress progress-striped active col-xs-12"> \
	      						<div id="' + photo_index + '_bar" class="progress-bar progress-bar-info"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> \
    								<span class="sr-only">45% Complete</span> \
  								</div> \
							</div>' );
	      	
	      	$container.appendTo($target);
	      	$image_div.appendTo($container);
	      	$detail_div.appendTo($container);
	      	$describe.appendTo($detail_div);
	      	$bar.height(10);
	      	$bar.appendTo($container);
	      	
	      	$container.css("margin-bottom","10px");

	      	var height = $image_div.width();
	      	$container.height(height+10);
	      	$image_div.height(height);
	      	$detail_div.height(height);
		    data.submit();

		    //remove empty div
           	$(".empty_div").remove();
        },
        progress:function(e, data) {
        	
        	var progress = parseInt(data.loaded / data.total * 100, 10);
			$( $(".progress-bar") ,$("#" + data.files[0].name.split('.')[0] + "_container")).css('width',progress + '%');
        },
        done: function (e, data) {
        	var res = data.response().result;
        	var index = window.photos.length;
        	window.photos.push(res.photo[0]);
        	$container = $("#" + index + "_container").attr('id', index + "container_done");
        	$img_div = $("#" + index + "_img_div", $container);
        	$detail_div = $("#" + index + "_detail_div", $container);
        	$describe = $("#" + index + "_describe", $container);
        	$bar = $("#" + index + "_bar", $container);

        	if ( res.error ) {
        		$describe.remove();
        		$('<p>').html(res.error).css('color','red').appendTo($detail_div);
        		
        		$bar.removeClass("progress-bar-info");
        		$bar.addClass("progress-bar-danger");
        		$bar.parent().removeClass("active");
        	} 
        	else {
        		//append empty div
        		if ( $(".empty_div").length )
        			;
        		else{
        			var $empty_div = $('<div class="empty_div"></div>');
					var ext_height = $photo_setting_container.height() - $container.height();
					$empty_div.height( ext_height ).appendTo($photo_setting_container);	
        		}        		
        		var url = res.photo[0].url;
        		var ph_index = window.photos.length-1;

        		$describe.attr("id",ph_index+"_describe");

        		img = new Image();
        		img.src = url;
        		$(img).addClass('list_photo_img').attr('id', photo_index + "_img");
        		$(img).attr('alt','圖檔錯誤,' + url);
        		$(img).appendTo( $img_div );

        		$(img).load(function() {
        			if ( $(this).height() > $(this).width() ) {
        				$(this).height( $(this).parent().height() );
        				var left = $(this).parent().width()/2 - $(this).width()/2;
        				$(this).css('margin-left',left + 'px');
        			}
        			else {
        				$(this).width( $(this).parent().width() );
        				var top = $(this).parent().height()/2 - $(this).height()/2;
        				$(this).css('margin-top', top + 'px');
        			}
        		});

        		var $describe_list = $('<div id="dropdown_list_'+ph_index+'" class="dropdown describe_dropdown"> \
										  <ul class="dropdown-menu col-xs-12" style="max-height:100px; overflow-x:hidden; overflow-y:scroll; -webkit-transform: translateZ(0px);" role="menu" aria-labelledby="dropdown_list_'+ ph_index +'" style="z-index:999;"></ul> \
										</div>');

				var $index_describe = $("#"+ph_index+"_describe");
				$index_describe.data('ph_index', ph_index);
				$describe_list.appendTo($detail_div);

				$index_describe.on('click touchdown',function(e){
					e.stopPropagation();
					$(".describe_dropdown").removeClass('open');
					//automatically scroll to target div
					var index = parseInt($(this).data('ph_index'));
					var scroll_top = ($(this).parent().parent().height()+10)*index;
					TweenMax.to( $photo_setting_container,0.5,{ scrollTop:scroll_top,ease:'Power0.easeNone' } );
					//clear dropdown-menu
					var $container =  $(".dropdown-menu", $(this).parent());
					$container.html('');
					//judge the list element is repeat or not
					for(var i=0;i<window.photos.length;i++){
						var $describe = $("#"+i+"_describe");
						
						if( $describe.val().length ){

							var found = false;
							for (var k = 0; k < window.describe_array.length && !found; k++) {
								if ( window.describe_array[k] == $describe.val() )
									found = true;
							}

							if (!found) {
								window.describe_array.push( $describe.val() );
								if ( window.describe_array.length )
									;
							}
								
						}
						else ;							
					}

					$index_describe.on('mouseleave',function(e){
						$(".describe_dropdown").removeClass('open');
					});

					//append list element to drop-menu
					for (var m = 0; m < window.describe_array.length; m++ ){
						var $element = $('<li></li>');
						var $element_a = $('<a class="dropdown_element" tabindex="-1"></a>');
						
						$element_a.html( window.describe_array[m] ).data('index',$(this).data('ph_index'));
						$element.append($element_a).appendTo($container);

						$element_a.click(function(e) {
							var index = $(this).data('index');
							$("#"+index+"_describe").val($(this).html());
							$(".describe_dropdown").removeClass('open');
						});
					}
					//open drop-menu
					if (window.describe_array.length)
						$(".describe_dropdown", $(this).parent()).addClass('open');
					else;
					
				});
				//close the drop-menu
				$('#photo_setting_container').on('click',function(e){
					e.stopPropagation();
					$(".describe_dropdown").removeClass('open');
				});
				
        		$edit_btn = $('<button style="margin-top:5px; margin-right:10px;" class="col-xs-6 col-sm-4 btn btn-primary btn-xs list_photo_btn">編輯相片</button>');
        		$edit_btn.data('ph_index', ph_index);
        		$edit_btn.appendTo($detail_div);


        		$edit_btn.click( function(e) {
        			e.preventDefault();
        			// 編輯照片
        			
        			$inputs = $('input');
					var amount = $inputs.length;


					for( var i = 0 ; i < amount; i++ ) {
						$inputs[i].blur();
					}

    				var index = parseInt($(this).data('ph_index'));
        			window.photo_index = index;
        			var url = window.photos[index].url;
        			var $photo_setting = $("#photo_setting");
	        		var $display_area = $("#display_area");
	        		var $photo_container = $('<div id="photo_container_'+index+'"></div>');

	        		$photo_setting.css("display","block");
	        		$display_area.css("display","block");

	        		var photo_obj = new Image();
					photo_obj.src = window.photos[index].url;
					$(photo_obj).height( $display_area.height()).width('auto');
					$(photo_obj).attr('id',index).css({'left':'0px','top':'0px','position':'relative','z-index':1});
					$photo_container.append(photo_obj);
					$photo_container.appendTo($display_area);
					
					$(photo_obj).load( function() {
						var photo_id = $(this).attr('id');
						$(this).css("height",$display_area.height()*window.photos[ parseInt(photo_id) ].zoom);
						$(this).css("width","auto");
						PhotoContainerAdjust( parseInt(photo_id) );
						var ratio = window.photos[ parseInt(photo_id)].zoom;
						var left = window.photos[ parseInt(photo_id) ].positionLeft;
						var top = window.photos[ parseInt(photo_id) ].positionTop;
						PhotoPositionAdjust( parseInt(photo_id), left, top );
					});

					ZoomGestureMobile( index );
					PhotoDraggable( index );	
					SaveAllPhotoDescribe(function(){
						viewModel.currentSettingPhoto(999);
					});

					window.location.hash = "photo-edit";

        		});


        		$rmv_btn = $('<button  style="margin-top:5px;" class="col-xs-4 col-sm-2 btn btn-danger btn-xs list_photo_btn">刪除</button>');
        		$rmv_btn.data('url', url);
        		$rmv_btn.appendTo($detail_div);

        		$rmv_btn.click( function(e) {
        			e.preventDefault();
        			// 刪除照片

        			var $target = $(this).parent().parent();
        			var index = $(this).data('ph_index');
        			var url = $(this).data('url');
	        		$.ajax({
		        		type:'POST',
		        		dataType: 'JSON',
		        		data:{"pageId":window.pageId,"url":url},
		        		url:'/RemovePhoto/',
		        		success: function(JData){
		        			if( JData.error )
		        				 alert(JData.error);
		        			else if ( JData.remove ){
		      	  				$target.remove();
		      	  				var ph_index = -1;
		      	  				for ( var i = 0; i < window.photos.length; i++ ) {

		      	  					if ( window.photos[i].url == JData.remove ) {
		      	  						ph_index = i;
		      	  					}
		      	  				}
		      	  				if ( ph_index != -1 )
		      	  					window.photos.splice(ph_index,1);
		      	  					window.photo_index = $(".list_photo_container").length;
		           			}
		        			else   				
		        				;
		        		},
		        		error: function(){
		        			alert("/RemovePhoto/ function error!");
		        		}
		        	})
        		});
				
				$bar.remove();

				//window scroll setting
				WindowFixNScroll();
			}
		}     
	});   
}


function PhotoContainerAdjust( index ){
	var $display_area = $("#display_area");
	var zoom = window.photos[ index ].zoom ;
	var $container = $('#photo_container_'+index); 
	var $img = $('#'+index, $container);
	var container_width = $img.width()*2-$display_area.width();
	var container_height = $img.height()*2-$display_area.height();
	
	var target_width = $display_area.width();
	var target_height = $display_area.height();

	if ( container_width <= target_width ) {
		$container.css( "width", target_width );
		$container.css('margin-left', 0 );
	}
	else {
		$container.css( "width", container_width );
		$container.css('margin-left', -( container_width - target_width ) / 2 );
	}

	if ( container_height <= target_height ) {
		$container.css( "height", target_height );
		$container.css('margin-top', 0 );
	}
	else {
		$container.css( "height", container_height );
		$container.css('margin-top', -( container_height - target_height ) / 2 );
	}
}


function PhotoContainerAdjust( index ){
	var $display_area = $("#display_area");
 	var zoom = window.photos[ index ].zoom ;
	var $container = $('#photo_container_'+index); 
	var $img = $('#'+index, $container);
	var container_width = $img.width()*2-$display_area.width();
	var container_height = $img.height()*2-$display_area.height();
	
	var target_width = $display_area.width();
	var target_height = $display_area.height();

	if ( container_width <= target_width ) {
		$container.css( "width", target_width );
		$container.css('margin-left', 0 );
	}
	else {
		$container.css( "width", container_width );
		$container.css('margin-left', -( container_width - target_width ) / 2 );
	}

	if ( container_height <= target_height ) {
		$container.css( "height", target_height );
		$container.css('margin-top', 0 );
	}
	else {
		$container.css( "height", container_height );
		$container.css('margin-top', -( container_height - target_height ) / 2 );
	}
}

function PhotoPositionAdjust( index, left, top ){
	var $display_area = $("#display_area");
	var $container = $("#photo_container_"+index);
	var $img = $('#'+index,'#photo_container_'+index);
	var ratio = window.photos[ index ].zoom;
	var img_width = $img.width();
	var img_height = $img.height();
	var div_width = $container.width();
	var div_height = $container.height();
	var target_width = $display_area.width();
	var target_height = $display_area.height();
	var photo_left = ( div_width - img_width )/2 + left*ratio*window.display_rate;
	var photo_top = ( div_height - img_height)/2 + top*ratio*window.display_rate;
	
	if ( img_width <= target_width ) {
		if ( photo_left < 0)
			photo_left = 0;
		else if ( photo_left > target_width-img_width )
			photo_left = target_width-img_width;
	}
	else{
		if ( photo_left > img_width - target_width ) {
			photo_left = img_width - target_width;
		}
		else if ( photo_left < 0 ) {
			photo_left = 0;
		}
	}

	if ( img_height <= target_height ) {
		if ( photo_top < 0 )
			photo_top = 0;
		else if ( photo_top > target_height-img_height )
			photo_top = target_height-img_height;
	}
	$img.css({'left':photo_left,'top':photo_top});	
}

function ReadPhotoPositionLeft() {
	return window.photos[window.photo_index].positionLeft;
}
function ReadPhotoPositionTop() {
	return window.photos[window.photo_index].positionTop;
}
function ReadPhotoZoomRatio(){
	return window.photos[window.photo_index].zoom;
}

function ZoomGestureMobile( index ){
	var $container = $('#photo_container_'+index);
	var $display_area = $("#display_area");

	$container.on('pinchingOut',function(e){
		e.preventDefault();
		var $img = $('#'+index,'#photo_container_'+index);
		var zoom = window.photos[window.photo_index].zoom;

		if ( window.photos[window.photo_index].zoom<1.5 ){
			zoom = zoom +0.1;
			window.photos[ window.photo_index ].zoom = Math.floor(zoom*10)/10;
			$img.height( $display_area.height()* zoom);
			$img.css( "width" , "auto");
		}
		else{
			zoom = 1;
			window.photos[window.photo_index].zoom = Math.floor(zoom*10)/10;
			$img.height( $display_area.height()* zoom);
			$img.css( "width" , "auto");			
		}
		var left = ReadPhotoPositionLeft();
		var top = ReadPhotoPositionTop();
		PhotoContainerAdjust( window.photo_index );
		PhotoPositionAdjust( window.photo_index, left, top );
		SavePhotoPosition( index );
	});

	$container.on( 'pinchingIn',function(e){
		e.preventDefault();
		var $img = $('#'+index,'#photo_container_'+index);
		var zoom = window.photos[window.photo_index].zoom;

		if ( window.photos[index].zoom>1 ){
			zoom = zoom -0.1;
			window.photos[ window.photo_index ].zoom = Math.floor(zoom*10)/10;
			
			$img.height( $display_area.height()* zoom);
			$img.css( "width" , "auto");
		}
		else{
			zoom = 1;
			window.photos[window.photo_index].zoom = Math.floor(zoom*10)/10;
			$img.height( $display_area.height()* zoom);
			$img.css( "width" , "auto");
		}
		var left = ReadPhotoPositionLeft();
		var top = ReadPhotoPositionTop();
		PhotoContainerAdjust( window.photo_index );
		PhotoPositionAdjust( window.photo_index, left, top );
		SavePhotoPosition( window.photo_index );
	});
}

function SavePhotoPosition(index)  {
	var ratio = window.photos[ index ].zoom;
	var left = parseFloat( $('#'+index, $('#photo_container_' + index)).css('left'));
	var top = parseFloat( $('#'+index, $('#photo_container_' + index)).css('top'));
	var img_width = $('#'+index,'#photo_container_'+index).width();
	var img_height = $('#'+index,'#photo_container_'+index).height();
	var div_width = $('#photo_container_'+index).width();
	var div_height = $('#photo_container_'+index).height();
	window.photos[index].positionLeft = ( left - (div_width - img_width)/2 ) / ratio / window.display_rate; 
	window.photos[index].positionTop = ( top - (div_height - img_height)/2 ) / ratio / window.display_rate;
}

function PhotoDraggable( index ){
	for (i=0;i<window.photos.length;i++){
		$('#'+i,'#photo_container_'+i ).draggable({
			containment: '#photo_container_'+i,
			stop: function() {
				SavePhotoPosition( index );
			}
		});
	}

	PhotoEnableDraggable();
}
function PhotoEnableDraggable(){
	for (i = 0;i<window.photos.length;i++){
		$('#'+ i,'#photo_container_'+i ).draggable( 'enable' );
	}
}
function PhotoDisableDraggable(){
	for (i = 0;i<window.photos.length;i++){
		$('#'+ i,'#photo_container_'+i ).draggable( 'disable' ).css('opacity',1);
	}
}



// ========================================================================================
// =================================== CSRF Setting =======================================
// ========================================================================================

$( document ).ready(function() {

	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				// Does this cookie string begin with the name we want?
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	function csrfSafeMethod(method) {
		// these HTTP methods do not require CSRF protection
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
  
	function sameOrigin(url) {
		// test that a given url is a same-origin URL
		// url could be relative or scheme relative or absolute
		var host = document.location.host; // host + port
		var protocol = document.location.protocol;
		var sr_origin = '//' + host;
		var origin = protocol + sr_origin;
		// Allow absolute or scheme relative URLs to same origin
		return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
			(url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
			// or any other URL that isn't scheme relative or absolute i.e relative.
			!(/^(\/\/|http:|https:).*/.test(url));
	}
	  
	$.ajaxSetup({
		crossDomain: false, // obviates need for sameOrigin test
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type)) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		}
	});
});






