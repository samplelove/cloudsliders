﻿from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'system.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'design.views.index'),
    url(r'^admin/', include(admin.site.urls)),
	
	url(r'^Login/$', 'design.views.Login_view' ),
	url(r'^Logout/$', 'design.views.Logout_view' ),
    url(r'^Regist/$', 'design.views.Regist_view' ),
	url(r'^GetUser/$', 'design.views.GetUser_view' ),
	url(r'^GetThemeClass/$', 'design.views.GetThemeClass_view' ),
	url(r'^SaveThemeSelected/$', 'design.views.SaveThemeSelected_view' ),
	url(r'^UploadPhoto/$', 'design.views.UploadPhoto_view'),
	url(r'^GetUploadPhotos/$', 'design.views.GetUploadPhotos_view'),
	url(r'^UploadPhotoSetting/$', 'design.views.SavePhotoSetting_view'),
	url(r'^RemovePhoto/$', 'design.views.RemovePhoto_view'),
	url(r'^GetTextContent/$', 'design.views.GetTextContent_view'),
	url(r'^UploadText/$', 'design.views.SaveTextContent_view'),
	url(r'^GenerateQR/$', 'design.views.GenerateQR_view'),
	url(r'^SaveAblumTitleAndDate/$', 'design.views.SaveAblumTitleAndDate_view'),
	url(r'^SavePage/$', 'design.views.SavePage_view'),
	url(r'^RemovePage/$', 'design.views.RemovePage_view'),
	#url(r'^EditPage/$', 'design.views.EditPage_view'),

	#============= modify_by_cycho_2013_08_10 ====================
    # user auth urls
    url(r'^accounts/login/$', 'design.views.login'),
    url(r'^accounts/auth/$', 'design.views.auth_view'),
    url(r'^accounts/logout/$', 'design.views.logout'),
    url(r'^accounts/loggedin/$', 'design.views.loggedin'),
    url(r'^accounts/invalid/$', 'design.views.invalid_login'),
    url(r'^accounts/regist/$', 'design.views.regist'),
    url(r'^accounts/regist_view/$', 'design.views.regist_view'),
	
    #============= modify_by_cycho_2013_08_10 ====================
	url(r'^ProductSetting/(?P<className>.+)/$', 'design.views.ProductSetting_view'),
	url(r'^page/(?P<uid>.+)/$', 'design.views.ProducePage_view'),

	#============= wedding project ===============================
	url(r'^AddNewPage/(?P<className>.+)/(?P<themeName>.+)/(?P<pageId>.+)/$', 'design.views.AddNewPage_view'),
	url(r'^WeddingAlbumSetting/(?P<uid>.+)/$', 'design.views.WeddingSetting_view'),
	url(r'^SavePhotoFrame/$', 'design.views.SavePhotoFrame_view'),
	#============= last judge ====================================
	url(r'^(?P<uString>.+)/$', 'design.views.JudgeThemeClassName_view'),
	
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
